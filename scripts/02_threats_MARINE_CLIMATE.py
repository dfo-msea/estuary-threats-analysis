# John Cristiani john.cristiani@dfo-mpo.gc.ca
# October 2022

# Objective:
# Process threat data for overlay analysis with estuaries.
# Each feature requires different cleanup, so unfortunately it is a bit hard to standardize this
# into common functions. Therefore, its a bit verbose.


import arcpy
import os
import pandas as pd
import numpy as np
import copy

arcpy.CheckOutExtension("Spatial")
from arcpy.sa import *



#########################################
# Data

# IN
root = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\spatial'
# threat data - original
threats_dir = os.path.join(root, '01_original/threat_data')

# OUT
# In general, 01scratch is a staging area of the data with any initial preprocessing required.
# 02estuaries gdb is with the data associated to the estuaries.
gdb_clean = os.path.join(root, '02_working/threats_01scratch.gdb')
gdb_out = os.path.join(root, '02_working/threats_02estuaries.gdb')
# temporary out options for presentation mapping purposes:
#gdb_clean = os.path.join(root, '02_working/presentation/presentation_01scratch.gdb')
#gdb_out = os.path.join(root, '02_working/presentation/presentation_02estuaries.gdb')


# estuaries
estuaries = os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_01_ALL')
# land
land = os.path.join(root, r'02_working/estuaries_watersheds.gdb/land_erase')

arcpy.env.workspace = gdb_clean
arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(3005)

# Cell size for analysis
cs = 100  # meters

# Cell value for area based features
area_cell_value = 100


#########################################
# Convert estuaries to raster to use as a snap raster and for zonal statistics

# add a priority field. This is required in the PolygonToRaster or else very small estuaries won't
# be given a value.

est_rast = os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_rast')
if not arcpy.Exists(est_rast):

    fields = arcpy.Describe(estuaries).fields
    if 'priority' not in [f.name for f in fields]:
        arcpy.AddField_management(estuaries, 'priority', 'SHORT')
        with arcpy.da.UpdateCursor(estuaries, ['priority']) as cursor:
            for row in cursor:
                row[0]=1
                cursor.updateRow(row)

    arcpy.PolygonToRaster_conversion(estuaries, 'EST_NO', est_rast, 'MAXIMUM_AREA', 'priority', cs)


#########################################
# Create a coastline that is buffered 10m from normal coastline
# This is used to snap points to that are over land but should be on water.
# For these points, I was originally snapping just to the coastline, but I found that with the KD
# function, that even with land as a barrier, it would project on both side of the coastline,
# potentially reaching to other areas of water on the other side of land.
# I originally tried with 0.5 meters and that wasn't enough. Not sure what the threshold is, but 10m
# shouldn't make too much of a difference.

landsnap = os.path.join(root, '02_working/estuaries_watersheds.gdb/land_buffsnap')
if not arcpy.Exists(landsnap):
    arcpy.PairwiseBuffer_analysis(land, landsnap, 10, 'ALL')



#########################################
#
def selLocation(layer, buffer, outname):
    '''
    Function to select features based on distance to estuaries
    This helps to speed up processing when creating the kernel density layer
    '''
    print('Selecting features within buffer distance of estuaries')
    arcpy.MakeFeatureLayer_management(layer,'memory/select')
    arcpy.SelectLayerByLocation_management('memory/select', 'INTERSECT', estuaries, buffer)
    arcpy.CopyFeatures_management('memory/select', outname)
    arcpy.Delete_management('memory/select')



#########################################
#
def bufferOrphanErase(outname, buffer):

    '''
    This function creates a buffered feature but with the portions interesecting land erased. It is
    used for clipping the multiring buffers in that function.
    
    There's potentially the case where a buffer extends over a land barrier, and if we erase
    with land, the feature is disconnected, indicating that the threat would not have drifted
    to that estuary. Therefore:
    Erase where land, then delete parts not touching the original feature.

    Doing one spatial select with 'Intersect' or 'Contains' does not work. You 
    still get small pieces selected because they touch other features that are not 
    their ID feature. So I think I need to do a more involved process:
    In a for loop, create a layer for each uID, for both the sg and the buff
    do the spatial select and add the partid of the piece to a list
    then use the list to select the pieces by attribute.

    This takes a while to run, but I think it is the only way.
    '''

    print('Creating buffer orphan clipper')

    # add a uID field
    arcpy.AddField_management(outname, 'uID', 'LONG')
    # get name of OID field
    oid_field = arcpy.ListFields(outname, field_type='OID')[0].name
    with arcpy.da.UpdateCursor(outname, [oid_field, 'uID']) as cursor:
        for row in cursor:
            row[1]=row[0]
            cursor.updateRow(row) 

    arcpy.PairwiseBuffer_analysis(outname, 'memory/out', buffer, dissolve_option='NONE')
    arcpy.PairwiseErase_analysis('memory/out', land, 'memory/erase')
    arcpy.MultipartToSinglepart_management('memory/erase', 'memory/multising')
    arcpy.AddField_management('memory/multising', 'partID', 'SHORT')
    oid_field = arcpy.ListFields('memory/multising', field_type='OID')[0].name
    with arcpy.da.UpdateCursor('memory/multising', [oid_field, 'partID']) as cursor:
        for row in cursor:
            row[1]=row[0]
            cursor.updateRow(row)

    arcpy.SetLogHistory(False) # trying to speed this up, not sure if it matters
    features = []
    uIDs = [row[0] for row in arcpy.da.SearchCursor(outname, ['uID'])]
    arcpy.MakeFeatureLayer_management(outname, 'tmp_sg') # way faster creating layers
    arcpy.MakeFeatureLayer_management('memory/multising', 'tmp_buff')
    for i in uIDs:
        # first check if there is just one part per uID. I can skip the other selections if so.
        arcpy.SelectLayerByAttribute_management(
            'tmp_buff',
            'NEW_SELECTION',
            "uID = {}".format(i)
        ) 
        sel_count = int(arcpy.GetCount_management('tmp_buff')[0])
        if sel_count==1:
            with arcpy.da.SearchCursor('tmp_buff', ['partID']) as cursor:
                for row in cursor:
                    features.append(row[0])
            arcpy.SelectLayerByAttribute_management('tmp_buff', 'CLEAR_SELECTION')
            continue
        arcpy.SelectLayerByAttribute_management('tmp_buff', 'CLEAR_SELECTION')
        # if not, then continue you with identifying overlapping features
        print('spatial select for feature {}, {} of {}'.format(str(i), str(uIDs.index(i)), str(len(uIDs))))
        arcpy.SelectLayerByAttribute_management( # get the origin piece
            'tmp_sg',
            'NEW_SELECTION',
            "uID = {}".format(i)
        )
        arcpy.SelectLayerByLocation_management( # get just the part that overlaps with the original feature
            'tmp_buff',
            'INTERSECT',
            'tmp_sg',
        )  
        arcpy.SelectLayerByAttribute_management( # get just the pieces that are associated with the origin feature
            'tmp_buff',
            'SUBSET_SELECTION',
            "uID = {}".format(i)
        ) 
        sel_count = int(arcpy.GetCount_management('tmp_buff')[0])
        if sel_count!=1: # check if somnething weird happened
            print(sel_count)
            print('more/less than 1 feature selected')
            break
        with arcpy.da.SearchCursor('tmp_buff', ['partID']) as cursor: # add those feature ids to list to keep
            for row in cursor:
                features.append(row[0])
        arcpy.SelectLayerByAttribute_management('tmp_buff', 'CLEAR_SELECTION')
        arcpy.SelectLayerByAttribute_management('tmp_sg', 'CLEAR_SELECTION')
    
    feat_tup = tuple(features)
    sel_feat = arcpy.SelectLayerByAttribute_management(
        'memory/multising',
        'NEW_SELECTION',
        "partID IN {}".format(feat_tup)
    )
    arcpy.CopyFeatures_management(sel_feat, outname+'_buffclipper')
    arcpy.Delete_management('tmp_buff')
    arcpy.Delete_management('tmp_sg')
    arcpy.Delete_management('memory/out')
    arcpy.Delete_management('memory/erase')
    arcpy.Delete_management('memory/multising')



#########################################
#
def featureToRaster(outname, field):

    '''
    This function converts a polygon or line threat feature to raster (not set up for line data, 
    assuming we will buffer lines to polygons first).

    It calculates the POPULATION value (i.e. the threat value) present in each cell.
    If it is an area based feature, I use a generic value of 100.
    If it is a different value (e.g. aquaculature gate value), then this value gets evenly spread
    between cells (for polygons).

    This function ended up being redundant for certian features. If a feature is buffered, then the
    output from this function is not used.
    '''

    print("Feature to raster conversion and POPULATION field calculation")

    fc_desc = arcpy.Describe(outname)
    shptype = fc_desc.shapeType
    fields = fc_desc.fields

    if shptype != 'Point' and 'priority' not in [f.name for f in fields]:
        arcpy.AddField_management(outname, 'priority', 'SHORT')
        arcpy.CalculateField_management(outname, 'priority', 1)

    arcpy.env.snapRaster = est_rast

    # IF POINT
    if shptype == 'Point':
        if 'POPULATION' not in [f.name for f in fields]:
            arcpy.AddField_management(outname, 'POPULATION', 'DOUBLE')      
            if field == 'area_based': # raster cells can all just be the generic area value
                cell_value = area_cell_value
                arcpy.CalculateField_management(outname, 'POPULATION', area_cell_value)
            else:
                with arcpy.da.UpdateCursor(outname,[field, 'POPULATION']) as cursor:
                    for row in cursor:
                        row[1]=row[0]
                        cursor.updateRow(row)
        arcpy.PointToRaster_conversion(outname, 'POPULATION', outname+'_rast', 'SUM', cellsize=est_rast)
        return outname+'_rast'


    # ISSUES discovered 2023-06-14:
    #
    # I am writing this all out just in case this code gets adapted for any future projects. 
    # There is no error in this code, I am just noting assumptions made that created issues
    # for certain features (e.g. shellfish aquaculture value-based features).
    #
    # 1st issue:
    # When you have features that overlap, when converting to raster, its possible that small
    # features will not be represented. This only came to light in the aquaculture data, so it was
    # not an issue for the other features (e.g. dreding and log handling were dissolved before, most
    # features do not have overlap, and the other value-based features are already gridded).
    #
    # To fix this for aquaculture, I calculated the Priority field prior to this function and
    # assigned the smallest features the highest priority.
    #
    # 2nd issue:
    # For value-based features below, when converting from raster back to poly, its possible that
    # multiple part features are generated, but I originally did not specify "MULTIPLE_OUTER_PART",
    # and therefore single part features were created. In the following steps when I join back to
    # the original polys, it ends up duplicating features and therefore a threat will get
    # overrepresented for that area.
    # To fix this I added "MULTIPLE_OUTER_PART" in the RasterToPolygon conversion function.
    #
    # This is only an issue for value-based features. Area based features are handled in the
    # first part of the IF statement. They are only converted to raster, but there is no joining
    # back. Essentially, this acts as a dissolve. That raises the question, when does dissolving
    # make sense? Things like instances of log booms can't overlap, so it makes sense to dissolve
    # them. Something like underwater infrastructure could overlap (this is the only area based 
    # polygon feature with overlapping polys where this would make sense). However, all we have are
    # tenures, which we don't know much about, so a dissolve is actually the most conservative
    # approach. 
    #
    # SO...
    # The issue that remains that we will just have to accept as a limitation of our approach: 
    # for overlapping features, when converted to raster and then back to poly, for the larger
    # feature, its shape will have changed (i.e., a hole in the middle), and therefore, the intensity
    # value per cell would have been calculated over fewer cells than what would actually represent
    # the feature.
    # This isn't an issue for features without a buffer, since the output from this function is used
    # in the estuaryThreats function, but for features with a buffer, this values is used over the
    # entire unaltered area (features are rasterized individually in the bufferRings function).
    # However, this is only really an issue for very few features. The change in values
    # would be very small. Therefore, this is just a limitation of our approach. If we wanted to
    # redo this in the future, then this function would have to process each feature in a feature
    # class individually, similarly to how I do it in the bufferRings function.
    # 
    # Also to note: not an issue for point data.
    #


    # POLYGONS
    if field == 'area_based': # raster cells can all just be the generic area value
        cell_value = area_cell_value
        if 'POPULATION' not in [f.name for f in fields]:
            arcpy.AddField_management(outname, 'POPULATION', 'DOUBLE')
            arcpy.CalculateField_management(outname, 'POPULATION', area_cell_value)
        arcpy.PolygonToRaster_conversion(outname, 'POPULATION', outname + '_rast', 'MAXIMUM_AREA', 'priority', est_rast)

    else:

        # For non area-based features, we need to spread the value of the stressor evenly throughout
        # the feature.
        # We need to convert to raster to first get the area of the feature
        # in vector format, the number of cells that would make up that feature is dependent on the
        # shape, so we can't rely on just that area to calculate the number of cells.
        # whereas, if a feature is made up of all squares, and we know the area of those squares
        # then we can know how many squared make it up

        # convert to raster, code as OID
        oid_fn = arcpy.Describe(outname).OIDFieldName
        arcpy.PolygonToRaster_conversion(outname, oid_fn, outname + '_tr1', 'MAXIMUM_AREA', 'priority', est_rast)

        # convert back to poly
        arcpy.RasterToPolygon_conversion(outname+'_tr1', outname+'_rastpoly', 'NO_SIMPLIFY', 'Value', 'MULTIPLE_OUTER_PART')
        arcpy.AddField_management(outname+'_rastpoly', 'rastarea', 'DOUBLE')
        #arcpy.CalculateField_management(outname+'_rastpoly', 'rastarea', '!Shape_Area!')

        with arcpy.da.UpdateCursor(outname+'_rastpoly', ['rastarea', 'Shape_Area']) as cursor:
            for row in cursor:
                row[0]=row[1]
                cursor.updateRow(row)

        # join attributes of original poly
        arcpy.MakeFeatureLayer_management(outname, 'temp_lyr')
        arcpy.env.qualifiedFieldNames = False
        arcpy.AddJoin_management('temp_lyr', oid_fn, outname+'_rastpoly', 'gridcode')
        arcpy.CopyFeatures_management('temp_lyr', outname+'_join')
        arcpy.Delete_management(outname)
        arcpy.CopyFeatures_management(outname+'_join', outname)

        # Add Population field and calculate as this new value
        area = cs ** 2
        arcpy.AddField_management(outname, 'POPULATION', 'DOUBLE')
        with arcpy.da.UpdateCursor(outname, [field, 'rastarea', 'POPULATION']) as cursor:
            for row in cursor:
                number_of_cells = row[1] / area
                row[2] = row[0] / number_of_cells 
                cursor.updateRow(row)

        # convert back to raster again
        arcpy.PolygonToRaster_conversion(outname, 'POPULATION', outname + '_rast', 'MAXIMUM_AREA', 'priority', est_rast)

        arcpy.Delete_management(outname+'_tr1')
        arcpy.Delete_management('temp_lyr')
        arcpy.Delete_management(outname+'_rastpoly')
        arcpy.Delete_management(outname+'_join')
    
    return outname+'_rast'



#########################################
#
def bufferRingsToRaster(outname, buffer):

    '''
    This function creates multiple non-overlapping buffer rings around a feature to simulate the
    spread of a stressor.

    We created this approach because a Kernel Density-like approach was not available for polygon
    features. We decided to apply it to point features as well to maintain consistency.
    However, one limitation of this approach is that buffers erased by land still don't account for
    the extra distance and dissipation from flow around a a water feature.

    It ended up being a bit more complicated than expected because with the arcgis function to
    create the rings, if you wanted to make them non-overlapping internally, then it would end up
    dissolving them with any similar ones they overlap externaly.
    The only way around this is to generate the rings for each feature separately.
    I then to convert to raster and add up the values to capture any overlap.

    See additional notes below for more detailed explanation.
    '''
    
    print('Creating multiring buffers')

    rings = 9 # I will do 9 steps (so 10 total including the original feature)
    weights = [0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1] # must be descending

    ring_width = buffer / 9
    distances = []
    for i in range(1,rings+1):
        distances.append(int(ring_width*i))

    # How does a stressor spread?
    # Do we think of it like a single discrete event i.e. one quantity that spreads out, where all
    # the neighboring cells would add up to the original amount?
    # Or, do we think of it like a continuous event where the stressor is continuously generated so
    # that at the source it is the full amount and further away it dissipates, but over the entire
    # area it would add up to more than the original amount?

    # We are using the second approach, the "fountain model". This would differ from the Kernel
    # Density approach, but I think it actually makes more sense for the process we are trying to
    # capture.

    # Keep in mind - no matter what, a "stressor" spreading is an abstract thing.
    # For example, an aquaculture gate value is the number of fish, but we aren't spreading fish 
    # around, we are spreading a perceived stressor around
    # **and we want to know what that unit of estuary would be continuously experience.**
    # We don't really know how most stressors actually spread and dilute, so it is just a matter of 
    # making the case for one model or another.

    gdb_rasters = arcpy.CreateFileGDB_management(root, 'temp')

    oid_fn = arcpy.Describe(outname).OIDFieldName
    count = arcpy.GetCount_management(outname)[0]
    shptype = arcpy.Describe(outname).shapeType

    with arcpy.da.SearchCursor(outname, [oid_fn, 'POPULATION']) as cursor:
        for row in cursor:

            print(f'Processing {str(row[0])} feature out of {count}')

            where = f'{oid_fn} = {row[0]}'
            arcpy.MakeFeatureLayer_management(outname, 'memory/templyr', where)

            tlyr = 'memory/templyr'+str(row[0])

            if shptype == 'Point':
                arcpy.MultipleRingBuffer_analysis(
                    'memory/templyr', 
                    tlyr,      
                    distances,
                    Dissolve_Option = 'ALL')
            else:                
                arcpy.MultipleRingBuffer_analysis(
                    'memory/templyr', 
                    tlyr,      
                    distances,
                    Dissolve_Option = 'ALL',
                    Outside_Polygons_Only='OUTSIDE_ONLY')

            arcpy.Delete_management('memory/diss_fc') # this is weird, this gets generated by the buffer function, but it doesn't delete itself

            oid = row[0] # need these values for later after the row values get overwritten by the next update cursor
            popn = row[1]

            # add field for weighting and calculate new POPULATION VALUE
            arcpy.AddField_management(tlyr, 'weight', 'FLOAT')
            arcpy.AddField_management(tlyr, 'POPULATION', 'DOUBLE')
            with arcpy.da.UpdateCursor(tlyr, ['distance', 'weight', 'POPULATION']) as cursor:
                for row in cursor:
                    ind = distances.index(row[0])
                    row[1] = weights[ind]
                    row[2] = weights[ind] * popn
                    cursor.updateRow(row)

            # clip with bufferorphanclipper feature
            arcpy.PairwiseClip_analysis(tlyr, outname+'_buffclipper', tlyr+'_clip')
            arcpy.Delete_management(tlyr)

            # output to raster
            arcpy.AddField_management(tlyr+'_clip', 'priority', 'SHORT')
            arcpy.CalculateField_management(tlyr+'_clip', 'priority', 1)
            arcpy.env.snapRaster = est_rast
            arcpy.PolygonToRaster_conversion(tlyr+'_clip', 'POPULATION', tlyr+'_rast', 'MAXIMUM_AREA', 'priority', est_rast)

            # Add back in center piece
            # However, you can't just add back in the whole dataset to each buffered piece. Where a
            # buffer overlaps another feature origin, you end up overwriting that features buffer.
            # So, use each feature as a mask to get just those cells.
            # (This is kinda screwy with the IsNull, but managing nulls with just the Con tool 
            # wasn't working as expected)

            # ExtractByMask doesn't work. If two points overlap the same cell, their values get added
            # together when creating the original feature to raster (as they should. It needs to be
            # this way for features not buffered). So, I can't use ExtractByMask, I need to
            # create a raster for each feature.

            if shptype == 'Polygon':
                arcpy.PolygonToRaster_conversion('memory/templyr', 'POPULATION', 'memory/templyrrast', 'MAXIMUM_AREA', 'priority', est_rast)
            else:
                arcpy.PointToRaster_conversion('memory/templyr', 'POPULATION', 'memory/templyrrast', 'MOST_FREQUENT', cellsize=est_rast)

            #outMask = ExtractByMask(outname+'_rast', 'memory/templyr')
            outIsNull = IsNull('memory/templyrrast')
            arcpy.env.extent = tlyr+'_rast'
            out = os.path.join(gdb_rasters[0], 'templyr'+str(oid))            
            outCon = Con(outIsNull, 'memory/templyrrast', tlyr+'_rast', "Value = 0")
            outRas = SetNull(outCon, outCon, 'Value=0.0')
            outRas.save(out)
            arcpy.env.extent = None

            arcpy.Delete_management('memory/templyr')
            arcpy.Delete_management('memory/templyrrast')
            arcpy.Delete_management(tlyr+'_clip')
            arcpy.Delete_management(tlyr+'_rast')

    # add up rasters
    print('adding rasters')
    arcpy.env.workspace = gdb_rasters[0]
    rasts = arcpy.ListRasters()
    arcpy.env.extent = 'MAXOF'
    outCellStats = CellStatistics(rasts, 'SUM')
    arcpy.env.workspace = gdb_clean
    outCellStats.save(outname+'_rastbuffadd')

    return outname+'_rastbuffadd'



#########################################
#
def estuaryThreats(outfc, outname):
    '''
    Function to associate threat values with estuaries
    '''
    print('Zonal statistic - threats and estuaries')
    outZSaT = ZonalStatisticsAsTable(
        est_rast, 
        'Value', 
        outfc, 
        os.path.join(gdb_out, outname), 
        'DATA', 
        'SUM')
    # change Value to EST_NO
    arcpy.AlterField_management(
        os.path.join(gdb_out, outname),
        'Value',
        'EST_NO',
        'EST_NO')


#########################################
#
def cleanUp(outname):
    '''
    Clean up intermediate datasets
    You need to run this before processing another feature, but it is a feature to turn off for
    testing purposes.
    '''
    print('Delete intermediate data')
    arcpy.Delete_management(root + '/temp.gdb')
    arcpy.Delete_management(outname+'_rast')
    arcpy.Delete_management(outname+'_buffclipper')
    arcpy.Delete_management(outname+'_rastbuffadd')




#######################################
#######################################
# Test data

# outname = 'thrt_test'
# #outname = 'thrt_testpt'
# buffer = 4000 # if no buffer, put 0 for the spatial select
# #value_field = 'gate_value'
# value_field = 'area_based'

# # if the feature doesn't need to be buffered then you don't need to run the bufferOrphanErase
# # or bufferRingsToRaster function

# selLocation('test', buffer, outname)
# #selLocation('testpt', buffer, outname)
# bufferOrphanErase(outname, buffer)
# outfc = featureToRaster(outname, value_field)
# outfc = bufferRingsToRaster(outname, buffer)
# estuaryThreats(outfc, outname)
# cleanUp(outname)





#########################################
#
# Marine threats
#
#########################################


#######################################
#######################################
# Disposal at sea

outname = 'thrt_mar_disposalatsea'
buffer = 2000
value_field = 'area_based'

disposal = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb\DAS_Active_Inactive_Sites_2018')
selLocation(disposal, buffer, outname)
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)


#########################################
#########################################
# Underwater cable/infrastructure

outname = 'thrt_mar_underwaterinfrastructure'
buffer = 0
value_field = 'area_based'

tenures = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/TA_CRT_SVW_polygon')
where = """TEN_SUBPRP IN ('ELECTRIC POWER LINE', 'SCIENCE MEASUREMENT/RESEARCH', 'SEWER/EFFLUENT LINE', 'TELECOMMUNICATION LINE', 'WATER LINE')"""
arcpy.MakeFeatureLayer_management(tenures, 'memory/out', where_clause=where)

# if I ever need to buffer this, then I will need to dissolve first since there are overlapping
# features

selLocation('memory/out', buffer, outname)
arcpy.Delete_management('memory/out')
outfc = featureToRaster(outname, value_field)
estuaryThreats(outfc, outname)
cleanUp(outname)


#########################################
#########################################
# Anchorages

#commercial
outname = 'thrt_mar_anchoragescom'
buffer = 2000
value_field = 'area_based'

#anch_co = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/ACHBRT_P')
# Data is unpublished updated anchorages from Cathryn
anch_co_excel = os.path.join(threats_dir, 'BC_commercial_anchorages_Jan2022.xlsx')

anch = pd.read_excel(anch_co_excel)
anch = anch[anch.Latitude != 'Not allocated']
x = np.array(np.rec.fromrecords(anch.values)) 
names = anch.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToTable(x, os.path.join(arcpy.env.workspace, f'temp_table'))
arcpy.MakeXYEventLayer_management('temp_table', 'Longitude', 'Latitude', 'temp_lyr', 4326)
arcpy.CopyFeatures_management('temp_lyr', 'temp_pts')
arcpy.Delete_management('temp_table')
arcpy.Delete_management('temp_lyr')

selLocation('temp_pts', buffer, outname)
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)
arcpy.Delete_management('temp_pts')


# recreational
outname = 'thrt_mar_anchoragesrec'
buffer = 500
value_field = 'area_based'
anch_re = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/ACHARE_P')
selLocation(anch_re, buffer, outname)
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)


#######################################
#######################################
# Ports and Terminals

# some smaller ports are redundant with docks in the floating infrastructure 
# dataset (see Bamfield). However, some ports are large and floating e.g. (Victoria).
# Therefore, I will include only those larger ports.

outname = 'thrt_mar_portsandterminals'
buffer = 2000
value_field = 'area_based'

ports = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb\PRT_TRMNAL_point')
keep_id = ['241','242','185','188','190','189']
where = f"""SRCDATA_ID IN {tuple(keep_id)}"""
arcpy.MakeFeatureLayer_management(ports,'memory/select',where_clause=where)
arcpy.CopyFeatures_management('memory/select', outname+'_sel')
arcpy.Delete_management('memory/select')
# some of the features are contained by land. When I do the complex buffer operation where I need
# to reference the original point, this part of the buffer gets erased by land, so it won't ever
# overlap the original point.
# Therefore, for any points on land, snap these to the land boundary.
# (need to do select without making a feature layer, so that it edits just the ones selected, but
# maintains the rest of the points too)
sel = arcpy.SelectLayerByLocation_management(outname+'_sel', 'INTERSECT', land)
arcpy.Snap_edit(sel, [[landsnap, 'EDGE', '200 Meters']])

selLocation(outname+'_sel', buffer, outname)
arcpy.Delete_management(outname+'_sel')
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)


#######################################
#######################################
# Docks, marinas, home/lodges

outname = 'thrt_mar_docks'
buffer = 500
value_field = 'area_based'
docks = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb\docks')
where = f"""category NOT IN ('Marina') AND region IN ('BC')"""
arcpy.MakeFeatureLayer_management(docks,'memory/select',where_clause=where)
arcpy.CopyFeatures_management('memory/select', outname+'_sel')
arcpy.Delete_management('memory/select')
# move any on land to nearest land boundary
sel = arcpy.SelectLayerByLocation_management(outname+'_sel', 'INTERSECT', land)
arcpy.Snap_edit(sel, [[landsnap, 'EDGE', '200 Meters']])
selLocation(outname+'_sel', buffer, outname)
arcpy.Delete_management(outname+'_sel')
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)


outname = 'thrt_mar_marinas'
buffer = 2000
value_field = 'area_based'
marinas = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb\docks')
where = f"""category IN ('Marina') AND region IN ('BC')"""
arcpy.MakeFeatureLayer_management(marinas,'memory/select',where_clause=where)
arcpy.CopyFeatures_management('memory/select', outname+'_sel')
arcpy.Delete_management('memory/select')
# move any on land to nearest land boundary
sel = arcpy.SelectLayerByLocation_management(outname+'_sel', 'INTERSECT', land)
arcpy.Snap_edit(sel, [[landsnap, 'EDGE', '200 Meters']])
selLocation(outname+'_sel', buffer, outname)
arcpy.Delete_management(outname+'_sel')
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)


outname = 'thrt_mar_homeslodges'
buffer = 2000
value_field = 'area_based'
homeslodges = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb\homes_lodges')
where = f"""region IN ('BC')"""
arcpy.MakeFeatureLayer_management(homeslodges,'memory/select',where_clause=where)
arcpy.CopyFeatures_management('memory/select', outname+'_sel')
arcpy.Delete_management('memory/select')
# move any on land to nearest land boundary
sel = arcpy.SelectLayerByLocation_management(outname+'_sel', 'INTERSECT', land)
arcpy.Snap_edit(sel, [[landsnap, 'EDGE', '200 Meters']])
selLocation(outname+'_sel', buffer, outname)
arcpy.Delete_management(outname+'_sel')
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)




#######################################
#######################################
# Derelict vessels

outname = 'thrt_mar_derelictvessels'
buffer = 500
value_field = 'area_based'

dvessels = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb\derelicts_BC_US')
# There is one that is super far inland and may be a mistake. Remove it.
where = f"""OBJECTID NOT IN (129, 130, 132)"""
arcpy.MakeFeatureLayer_management(dvessels,'memory/select',where_clause=where)
arcpy.CopyFeatures_management('memory/select', outname+'_sel')
arcpy.Delete_management('memory/select')
# move any on land to nearest land boundary
sel = arcpy.SelectLayerByLocation_management(outname+'_sel', 'COMPLETELY_WITHIN', land)
arcpy.Snap_edit(sel, [[landsnap, 'EDGE', '250 Meters']])

selLocation(outname+'_sel', buffer, outname)
arcpy.Delete_management(outname+'_sel')
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)



#######################################
#######################################
# Dredging

outname = 'thrt_mar_dredging'
buffer = 0
value_field = 'area_based'

# original dataset:
dredging = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb\Dredging_Sites_2010_to_2020')

# late update - we are now including dredging points from the PATH dataset
# PATH data:
# Most of the records are for small scale construction, repair or maintenance events. It's not to
# note the presence of a feature, but instead to note an event. If we wanted to use the data to 
# capture for things like culverts, it would then be a wildly incomplete dataset since its not about
# documenting where all the culverts are.
# For events, we could possibly incorporate these into shoreline and general development since we
# just dissolve the footprint anyways, but if the repair event only produces a stressor for a short 
# period of time then this may not be relevant.
# For now we will use the dataset only for dredging and freshwater obstruction points.
# For dredging, incorporate only marine dredging (within x km of estuary)
path_csv = os.path.join(threats_dir, 'PATH/2023-01-23_CSAS_Referrals_V2.xlsx')
arcpy.conversion.ExcelToTable(path_csv, 'PATH_01_tbl')
cs = arcpy.SpatialReference(4326)
arcpy.XYTableToPoint_management('path_01_tbl', 'path_02_pt', 'Decimal_Longitude', 'Decimal_Latitude','', cs)
# select dredging points
where = """Primary_Impact = 'Dredging/Excavating'"""
arcpy.MakeFeatureLayer_management('path_02_pt', 'temp_lyr', where_clause=where)
arcpy.SelectLayerByLocation_management('temp_lyr', 'INTERSECT', estuaries, 2000) # just get ones in the neighborhood
arcpy.CopyFeatures_management('temp_lyr', 'path_03_dredge')
# Buffer by 50. This is a conservative estimate. Most of these are small maintenance projects.
arcpy.Buffer_analysis('path_03_dredge', 'path_04_buffer', 50)

# Merge the two datasets
arcpy.Merge_management([dredging, 'path_04_buffer'], 'temp_dredge_merge')
arcpy.Delete_management(['path_01_tbl', 'path_02_pt', 'temp_lyr', 'path_03_dredge', 'path_04_buffer'])

# There are overlapping polygons, which are fine once we do the final buffer, but at the start
# we don't want overlapping polygons if the activity is relatively the same.
arcpy.Dissolve_management('temp_dredge_merge', 'temp_diss', multi_part='SINGLE_PART')
# Erase with land. There are a number of places far in land. We are only interested in the marine
# aspect of dredging.
arcpy.Erase_analysis('temp_diss', land, 'temp_erase')
arcpy.MultipartToSinglepart_management('temp_erase', outname+'_ms')
arcpy.Delete_management('temp_dredge_merge')
arcpy.Delete_management('temp_diss')
arcpy.Delete_management('temp_erase')

# Value field: we could use Quantity Actual (m3), but since we are dealing with polygons, this
# quantity seems to scale with the size of the polygon. Keep it simple and just use the area.

selLocation(outname+'_ms', buffer, outname)
arcpy.Delete_management(outname+'_ms')
outfc = featureToRaster(outname, value_field)
estuaryThreats(outfc, outname)
cleanUp(outname)



#########################################
#########################################
# Log handling sites

outname = 'thrt_mar_loghandling'
buffer = 2000
value_field = 'area_based'

lh_tenures = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/TA_CRT_SVW_polygon')
lh_whse = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/WHSE_ENVIRONMENTAL_MONITORING_CHRA_INDUSTRIES_POINT')

where = """TEN_SUBPRP = 'LOG HANDLING/STORAGE'"""
lh_ten_sel = arcpy.SelectLayerByAttribute_management(lh_tenures, where_clause=where)
where = """DESCRIPTION IN ('Log Booming', 'Log Booms', 'Log Dump', 'Log Dump/Sort')"""
lh_whs_sel = arcpy.SelectLayerByAttribute_management(lh_whse, where_clause=where)
arcpy.Buffer_analysis(lh_whs_sel, 'temp_buffer', 1)

arcpy.Merge_management([lh_ten_sel, 'temp_buffer'], 'temp_merge')
arcpy.Dissolve_management('temp_merge', 'temp_diss', multi_part='SINGLE_PART')
arcpy.PairwiseErase_analysis('temp_diss', land, 'temp_erase')
arcpy.MultipartToSinglepart_management('temp_erase', 'temp_ms')
# move any on land to nearest land boundary
sel = arcpy.SelectLayerByLocation_management('temp_ms', 'COMPLETELY_WITHIN', land)
arcpy.Snap_edit(sel, [[landsnap, 'EDGE', '250 Meters']])

selLocation('temp_ms', buffer, outname)

fcs = arcpy.ListFeatureClasses('temp_*')
for fc in fcs:
    arcpy.Delete_management(fc)

bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)



#######################################
#######################################
# Aquaculture
# (1) Finfish
# (2) Shellfish

# tenures = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/TA_CRT_SVW_polygon')
# aquaculture = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/aquaculture')

# buffer = 2000
# value_field = 'area_based'  # THIS WILL CHANGE IF WE EVER GET GATE VALUES FROM KRSISTA/ASHLEY

# #### Shellfish
# outname = 'thrt_mar_aquaculture_shellfish'

# where = """TEN_PURPOS = 'AQUACULTURE' And TEN_SUBPRP = 'SHELL FISH'"""
# arcpy.MakeFeatureLayer_management(tenures, 'temp_tenures', where_clause=where)
# arcpy.CopyFeatures_management('temp_tenures', 'temp_tenures_shellfish')
# arcpy.Delete_management('temp_tenures')

# where = """region = 'BC' And type = 'shellfish'"""
# arcpy.MakeFeatureLayer_management(aquaculture, 'temp_aqua', where_clause=where)

# # remove points overlapping a tenure polygon
# arcpy.SelectLayerByLocation_management('temp_aqua', 'INTERSECT', 'temp_tenures_shellfish')
# arcpy.SelectLayerByLocation_management('temp_aqua', selection_type='SWITCH_SELECTION') 
# arcpy.CopyFeatures_management('temp_aqua', 'temp_aqua_select')
# arcpy.Delete_management('temp_aqua')

# # buffer points 100m
# arcpy.Buffer_analysis('temp_aqua_select', 'temp_buffer', 100) # about the size of a tenure???
# # merge
# arcpy.Merge_management(['temp_buffer', 'temp_tenures_shellfish'], 'temp_merge')
# # erase with land
# arcpy.PairwiseErase_analysis('temp_merge', land, 'temp_erase')

# # DO A DISSOLVE HERE. There are many overlapping polygons. Doesn't make sense to have them separate
# # if it is just an area based feature. It doesn't matter for the overall polygon to raster, but it
# # does matter when buffering.
# # HOWEVER, this might change once I get gate values.
# arcpy.Dissolve_management('temp_erase', 'temp_diss', multi_part='SINGLE_PART')
# arcpy.MultipartToSinglepart_management('temp_diss', 'temp_ms')

# selLocation('temp_ms', buffer, outname)

# fcs = arcpy.ListFeatureClasses('temp_*')
# for fc in fcs:
#     arcpy.Delete_management(fc)

# bufferOrphanErase(outname, buffer)
# outfc = featureToRaster(outname, value_field)
# outfc = bufferRingsToRaster(outname, buffer)
# estuaryThreats(outfc, outname)
# cleanUp(outname)



# #### Finfish
# outname = 'thrt_mar_aquaculture_finfish'

# where = """TEN_PURPOS = 'AQUACULTURE' And TEN_SUBPRP = 'FIN FISH'"""
# arcpy.MakeFeatureLayer_management(tenures, 'temp_tenures', where_clause=where)
# arcpy.CopyFeatures_management('temp_tenures', 'temp_tenures_finfish')
# arcpy.Delete_management('temp_tenures')

# where = """region = 'BC' And type = 'finfish'"""
# arcpy.MakeFeatureLayer_management(aquaculture, 'temp_aqua', where_clause=where)

# # remove points overlapping a tenure polygon
# arcpy.SelectLayerByLocation_management('temp_aqua', 'INTERSECT', 'temp_tenures_finfish')
# arcpy.SelectLayerByLocation_management('temp_aqua', selection_type='SWITCH_SELECTION') 
# arcpy.CopyFeatures_management('temp_aqua', 'temp_aqua_select')
# arcpy.Delete_management('temp_aqua')

# # buffer points 100m
# arcpy.Buffer_analysis('temp_aqua_select', 'temp_buffer', 100)
# # merge
# arcpy.Merge_management(['temp_buffer', 'temp_tenures_finfish'], 'temp_merge')
# # erase with land
# arcpy.PairwiseErase_analysis('temp_merge', land, 'temp_erase')

# # MIGHT NEED TO REMOVE THIS DISSOLVE IF I no longer do it as an area based feature
# arcpy.Dissolve_management('temp_erase', 'temp_diss', multi_part='SINGLE_PART')
# arcpy.MultipartToSinglepart_management('temp_diss', 'temp_ms')

# selLocation('temp_ms', buffer, outname)

# fcs = arcpy.ListFeatureClasses('temp_*')
# for fc in fcs:
#     arcpy.Delete_management(fc)

# bufferOrphanErase(outname, buffer)
# outfc = featureToRaster(outname, value_field)
# outfc = bufferRingsToRaster(outname, buffer)
# estuaryThreats(outfc, outname)
# cleanUp(outname)



##################
# New AQUACULTURE data
# (1) Finfish
# (2) Shellfish

buffer = 2000
value_field = 'total_value'
# This is now the definitive source and we will not combine with the Tenures data (which may not be
# active) or with the floating structures data, which may misidentify aquaculture structures.
xls = os.path.join(threats_dir, 'aquaculture/Robb_2011-2021 Aquaculture Statistical Data.xlsx')

# EXPLORE:

# Remove any not within 2km of estuaries
# This is just for exploratory purposes. In the end I will use the full dataset because I want to 
# buffer by 2km from the edge of the tenure polygons and not just the points.
df = pd.read_excel(xls, 'Export_Search_Loaded_Annual_Sta')
x = np.array(np.rec.fromrecords(df.values)) 
names = df.dtypes.index.tolist() 
x.dtype.names = tuple(names)
arcpy.da.NumPyArrayToTable(x, os.path.join(arcpy.env.workspace, f'temp_tbl'))
arcpy.XYTableToPoint_management('temp_tbl', 'temp_aquaculture', 'Longitude', 'Latitude')
arcpy.Delete_management('temp_tbl')
arcpy.MakeFeatureLayer_management('temp_aquaculture', 'temp_lyr')
arcpy.SelectLayerByLocation_management('temp_lyr', 'INTERSECT', estuaries, search_distance=2000)
arcpy.CopyFeatures_management('temp_lyr', 'temp_aqua_select')
arcpy.Delete_management(['temp_aquaculture', 'temp_lyr'])
field_names = [i.name for i in arcpy.ListFields('temp_aqua_select') if i.type != 'OID'] 
cursor = arcpy.da.SearchCursor('temp_aqua_select', field_names) 
df = pd.DataFrame(data=[row for row in cursor], columns=field_names) 
arcpy.Delete_management('temp_aqua_select')
df = df.drop(columns={'Shape'})
df.to_csv(os.path.join(root, 'TEMP_aqua.csv'))

# Disregard transfer values
# There are very few seeding values and they are in different units, so just disregard. Also, all of
# those facilities also have other types of values to represent their impact.
# Blanks mean no production and that a facility may just be growing that year and not necessarily
# inactive.
# Despite the different units of reporting, there are columns that are in dollar value for both 
# market value and restocking value. Just use these values.
# Groupby and add market and restocking values. Do not average, as this could misprepresent what is 
# happening in zero value years.
# How to spatially represent farms with all zeros? Perhaps take the minimum non-zero value and apply
# that? Even though that may not represent actual impact. It wouldn't be fair to apply a value 
# greater or less than a real value I am actually using.
# First see if there end up being any that within 2 km of estuaries.



### FINFISH
outname = 'thrt_mar_aquaculture_finfish'

df = pd.read_excel(xls, 'Export_Search_Loaded_Annual_Sta')
df_ff = df[df['Licence Type']=='Marine Finfish']
df_ff = df_ff.groupby(['Facility Ref No']).agg({
    'Facility Name': 'first',
    'Landfile No': 'first',
    'Latitude': 'first',
    'Longitude': 'first',
    'Food Market Sales: Total Value (CDN)': 'sum',
    'Restocking/Ongrowing Sales: Total Value (CDN)': 'sum'
}).reset_index()
df_ff['total_value'] = df_ff['Food Market Sales: Total Value (CDN)'] + df_ff['Restocking/Ongrowing Sales: Total Value (CDN)']
df_ff = df_ff[['Facility Ref No', 'Facility Name', 'Landfile No', 'Latitude', 'Longitude', 'total_value']]

# Convert to feature class
x = np.array(np.rec.fromrecords(df_ff.values)) 
names = df_ff.dtypes.index.tolist() 
x.dtype.names = tuple(names)
arcpy.da.NumPyArrayToTable(x, os.path.join(arcpy.env.workspace, f'temp_tbl'))
arcpy.XYTableToPoint_management('temp_tbl', 'temp_aquaculture_ff', 'Longitude', 'Latitude')
arcpy.Delete_management('temp_tbl')

# A spatial select of the points with the tenures polys shows that there are 17 of the 130 that do
# not overlap.

# Join with tenures based on Landfile_No
tenures = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/TA_CRT_SVW_polygon')
where = """TEN_PURPOS = 'AQUACULTURE' And TEN_SUBPRP = 'FIN FISH'"""
arcpy.MakeFeatureLayer_management(tenures, 'temp_tenures', where_clause=where)
arcpy.CopyFeatures_management('temp_tenures', 'tenures_ff')
arcpy.AddField_management('tenures_ff', 'LN', 'LONG')
arcpy.CalculateField_management('tenures_ff', 'LN', 'int(!CL_FILE!)')

arcpy.MakeFeatureLayer_management('temp_aquaculture_ff', 'temp_ff')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_ff', 'Landfile_No', 'tenures_ff', 'LN', 'KEEP_ALL')
arcpy.CopyFeatures_management('temp_ff', 'temp_ff_tenures_joined')

# Select those that are null for tenures and make a new feature layer
# Buffer these points by 250 meters
arcpy.MakeFeatureLayer_management('temp_ff_tenures_joined', 'temp_lyr', 'LN IS NULL')
arcpy.Buffer_analysis('temp_lyr', 'temp_buff', 10)

# New join of original points to Tenures to get polygons and copy to new fc (only keep common)
arcpy.MakeFeatureLayer_management('tenures_ff', 'temp_t')
arcpy.AddJoin_management('temp_t', 'LN', 'temp_aquaculture_ff', 'LandFile_No', 'KEEP_COMMON')
arcpy.CopyFeatures_management('temp_t', 'temp_tenpts_common')

# Remove duplicates (some tenures had a polygon for both the application and the tenure)
arcpy.DeleteIdentical_management('temp_tenpts_common', 'Landfile_No')

# Merge with null buff polygons created above
arcpy.Merge_management(['temp_buff', 'temp_tenpts_common'], 'temp_merge')

# Spatial select in desktop of those within 2km of estuaries. Are there any left that have all zero 
# values? Yes there are 3.
# Go back and look at the spreadsheet
# Look at satellite imagery.
# There are no structures visible in satellite imagery for all 3 of these, so I will remove them.
sel_loc = arcpy.SelectLayerByLocation_management('temp_merge', 'INTERSECT', estuaries, 2000)
sel = arcpy.SelectLayerByAttribute_management(sel_loc, 'SUBSET_SELECTION', 'total_value > 0')
arcpy.CopyFeatures_management(sel, 'temp_final')

# Delete temp files
arcpy.Delete_management(['temp_aquaculture_ff', 'temp_tenures', 'tenures_ff', 'temp_aquaculture_ff',
                          'temp_ff', 'temp_ff_tenures_joined', 'temp_lyr', 'temp_buff', 'temp_t',
                          'temp_tenpts_common', 'temp_merge'])

# send to common functions
selLocation('temp_final', buffer, outname)
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)

arcpy.Delete_management('temp_final')



# SHELLFISH
outname = 'thrt_mar_aquaculture_shellfish'

df = pd.read_excel(xls, 'Export_Search_Loaded_Annual_Sta')
df_ff = df[df['Licence Type']=='Shellfish']
df_ff = df_ff.groupby(['Facility Ref No']).agg({
    'Facility Name': 'first',
    'Landfile No': 'first',
    'Latitude': 'first',
    'Longitude': 'first',
    'Food Market Sales: Total Value (CDN)': 'sum',
    'Restocking/Ongrowing Sales: Total Value (CDN)': 'sum'
}).reset_index()
df_ff['total_value'] = df_ff['Food Market Sales: Total Value (CDN)'] + df_ff['Restocking/Ongrowing Sales: Total Value (CDN)']
df_ff = df_ff[['Facility Ref No', 'Facility Name', 'Landfile No', 'Latitude', 'Longitude', 'total_value']]

# Landfile Number has mix of text and ints which screws up the join
whereText = ~df_ff['Landfile No'].apply(lambda x: isinstance(x, (int, float))) 
df_ff['Landfile No'][whereText] = None
df_ff['Landfile No'] = df_ff['Landfile No'].infer_objects()

# Convert to feature class
x = np.array(np.rec.fromrecords(df_ff.values)) 
names = df_ff.dtypes.index.tolist() 
x.dtype.names = tuple(names)
arcpy.da.NumPyArrayToTable(x, os.path.join(arcpy.env.workspace, f'temp_tbl'))
arcpy.XYTableToPoint_management('temp_tbl', 'temp_aquaculture_sf', 'Longitude', 'Latitude')
arcpy.Delete_management('temp_tbl')

# A spatial select of the points with the tenures polys shows that there are 69 of the 538 that do
# not overlap.

# Join with tenures based on Landfile_No
tenures = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/TA_CRT_SVW_polygon')
where = """TEN_PURPOS = 'AQUACULTURE' And TEN_SUBPRP = 'SHELL FISH'"""
arcpy.MakeFeatureLayer_management(tenures, 'temp_tenures', where_clause=where)
arcpy.CopyFeatures_management('temp_tenures', 'tenures_sf')
arcpy.AddField_management('tenures_sf', 'LN', 'LONG')
arcpy.CalculateField_management('tenures_sf', 'LN', 'int(!CL_FILE!)')

arcpy.MakeFeatureLayer_management('temp_aquaculture_sf', 'temp_sf')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_sf', 'Landfile_No', 'tenures_sf', 'LN')
arcpy.CopyFeatures_management('temp_sf', 'temp_sf_tenures_joined')

# Select those that are null for tenures and make a new feature layer
# Buffer these points by 250 meters
arcpy.MakeFeatureLayer_management('temp_sf_tenures_joined', 'temp_lyr', 'LN IS NULL')
arcpy.Buffer_analysis('temp_lyr', 'temp_buff', 10)
# THE BUFFER needs to be kept small for this feature or else we get too much overlap.

# New join of original points to Tenures to get polygons and copy to new fc (only keep common)
arcpy.MakeFeatureLayer_management('tenures_sf', 'temp_t')
arcpy.AddJoin_management('temp_t', 'LN', 'temp_aquaculture_sf', 'LandFile_No', 'KEEP_COMMON')
arcpy.CopyFeatures_management('temp_t', 'temp_tenpts_common')

# Remove duplicates (some tenures had a polygon for both the application and the tenure)
arcpy.DeleteIdentical_management('temp_tenpts_common', 'Landfile_No')

# Merge with null buff polygons created above
arcpy.Merge_management(['temp_buff', 'temp_tenpts_common'], 'temp_merge')

# There is a tenure that is multipart which results in an error when doing the bufferOrphanErase
arcpy.MultipartToSinglepart_management('temp_merge', 'temp_multising')

# Spatial select in desktop of those within 2km of estuaries. Are there any left that have all zero 
# values? Yes there are 21.
sel_loc = arcpy.SelectLayerByLocation_management('temp_multising', 'INTERSECT', estuaries, 2000)
arcpy.CopyFeatures_management(sel_loc, 'temp_selloc')

# Go back and look at the spreadsheet
# Look at satellite imagery.
# Many ones with zero values were directly adjacent to other larger operation (e.g. Fanny Bay), so
# I did not include these since they are likely accounted for elsewhere.
# Otherwise, there were only two records that had visible structures (Facility Ref No: 726, 1622),
# so for simplicity and to not make assumptions of value, I will just remove all that are 0.
sel = arcpy.SelectLayerByAttribute_management('temp_selloc', 'NEW_SELECTION', 'total_value > 0')
arcpy.CopyFeatures_management(sel, 'temp_nonzero')
arcpy.CopyFeatures_management(sel, 'temp_final')

# For multipart features, split out their total value by area so that I am not double counting
# First, get list of duplicates
dups = []
fids = []
with arcpy.da.SearchCursor('temp_final', ['Facility_Ref_No', 'OBJECTID_12_13']) as cursor1:
    for row1 in cursor1:
        refno = row1[0]
        count = 0
        with arcpy.da.SearchCursor('temp_final', ['Facility_Ref_No']) as cursor2:
            for row2 in cursor2:
                if row2[0] == refno:
                    count += 1
            if count > 1:
                fids.append(row1[1])
                if refno not in dups:
                    dups.append(refno)
            cursor2.reset()
# Get total areas:
total_areas = []
for d in dups:
    sel = arcpy.SelectLayerByAttribute_management('temp_final', 'NEW_SELECTION', 'Facility_Ref_No = {}'.format(d))
    summed_total = sum((r[0] for r in arcpy.da.SearchCursor(sel, ['Shape_Area'])))
    total_areas.append(summed_total)
# Calculate
with arcpy.da.UpdateCursor('temp_final', ['OBJECTID_12_13', 'Facility_Ref_No', 'Shape_Area', 'total_value']) as cursor:
    for row in cursor:
        if row[0] in fids:
            index = dups.index(row[1])
            proportion = row[2] / total_areas[index]
            row[3] = row[3] * proportion
            cursor.updateRow(row)

# Delete all the duplicate OBJECTID fields for clarity
fields = arcpy.ListFields('temp_final', 'OBJECTID*')
oid_field = arcpy.ListFields('temp_final', field_type='OID')[0].name
for field in fields:
    if field.name != oid_field:
        arcpy.DeleteField_management('temp_final', field.name)

# Calculate priority field here to deal with overlapping polygon issue when converting to raster
# (see detailed notes in featureToRaster function)
# There are some very small features that are completely contained by other features. When
# converting to raster, these end up disappearing as they other overlapping feature has the larger
# area in the raster cell. However, with a priority field, I can set the smaller features as more
# important.
arcpy.AddField_management('temp_final', 'priority', 'DOUBLE')
all_rows = [i[0] for i in arcpy.da.SearchCursor('temp_final',['Shape_Area'])]
min_val = min(all_rows)
with arcpy.da.UpdateCursor('temp_final', ['priority', 'Shape_Area']) as cursor:
    for row in cursor:
        row[0] = min_val / row[1]
        cursor.updateRow(row)

# Delete temp files
arcpy.Delete_management(['temp_aquaculture_sf', 'temp_tenures', 'tenures_sf', 'temp_sf', 
                         'temp_sf_tenures_joined', 'temp_lyr', 'temp_buff', 'temp_t',
                         'temp_tenpts_common', 'temp_merge', 'temp_selloc', 'temp_multising',
                         'temp_nonzero'])

# send to common functions
selLocation('temp_final', buffer, outname)
bufferOrphanErase(outname, buffer)
outfc = featureToRaster(outname, value_field)
outfc = bufferRingsToRaster(outname, buffer)
estuaryThreats(outfc, outname)
cleanUp(outname)

arcpy.Delete_management('temp_final')



#######################################
#######################################
# Commercial catch data

out_name = 'thrt_mar_fishingcomm_{}'
buffer = 0
value_field = 'total_kg_DST'

catch_com = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/all_fisheries_filtered_gridded')

# view list of unique values
names = []
with arcpy.da.SearchCursor(catch_com, ['name_label']) as cursor:
    for row in cursor:
        if row[0] not in names:
            names.append(row[0])

# From Carrie:
# Bottom Trawl - trawl
# Geoduck - pressure hose
# GSU - dive
# Halibut -hook and line
# Halibut and Sablefish combo - hook and line
# Lingcod - hook and line
# Midwater trawl - trawl
# Prawn and shrimp by trap - trap
# Red sea urchin - dive
# Rockfish -hook and line
# Sablefish - trap and hook and line
# Sea Cucumber - dive
# Shrimp by Trawl - trawl

atts = {
    'trawl': ['Bottom Trawl', 'Midwater Trawl', 'Shrimp by Trawl'],
    'pressurehose': ['Geoduck'],
    'hookandline': ['Halibut', 'Halibut and Sablefish combo', 'Lingcod', 'Rockfish', 'Sablefish'],
    'dive': ['Green Sea Urchin', 'Red Sea Urchin', 'Sea Cucumber'],
    'trap': ['Prawn and Shrimp by Trap']
}

# separate out
for key in atts:

    if len(atts[key]) > 1:
        where = f"""name_label IN {tuple(atts[key])}"""
    else:
        where = f""""name_label" = '{atts[key][0]}'"""
    arcpy.MakeFeatureLayer_management(
        catch_com,
        'temp_lyr',
        where_clause=where)
    arcpy.CopyFeatures_management('temp_lyr', 'temp_lyrout')
    arcpy.Delete_management('temp_lyr')

    # calculate total and dissolve
    # add field for total_kg
    # calculate from total_kg or total_kg_filtered
    arcpy.AddField_management('temp_lyrout', 'total_kg_DST', 'FLOAT')
    with arcpy.da.UpdateCursor('temp_lyrout', ['total_kg_unfiltered', 'total_kg_filtered', 'total_kg_DST']) as cursor:
        for row in cursor:
            if row[0] > 0:
                row[2] = row[0]
            else:
                row[2] = row[1]
            cursor.updateRow(row)
    outfc = f'temp_{key}'
    arcpy.Dissolve_management('temp_lyrout', outfc, ['PU_ID'], [['total_kg_DST', 'SUM']], 'SINGLE_PART')
    arcpy.AlterField_management(outfc, 'SUM_total_kg_DST', 'total_kg_DST', 'total_kg_DST')
    arcpy.Delete_management('temp_lyrout')

for key in atts:
    outname = out_name.format(key)
    selLocation(f'temp_{key}', buffer, outname)
    arcpy.Delete_management(f'temp_{key}')
    outfc = featureToRaster(outname, value_field)
    estuaryThreats(outfc, outname)
    cleanUp(outname)



#######################################
#######################################
# Salmon catch data

salmon_ds = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/SLPSA_1kmGrid_{}')
gears = ['Gill', 'Seine', 'Troll']
buffer = 0
value_field = 'SUM_kg_all'

for gear in gears:
    ds = salmon_ds.format(gear)
    outname = f'thrt_mar_fishingcomm_salmon_{gear.lower()}'
    arcpy.Dissolve_management(ds, 'temp_diss', ['PU_ID'], [['kg_all', 'SUM']], 'SINGLE_PART')

    selLocation(f'temp_diss', buffer, outname)
    arcpy.Delete_management(f'temp_diss')
    outfc = featureToRaster(outname, value_field)
    estuaryThreats(outfc, outname)
    cleanUp(outname)



#########################################
#########################################
# Rec fishing

buffer = 0
value_field = 'area_based'

fc_name = 'bcmca_hu_sportfish_{}_data'
datasets = ['anadromous', 'crab', 'groundfish', 'prawnandshrimp']

for ds in datasets:
    fc = fc_name.format(ds)
    outname = f'thrt_mar_fishingrec_{ds}'
    recfish = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb', fc)
    arcpy.Dissolve_management(recfish, 'temp_diss', multi_part='SINGLE_PART')
    
    selLocation('temp_diss', buffer, outname)
    arcpy.Delete_management('temp_diss')
    outfc = featureToRaster(outname, value_field)
    estuaryThreats(outfc, outname)
    cleanUp(outname)



#######################################
#######################################
# Recreational boating

# This would be 1956 features to buffer. That will take about 7 hours, so I guess I could just
# do it overnight. However, the shipping vessel traffic will take 9 days. To stay somewhat 
# consistent with the general category of "boat traffic", just process these with the spatial join.
# Cathryn Murray's team did not do KD, so a spatial join should be fine.

buffer = 2000
outname = 'thrt_mar_recboating'
value_field = 'Rec_Pred'

recbt = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/ALLBC_RecBoat_Predictions')

where = """Rec_Pred > 0"""
arcpy.MakeFeatureLayer_management(recbt, 'temp', where_clause=where)

arcpy.SpatialJoin_analysis(
    'temp',
    estuaries,
    outname,
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='WITHIN_A_DISTANCE',
    search_radius=buffer
)
arcpy.Delete_management('temp')
# It's a One to Many relationship, so cells will be duplicated if they are within range of multiple
# estuaries.

arcpy.Dissolve_management(outname, 'temp_diss', ['EST_NO'], [['Rec_Pred', 'SUM']], 'MULTI_PART')
arcpy.AlterField_management('temp_diss', 'SUM_Rec_Pred', 'SUM', 'SUM')
arcpy.TableToTable_conversion('temp_diss', gdb_out, outname)
arcpy.Delete_management('temp_diss')




#######################################
#######################################
# Shipping Vessel traffic

# Too many features to buffer and do distance decay.
# Cathryn Murray's team did not do KD, so a spatial join should be fine.

outname = 'thrt_mar_shipping'
buffer = 30000
value_field = 'YEARLY_COUNT'

shipping = os.path.join(threats_dir, 'TC_FusedAIS_DensityGrids_2021.gdb\TC_FusedAIS_Canada_Density_2021_S60_9km2_N60_25km2')

where = """YEARLY_COUNT > 0"""
arcpy.MakeFeatureLayer_management(shipping, 'temp', where_clause=where)
arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(3005)
arcpy.SpatialJoin_analysis(
    'temp',
    estuaries,
    outname,
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='WITHIN_A_DISTANCE',
    search_radius=buffer
)
arcpy.Delete_management('temp')
# It's a One to Many relationship, so cells will be duplicated if they are within range of multiple
# estuaries.

arcpy.Dissolve_management(outname, 'temp_diss', ['EST_NO'], [[value_field, 'SUM']], 'MULTI_PART')
arcpy.AlterField_management('temp_diss', 'SUM_YEARLY_COUNT', 'SUM', 'SUM')
arcpy.TableToTable_conversion('temp_diss', gdb_out, outname)
arcpy.Delete_management('temp_diss')




#######################################
#######################################
# Aquatic invasive species

# calculate as species richness

buffer = 0
outname = 'thrt_mar_aquaticinvasives'

aqinv = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/WHSE_FISH_FISH_AQUATIC_INVASIVE_SPCS_SP')
grcrb_event = os.path.join(threats_dir, 'greenCrab/dwca-egc_surveys-v1.0/event.txt')
grcrb_occur = os.path.join(threats_dir, 'greenCrab/dwca-egc_surveys-v1.0/occurrence.txt')
# extendedmeasurementtxt is not needed, I think. It has metrics like soak time and size, so
# ids are duplicated

# join
df_1 = pd.read_csv(grcrb_event, sep='\t')
df_2 = pd.read_csv(grcrb_occur, sep='\t')
df = df_2.merge(df_1, on='eventID')

# filter for: Carcinus maenas (European Green Crab)
df = df[df.scientificName == 'Carcinus maenas']
# there is an occurrence status column, but all are present

x = np.array(np.rec.fromrecords(df.values)) 
names = df.dtypes.index.tolist() 
x.dtype.names = tuple(names)
arcpy.da.NumPyArrayToTable(x, os.path.join(arcpy.env.workspace, f'temp_tbl'))
arcpy.XYTableToPoint_management('temp_tbl', 'temp_greencrab', 'decimalLongitude', 'decimalLatitude')

arcpy.Merge_management([aqinv, 'temp_greencrab'], 'temp_merge')

arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(3005)
arcpy.SpatialJoin_analysis(
    'temp_merge',
    estuaries,
    'temp_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
)

arcpy.AddField_management('temp_sjoin', 'species', 'TEXT')
with arcpy.da.UpdateCursor('temp_sjoin', ['SCIENTIFIC_NAME', 'scientificName', 'species']) as cursor:
    for row in cursor:
        if row[0] is not None:
            row[2] = row[0]
        elif row[1] is not None:
            row[2] = row[1]
        cursor.updateRow(row)

arcpy.Dissolve_management('temp_sjoin', outname, ['EST_NO', 'species'])
arcpy.Dissolve_management(outname, 'temp_diss', ['EST_NO'], [['species', 'COUNT']])
arcpy.TableToTable_conversion('temp_diss', gdb_out, outname)
arcpy.AlterField_management(os.path.join(gdb_out, outname), 'COUNT_species', 'SUM', 'SUM')

arcpy.Delete_management('temp_greencrab')
arcpy.Delete_management('temp_tbl')
arcpy.Delete_management('temp_merge')
arcpy.Delete_management('temp_sjoin')
arcpy.Delete_management('temp_diss')



#######################################
#######################################
# Anchor scours

# Data in email
# Line features. Selina did a 200m buffer on either side to turn it into a polygon.

outname = 'thrt_mar_anchorscours'
buffer = 0  # however, these are line features. CCM's team did a 200m buffer to turn it into a 
            # polygon. This also accounts for chain marks and swing as well(?)
value_field = 'area_based'

anch_scours = os.path.join(threats_dir, 'AnchorageMarksFinal2022\AnchorScours_WGS84.shp')

arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(3005)
arcpy.PairwiseBuffer_analysis(anch_scours, 'temp_buff', 200)
arcpy.PairwiseDissolve_analysis('temp_buff', 'temp_diss', multi_part='SINGLE_PART')

selLocation('temp_diss', buffer, outname)
outfc = featureToRaster(outname, value_field)
estuaryThreats(outfc, outname)
cleanUp(outname)
arcpy.Delete_management('temp_buff')
arcpy.Delete_management('temp_diss')






#########################################
#
# Climate threats
#
#########################################


#########################################
#########################################
# Sea level change

outname = 'thrt_cli_sealevelrise'

slr = os.path.join(threats_dir, 'of_8551\CANCOAST_SEALEVELCHANGE_2006_2099_V1\CANCOAST_SEALEVELCHANGE_2006_2099_V1.shp')

# Spatial join to estuaries that the lines touch
arcpy.MakeFeatureLayer_management(slr, 'temp')
arcpy.SpatialJoin_analysis(
    'temp',
    estuaries,
    outname,
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT'
)
arcpy.Delete_management('temp')

# average values per estuary
arcpy.Dissolve_management(
    outname,
    'temp_diss',
    'EST_NO',
    [['SLChange','MEAN']]
)
# join with estuaries to account for those that did not have bordering lines
arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_est', 'EST_NO', 'temp_diss', 'EST_NO')
arcpy.CopyFeatures_management('temp_est', 'temp_join')

# For the 7 estuaries without values add in the values of the closest line
s_values = {
    215:0.697886,
    216:0.697886,
    217:0.697886,
    268:0.620972,
    312:0.481372,
    313:0.481372,
    441:0.697886
}

with arcpy.da.UpdateCursor('temp_join', ['EST_NO', 'MEAN_SLChange']) as cursor:
    for row in cursor:
        if row[0] in s_values:
            row[1] = s_values[row[0]]
            cursor.updateRow(row)

arcpy.TableToTable_conversion('temp_join', gdb_out, outname)
arcpy.AlterField_management(os.path.join(gdb_out, outname), 'MEAN_SLChange', 'SUM', 'SUM')

arcpy.Delete_management('temp_diss')
arcpy.Delete_management('temp_est')
arcpy.Delete_management('temp_join')



#########################################
#########################################
# Change in temperature

# climatebc.ca/SpatialData

# can choose time period (e.g. 2070-2100) and emissions scenario (e.g. SSP2-4.5)
# yearly, season, or month
# can choose from 13 models, but climate BC will do a ensemble of these.
# see:
# https://bcgov-env.shinyapps.io/cmip6-BC/
# https://climatebc.ca/Help

# Download here:
# https://climatebc.ca/SpatialData

# Patrick got feedback recently to use the 4.5 scenario. Above that is too extreme.
# 13GCMs_ensemble_ssp245_2071-2100
# summer mean temperature (Tave_sm)

# compare this to:
# Normal_1981_2010

outname = 'thrt_cli_tempchange'

temp_hist = os.path.join(threats_dir, 'climatebc/Tave_sm_1981_2010.tif')
temp_futu = os.path.join(threats_dir, 'climatebc/Tave_sm_2071_2100_ssp245.tif')

# subtract
outMinus = Raster(temp_futu) - Raster(temp_hist)
outMinus.save('temp_minus')

# raster to points
arcpy.RasterToPoint_conversion('temp_minus', 'temp_pts')

# spatial join
arcpy.SpatialJoin_analysis(
    estuaries,
    'temp_pts',
    'temp_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_ALL',
    match_option='INTERSECT',
    search_radius='1000 METERS'
)

# dissolve and average
arcpy.Dissolve_management('temp_sjoin', outname, 'EST_NO', [['grid_code', 'MEAN']])

# copy, add field and divide by 10 (ClimateBC has all temp value multiplied by 10)
arcpy.AddField_management(outname, 'temperature_change', 'FLOAT')
with arcpy.da.UpdateCursor(outname, ['MEAN_grid_code', 'temperature_change']) as cursor:
    for row in cursor:
        row[1] = row[0] / 10.0
        cursor.updateRow(row)

arcpy.TableToTable_conversion(outname, gdb_out, outname)
arcpy.AlterField_management(os.path.join(gdb_out, outname), 'temperature_change', 'SUM', 'SUM')

# clean up
arcpy.Delete_management('temp_minus')
arcpy.Delete_management('temp_pts')
arcpy.Delete_management('temp_sjoin')



#########################################
#########################################
# Change in precipitation

# climatebc.ca/SpatialData

# Download here:
# https://climatebc.ca/SpatialData

# Patrick got feedback recently to use the 4.5 scenario. Above that is too extreme.
# 13GCMs_ensemble_ssp245_2071-2100
# Mean annual precipitation (MAP)
# compare this to:
# Normal_1981_2010

outname = 'thrt_cli_precipchange'

precip_hist = os.path.join(threats_dir, 'climatebc/MAP_precip_1981_2010.tif')
precip_futu = os.path.join(threats_dir, 'climatebc/MAP_precip_2071_2100_ssp245.tif')

# subtract
outMinus = Raster(precip_futu) - Raster(precip_hist)
outMinus.save('temp_minus')

# raster to points
arcpy.RasterToPoint_conversion('temp_minus', 'temp_pts')

# This is just for the area near the estuary. Without some more advanced flow modeling I wouldn't
# want to do it for the entire watershed.

# spatial join
arcpy.SpatialJoin_analysis(
    estuaries,
    'temp_pts',
    'temp_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_ALL',
    match_option='INTERSECT',
    search_radius='1000 METERS'
)

# dissolve and average
arcpy.Dissolve_management('temp_sjoin', outname, 'EST_NO', [['grid_code', 'MEAN']])

# copy, add field and divide by 10 (ClimateBC has all temp value multiplied by 10)
arcpy.AddField_management(outname, 'precip_change', 'FLOAT')
with arcpy.da.UpdateCursor(outname, ['MEAN_grid_code', 'precip_change']) as cursor:
    for row in cursor:
        row[1] = row[0] / 10.0
        cursor.updateRow(row)

arcpy.TableToTable_conversion(outname, gdb_out, outname)
arcpy.AlterField_management(os.path.join(gdb_out, outname), 'precip_change', 'SUM', 'SUM')

# clean up
arcpy.Delete_management('temp_minus')
arcpy.Delete_management('temp_pts')
arcpy.Delete_management('temp_sjoin')



#########################################
#########################################
# Stream temperature

# From Dan Weller. See his email for more details on the dataset.

# "The predictions are August means (°C) averaged over 20-year time periods.  We used 6 CMIP5-era 
# GCMs for our future scenarios.
# Tw8_0_00_0: 1981-2000 (historic)
# Tw8_0_00_1: 2001-2020 (historic)
# Tw8_9_45_4: ensemble average, RCP 4.5, 2061-2080
# Tw8_9_45_5: ensemble average, RCP 4.5, 2081-2100


outname = 'thrt_cli_streamtempchange'

strpts = os.path.join(threats_dir, 'streamTempChange_DanWeller/streamTempChange_DanWeller.gdb/Tw_preds_discharge_points_20230412')

arcpy.CopyFeatures_management(strpts, 'temp_copy')
arcpy.AddField_management('temp_copy', 'stream_temp_diff', 'DOUBLE')
with arcpy.da.UpdateCursor('temp_copy', ['Tw8_0_00_0', 'Tw8_9_45_5', 'stream_temp_diff']) as cursor:
    for row in cursor:
        row[2] = row[1] - row[0]
        cursor.updateRow(row)

# spatial join
arcpy.SpatialJoin_analysis(
    estuaries,
    'temp_copy',
    'temp_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_ALL',
    match_option='INTERSECT',
    search_radius='15 METERS'
)

# dissolve and average
arcpy.Dissolve_management('temp_sjoin', outname, 'EST_NO', [['stream_temp_diff', 'MEAN']])

# there are 3 estuaries that do not have points that are close. Manually add in the values for the
# closest points
s_values = {
    206:2.128304, # not any points directly upstream
    363:2.012388, # this point is quite far away and actually below the estuary. There is nothing else upstream.
    431:3.152508, # 
}

with arcpy.da.UpdateCursor(outname, ['EST_NO', 'MEAN_stream_temp_diff']) as cursor:
    for row in cursor:
        if row[0] in s_values:
            row[1] = s_values[row[0]]
            cursor.updateRow(row)

arcpy.TableToTable_conversion(outname, gdb_out, outname)
arcpy.AlterField_management(os.path.join(gdb_out, outname), 'MEAN_stream_temp_diff', 'SUM', 'SUM')

arcpy.Delete_management('temp_copy')
arcpy.Delete_management('temp_sjoin')

