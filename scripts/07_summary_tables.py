# Summary tables and stats






import os
import pandas as pd
import numpy as np
import arcpy
from arcgis.features import GeoAccessor, GeoSeriesAccessor 

root = r'C:/Users/cristianij/Documents/Projects/Estuary_threats/spatial'


#############################################################################
# Table of activity raw values. This will go on open data, so needs to be clean.

csv = os.path.join(root, 'estuary_threats_RAW.csv')
ests = pd.read_csv(csv)

# get EST_NAME and CLUSTER
csv_name_clust = os.path.join(r'C:/Users/cristianij/Documents/Projects/Estuary_threats/scripts/estuaries_plotting_R', 'clusters_estuaries.csv')
name_clust = pd.read_csv(csv_name_clust)
name_clust = name_clust[['name', 'cluster', 'EST_NO']]
name_clust = name_clust.rename(columns={'name':'estuary_name'})
ests = ests.merge(name_clust, 'left', 'EST_NO')
ests = ests.rename(columns={'EST_NO':'estuary_id'})

# use estuary_threats_log_pt feature class to get ACTIVITY COUNTS and CENTROIDS
fc = os.path.join(root, '02_working/threats_03associate.gdb/estuary_threats_log_pt')
sedf = pd.DataFrame.spatial.from_featureclass(fc)
df = sedf[['EST_NO', 'POINT_X', 'POINT_Y', 'ter_count', 'mar_count', 'count_total']]
df = df.rename(columns={
    'POINT_X':'estuary_centroid_x', 
    'POINT_Y':'estuary_centroid_y', 
    'ter_count':'activity_count_terrestrial',
    'mar_count':'activity_count_marine',
    'count_total':'activity_count_total',
    'EST_NO':'estuary_id'})
ests = ests.merge(df, 'left', 'estuary_id')

# Spatial join to BIOREGIONS
bioregions = r'C:\Users\cristianij\Documents\DATA_BASE\FederalMarineBioregions_GDB\FederalMarineBioregions.gdb\FederalMarineBioregions'
arcpy.MakeFeatureLayer_management(fc, 'temp_lyr')
arcpy.SpatialJoin_analysis(
    'temp_lyr', 
    bioregions, 
    'memory/sjoin',
    'JOIN_ONE_TO_ONE',
    'KEEP_ALL',
    match_option='CLOSEST')
sedf = pd.DataFrame.spatial.from_featureclass('memory/sjoin')
df = sedf[['EST_NO', 'NAME_E']]
df = df.rename(columns={'EST_NO':'estuary_id', 'NAME_E':'estuary_bioregion'})
ests = ests.merge(df, 'left', 'estuary_id')
arcpy.Delete_management(['temp_lyr', 'memory/sjoin'])

# For restricted data (aquaculture and dredging) - still include the column but leave it blank with 
# an asterisk in the header
ests['thrt_mar_aquaculture_shellfish'] = ''
ests['thrt_mar_aquaculture_finfish'] = ''
ests['thrt_mar_dredging'] = ''
ests = ests.rename(columns={
    'thrt_mar_aquaculture_shellfish':'thrt_mar_aquaculture_shellfish*',
    'thrt_mar_aquaculture_finfish':'thrt_mar_aquaculture_finfish*',
    'thrt_mar_dredging':'thrt_mar_dredging*',
})

# Renaming:
for col in ests.columns:
    if col.startswith('thrt'):
        ests = ests.rename(columns={col:col[5:]})
ests = ests.rename(columns={'area_watershedErase':'area_watershedEraseShorelineBuff', 'sho_shorelinedevelopment':'ter_shorelinedevelopment'})

# sort columns:
e = ests.columns.str.startswith("estuary")
c = ests.columns.str.startswith("cluster")
a = ests.columns.str.startswith("area")
m = ests.columns.str.startswith("mar_")
t = ests.columns.str.startswith("ter_")
ac = ests.columns.str.startswith("activity_")

cols = ests.columns[e].tolist() + ests.columns[c].tolist() + ests.columns[a].tolist() + ests.columns[m].tolist() + ests.columns[t].tolist() + ests.columns[ac].tolist()
ests = ests[cols]

ests.to_csv(os.path.join(root, 'out_tables/activities.csv'), index=False)


#############################################################################
# Table of eco data

csv = os.path.join(root, 'ecodata_{}.csv')

salar = csv.format('salmon_abundrich')
salcu = csv.format('salmon_CUs')
eul = csv.format('eulachon')
her = csv.format('herringspawnindex')
snd = csv.format('sandlanceHabitatProb')

birds = csv.format('estuaryImportanceBirds')

eg_bb = csv.format('eelgrassBiobands')
eg_pl = csv.format('eelgrassPolys')
kb_bb = csv.format('kelpBrownBiobands')
kc_bb = csv.format('kelpBrownCanopyBiobands')
kc_pl = csv.format('kelpBrownCanopyPolys')
ku_bb = csv.format('kelpGreenUlvaBiobands')
sm_bb = csv.format('saltmarshBiobands')
substrate_csv = os.path.join(root, 'ecodata_substrateType.csv')
rugo = csv.format('rugosityMean')
hab = csv.format('habitatCount')

crab = csv.format('crabProb')
prwn = csv.format('prawn')



# get df of estuary data from activities table
ests = pd.read_csv(os.path.join(root, 'out_tables/activities.csv'))
e = ests.columns.str.startswith("estuary")
c = ests.columns.str.startswith("cluster")
cols = ests.columns[e].tolist() + ests.columns[c].tolist() + ['area_estuary']
est = ests[cols]
est['EST_NO'] = est.estuary_id

# merge df
em = est.copy()




# read in dfs one by one

df = pd.read_csv(salar)
df = df.merge(est, 'right', 'EST_NO')
df = df[['EST_NO', 'Sum_Weight_KG', 'Sp_richness']]
df = df.fillna(0)
df = df.rename(columns={'Sum_Weight_KG':'salmon_biomass_kg', 'Sp_richness':'salmon_sprichness'})
df = df.astype({'salmon_biomass_kg':'int', 'salmon_sprichness':'int'})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(salcu)
df = df.merge(est, 'right', 'EST_NO')
df = df.groupby('EST_NO')['FREQUENCY'].sum().reset_index()
df = df.rename(columns={'FREQUENCY':'salmon_CUs'})
df = df.astype({'salmon_CUs':'int'})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(eul)
df = df.merge(est, 'right', 'EST_NO')
df = df.rename(columns={'eulachon_presence':'eulachon_presence'})
df = df[['EST_NO', 'eulachon_presence']]
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(her)
df = df.merge(est, 'right', 'EST_NO')
df = df[['EST_NO', 'biomass_MEAN']]
df = df.fillna(0)
df = df.rename(columns={'biomass_MEAN':'herring_spawn_biomass'})
df = df.astype({'herring_spawn_biomass':'int'})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(snd)
df = df.merge(est, 'right', 'EST_NO')
df = df[['EST_NO', 'MEAN_CR', 'MEAN_JH']]
df = df.round({'MEAN_CR':2, 'MEAN_JH':2})
df = df.rename(columns={'MEAN_CR':'psandlance_suitability_Robinson2021', 'MEAN_JH':'psandlance_suitability_huard2021'})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(crab)
df = df.merge(est, 'right', 'EST_NO')
df = df.rename(columns={'MEAN':'crab_dungeness_probability'})
df = df[['EST_NO', 'crab_dungeness_probability']]
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(prwn)
df = df.merge(est, 'right', 'EST_NO')
df = df.rename(columns={'records_count':'spot_prawn_count'})
df = df[['EST_NO', 'spot_prawn_count']]
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(birds)
df = df[['EST_NO', 'IMP_CL2019']]
df = df[df.IMP_CL2019 != 'not ranked']
df = df.rename(columns={'IMP_CL2019':'waterbird_importance'})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(eg_bb)
df = df.merge(est, 'right', 'EST_NO')
df = df.fillna(0)
#df['Eelgrass biobands'] = (df.length_div_estArea - df.length_div_estArea.min()) / (df.length_div_estArea.max() - df.length_div_estArea.min())
df['eelgrass_biobands_lengthDivEstArea'] = df.length_div_estArea
df = df[['EST_NO', 'eelgrass_biobands_lengthDivEstArea']]
#df = df.round({'eelgrass_biobands_lengthDivEstArea':4})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(eg_pl)
df = df.merge(est, 'right', 'EST_NO')
df = df.fillna(0)
df = df.rename(columns={'proportion_eelgrass':'eelgrass_polygons'})
df = df[['EST_NO', 'eelgrass_polygons']]
df = df.round({'Eelgrass polygons':4})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(kc_bb)
df = df.merge(est, 'right', 'EST_NO')
df = df.fillna(0)
#df['Canopy kelp biobands'] = (df.length_div_estArea - df.length_div_estArea.min()) / (df.length_div_estArea.max() - df.length_div_estArea.min())
df['canopyKelp_biobands_lengthDivEstArea'] = df.length_div_estArea
df = df[['EST_NO', 'canopyKelp_biobands_lengthDivEstArea']]
#df = df.round({'canopyKelp_biobands_lengthDivEstArea':4})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(kc_pl)
df = df.merge(est, 'right', 'EST_NO')
df = df.fillna(0)
df = df.rename(columns={'proportion_kelpcanopy':'canopyKelp_polygons'})
df = df[['EST_NO', 'canopyKelp_polygons']]
df = df.round({'canopyKelp_polygons':4})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(kb_bb)
df = df.merge(est, 'right', 'EST_NO')
df = df.fillna(0)
#df['Understory brown kelp biobands'] = (df.length_div_estArea - df.length_div_estArea.min()) / (df.length_div_estArea.max() - df.length_div_estArea.min())
df['understoryKelp_biobands_lengthDivEstArea'] = df.length_div_estArea
df = df[['EST_NO', 'understoryKelp_biobands_lengthDivEstArea']]
#df = df.round({'understoryKelp_biobands_lengthDivEstArea':4})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(ku_bb)
df = df.merge(est, 'right', 'EST_NO')
df = df.fillna(0)
#df['Understory green algae biobands'] = (df.length_div_estArea - df.length_div_estArea.min()) / (df.length_div_estArea.max() - df.length_div_estArea.min())
df['understoryGreenAlgae_biobands_lengthDivEstArea'] = df.length_div_estArea
df = df[['EST_NO', 'understoryGreenAlgae_biobands_lengthDivEstArea']]
#df = df.round({'understoryGreenAlgae_biobands_lengthDivEstArea':4})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(sm_bb)
df = df.merge(est, 'right', 'EST_NO')
df = df.fillna(0)
#df['Saltmarsh biobands'] = (df.length_div_estArea - df.length_div_estArea.min()) / (df.length_div_estArea.max() - df.length_div_estArea.min())
df['saltmarsh_biobands_lengthDivEstArea'] = df.length_div_estArea
df = df[['EST_NO', 'saltmarsh_biobands_lengthDivEstArea']]
#df = df.round({'saltmarsh_biobands_lengthDivEstArea':4})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(substrate_csv)
df = df.pivot_table(index='EST_NO', columns='substrate_type', values='proportion_substrateType',
                     fill_value=0).reset_index().rename_axis(None, axis=1)
df = df.merge(est, 'right', 'EST_NO')
df = df.fillna(0)
df = df[['EST_NO', 'Rock', 'Mixed', 'Sand', 'Mud']]
df = df.round({'Rock':2, 'Mixed':2, 'Sand':2, 'Mud':2})
df = df.rename(columns={'Rock':'substrate_rock', 'Mixed':'substrate_mixed', 'Sand':'substrate_sand', 'Mud':'substrate_mud'})
em = em.merge(df, 'left', 'EST_NO')

df = pd.read_csv(rugo)
df = df.merge(est, 'right', 'EST_NO')
df = df.fillna(0)
df = df.rename(columns={'MEAN':'rugosity'})
df = df[['EST_NO', 'rugosity']]
df = df.round({'rugosity':5})
em = em.merge(df, 'left', 'EST_NO')

# df = pd.read_csv(hab)
# df = df.rename(columns={'habitat_richness':'substrate_feature_richness'})
# df = df[['EST_NO', 'substrate_feature_richness']]
# em = em.merge(df, 'left', 'EST_NO')

em = em.drop(columns=['EST_NO'])

em.to_csv(os.path.join(root, 'out_tables/ecodata.csv'), index=False)

# out to featureclass for mapping
arcpy.env.workspace = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\spatial\02_working\threats_03associate.gdb'
arcpy.XYTableToPoint_management(os.path.join(root, 'out_tables/ecodata.csv'), 'ecodata', 'estuary_centroid_x', 'estuary_centroid_y', coordinate_system=arcpy.SpatialReference(3005))



##############################################
# Data dictionary

# Manually copy columns from activities and ecodata tables
# add columns for units and description



##################################################################################

# Originally I was standardizing some columns, but we are now keeping data in their raw format.

# OLD NOTES on standardizing:
# Standardizing columns:
# It makes sense to scale vaulues in some way because units are different for many metrics and some
# aren't intuitive (e.g. bioband length / estuary area).
# A 0-1 scale would be easy to interpret, however because some columns have 0 values and other do
# not, a zero scaled value for a column without zero values (this would be the min) would appear as
# not having the feature present even though there would be a small amount present.

# Actually, we are going to do a mix. I'll standardize some columns but not others.
# It doesn't make sense to standardize something like species richness. I think the reader will
# actually want to know the values.

# standardize to 0-1
# e.g., df['salmon_biomass'] = (df.Sum_Weight_KG - df.Sum_Weight_KG.min()) / (df.Sum_Weight_KG.max() - df.Sum_Weight_KG.min())
# Z score
#e.g., df['salmon biomass'] = (df.Sum_Weight_KG - df.Sum_Weight_KG.mean()) / df.Sum_Weight_KG.std()

# threats = os.path.join(root, 'estuary_threats_log.csv')
# threats = pd.read_csv(threats)

# threats = threats.drop(['POINT_X', 'POINT_Y'], axis=1)

# for col in threats.columns:
#     if col.startswith('thrt'):
#         threats[col] = (threats[col] - threats[col].min()) / (threats[col].max() - threats[col].min())

# for col in threats.columns:
#     if col.startswith('thrt'):
#         new_col = ' '.join(col.split('_')[2:])
#         threats = threats.rename(columns={col:new_col})

# threats = threats.round(4)

# threats.to_csv(os.path.join(root, 'estuary_threats_zscore.csv'), index=False)




##############################################
# Summary stats
# These are being done post CSAS meeting and are very general stats for the SAR and RD
# output these as individual pandas tables for now and just write the results here

csv = os.path.join(root, 'ecodata_{}.csv')
est = pd.read_csv(os.path.join(root, 'estuary_threats_log.csv'))
est = est[['EST_NO', 'EST_NAME']]
total_est = len(est)

# Number of salmon species per estuary
# Say something like x% of estuaries have at least 1 salmon species, y% have 6
# This is already calculated in the salmon csv
salar = csv.format('salmon_abundrich')
df = pd.read_csv(salar)
df = df.merge(est, 'right', 'EST_NO')
df = df[['EST_NO', 'Sp_richness']]
df = df.fillna(0)
df = df.groupby(['Sp_richness']).size().reset_index(name='count')
total = df['count'].sum()
# percent with at least 1 salmon species
salm_1plus = df['count'][df.Sp_richness > 0.0].sum() / total * 100   # 76%
# percent with 6 salmon species
salm_6 = df['count'][df.Sp_richness==6.0] / total *  100     # 20%


# Number of estuaries with CU populations
salcu = csv.format('salmon_CUs')
df = pd.read_csv(salcu)
df = df.groupby('EST_NO')['FREQUENCY'].sum().reset_index()
cus = len(df)/total_est * 100    # 21%


# % of estuaries with herring spawn biomass
# just get count of rows in csv
her = csv.format('herringspawnindex')
df = pd.read_csv(her)
herring = len(df)/total_est * 100   # 21%


# Habitat
em = est.copy()

# % with eelgrass presence
eg_bb = csv.format('eelgrassBiobands')
df = pd.read_csv(eg_bb)
df.loc[df.length_div_estArea > 0, 'eg_bb'] = 1
df_1 = df[['EST_NO', 'eg_bb']]

eg_pl = csv.format('eelgrassPolys')
df = pd.read_csv(eg_pl)
df.loc[df.proportion_eelgrass > 0, 'eg_pl'] = 1
df_2 = df[['EST_NO', 'eg_pl']]

df3 = df_1.merge(df_2, 'outer', 'EST_NO')
df3 = df3.fillna(0)
df3['eelgrass'] = np.where((df3.eg_bb == 1) | (df3.eg_pl == 1), 1, 0)
df3 = df3[['EST_NO', 'eelgrass']]
eel_count = df3.eelgrass.count() / total_est * 100    # 43%
em = em.merge(df3, 'left', 'EST_NO')


# % with kelp understory presence (green or brown)
kb_bb = csv.format('kelpBrownBiobands')
df = pd.read_csv(kb_bb)
df.loc[df.length_div_estArea > 0, 'kb_bb'] = 1
df_1 = df[['EST_NO', 'kb_bb']]

ku_bb = csv.format('kelpGreenUlvaBiobands')
df = pd.read_csv(ku_bb)
df.loc[df.length_div_estArea > 0, 'ku_bb'] = 1
df_2 = df[['EST_NO', 'ku_bb']]

df3 = df_1.merge(df_2, 'outer', 'EST_NO')
df3 = df3.fillna(0)
df3['kelp_understory'] = np.where((df3.kb_bb == 1) | (df3.ku_bb == 1), 1, 0)
df3 = df3[['EST_NO', 'kelp_understory']]
kelp_count = df3.kelp_understory.count() / total_est * 100    # 82%
em = em.merge(df3, 'left', 'EST_NO')

# % with saltmarsh presence
sm_bb = csv.format('saltmarshBiobands')
df = pd.read_csv(sm_bb)
df.loc[df.length_div_estArea > 0, 'saltmarsh'] = 1
df = df[['EST_NO', 'saltmarsh']]
sm_count = df.saltmarsh.count() / total_est * 100    # 81%
em = em.merge(df, 'left', 'EST_NO')


# % of estuaries that include 3 habitat types
em = em.fillna(0)
em['total'] = em.eelgrass + em.kelp_understory + em.saltmarsh
threetypes = em.total[em.total == 3.0].count() / total_est * 100   # 36%

# % of estuaries that include 3 habitat types by CLUSTER
root = r'C:\Users\cristianij\Documents\Projects\Estuary_threats'
c_csv = os.path.join(root, 'scripts\estuaries_plotting_R\clusters_estuaries.csv')
df_clusters = pd.read_csv(c_csv)
em_clust = em.merge(df_clusters, on='EST_NO')

# if total is greater than 2, make it 1, otherwise 0
em_clust.loc[em_clust.total > 1, 'total_greater_1'] = 1
em_clust = em_clust.fillna(0)
em_count = em_clust.groupby(['cluster'])['total_greater_1'].sum().reset_index()

cluster_counts = em_clust.cluster.value_counts().to_frame()
cluster_counts['cluster_NO'] = cluster_counts.index

cluster_counts = cluster_counts.merge(em_count, left_on='cluster_NO', right_on = 'cluster')
cluster_counts['percent'] = cluster_counts.total_greater_1 / cluster_counts.cluster_x *100
# 1: 97%
# 2: 77%
# 3: 75%
# 4: 57%
# 5: 75%

# Dungeness Crab
crab = csv.format('crabProb')
df = pd.read_csv(crab)

# Get count of estuaries that have values
est_count = len(df)     # 372 out of 439

# Get % of estuaries that are over 0.5
est_count_50 = len(df[df.MEAN >= 0.5])

est_percent = est_count_50 / est_count *100    # 84%



##################################################################
# Table of estuaries PER activity to pull additional summary stats from

csv = os.path.join(root, 'estuary_threats_log.csv')
df = pd.read_csv(csv)

df = df.drop(columns={'EST_NO', 'estuary_area', 'POINT_X', 'POINT_Y', 'thrt_cli_sealevelrise',
       'thrt_cli_tempchange', 'thrt_cli_precipchange','thrt_cli_streamtempchange', 'EST_NAME'})

columns = df.columns

for column in columns:
    df[column] = np.where(df[column]>0, 1, 0)

df.loc['Estuary count'] = df.sum(numeric_only=True, axis=0)
df.loc['Percent of estuaries'] = df.loc['Estuary count'] / 439 * 100

df = df.tail(2)
df = df.T
df = df.sort_values(by='Percent of estuaries')
df['Activity'] = df.index

df['Activity'] = df.Activity.str.replace('thrt_', '')

# zone column
df['Zone'] = np.where(df.Activity.str[:3]=='mar', 'Marine', 'Terrestrial')

df['Activity'] = df.Activity.str[4:]
df['Estuary count'] = df['Estuary count'].astype('int')
df['Percent of estuaries'] = df['Percent of estuaries'].round(2)
df = df[['Zone', 'Activity', 'Estuary count', 'Percent of estuaries']]

df.to_csv(os.path.join(root, 'out_tables/estuaries_per_threat.csv'), index=False)


