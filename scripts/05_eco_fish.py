#
# Salmon biomass, species richness, and conservation unit count overlapping estuaries
#

import arcpy
from arcgis.features import GeoAccessor, GeoSeriesAccessor 
import os
import pandas as pd
import numpy as np
import math

arcpy.CheckOutExtension("Spatial")
from arcpy.sa import *


#########################################
# Data

root = r'C:/Users/cristianij/Documents/Projects/Estuary_threats/spatial'

land = os.path.join(root, r'02_working/estuaries_watersheds.gdb/land_erase')
estuaries = os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_01_ALL')
wat = os.path.join(root, '02_working/estuaries_watersheds.gdb/watersheds_05_clip')


eco_data_dir = os.path.join(root, '01_original/ecological_data_analysis')

# existing abundance and richness salmon data (I think Patrick compiled this already? It actually was mostly done by Bea)
# https://gitlab.com/dfo-msea/salmon-estuaries-nuseds/-/blob/main/code/NuSEDS_output_20220502.Rmd
salmon_bio = os.path.join(eco_data_dir, 'Estuary_CSAS_ecological_data.gdb/salmon_estuaries_june2022_noBuffer_2022data')

# nuSEDS - New Salmon Escapement Database System
# https://open.canada.ca/data/en/dataset/c48669a3-045b-400d-b730-48aafe8c5ee6
cu_csv = os.path.join(root, eco_data_dir, 'nuSEDS/conservation_unit_system_sites.csv')

# eulachon data
eulachon = os.path.join(eco_data_dir, 'Estuary_CSAS_ecological_data.gdb/mpatt_eco_fish_eulachon_spawning_data_WGS84')

# herring spawn index
herring_csv = os.path.join(root, eco_data_dir, 'herringSpawnIndex\Pacific_herring_spawn_index_data_EN.csv')

# sand lance data
psl_dir = os.path.join(root, eco_data_dir, 'PacificSandLance')

# working gdb
working_gdb = os.path.join(root, r'02_working/ecological_data_analysis.gdb')

arcpy.env.workspace = working_gdb


#########################################

# CU salmon population count in watershed

# "A Conservation Unit (CU) is a group of wild Pacific salmon sufficiently isolated from other 
# groups that, if extirpated, is very unlikely to recolonize naturally within an acceptable 
# timeframe, such as a human lifetime or a specified number of salmon generations."

# But according to Patrick, its not about status, its more so about that it is isolated.

# In general, nuSEDs data:
# The term “escapement” is used to refer to the group of mature salmon that have ‘escaped’ from 
# various sources of exploitation, and returned to freshwater to spawn and reproduce. This data is 
# assigned to a “Counting Site”, which may be a complete watercourse with a marine terminus, a 
# tributary to a larger watercourse, or a defined reach within a watercourse that may or may not 
# encompass the entire population but represents an index of the abundance of that population.

# From the nuSEDs database, all I need are the CU data
# The record of populations is at conservation_unit_system_sites.csv
# The other CU sheet Conservation_Unit_Data_20220902, seems to be a record of survey data for each 
# CU, so it is M:1.
# So I think all I need is that first csv

# Regarding CU polygons:
# https://open.canada.ca/data/en/dataset/1ac00a39-4770-443d-8a6b-9656c06df6a3
# CU polygons are very broad and multipart. It doesn't make sense to me how these are delineated, so
# we will just have to rely on the point XYs and overlap with our estuaries.


cu = pd.read_csv(cu_csv)
# select current and VREQ-current data
cu = cu[cu.CU_TYPE.isin(['Current', 'VREQ[Current]'])]

# output to arcgis fc
x = np.array(np.rec.fromrecords(cu.values)) 
names = cu.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
sr = arcpy.SpatialReference(4326)
arcpy.da.NumPyArrayToFeatureClass(x, os.path.join(arcpy.env.workspace, f'CU_01_pts'), ('CU_LONGT', 'CU_LAT'), sr)

# spatial join with watersheds
arcpy.env.qualifiedFieldNames = False
arcpy.SpatialJoin_analysis(
    'CU_01_pts',
    os.path.join(root, '02_working/estuaries_watersheds.gdb/watersheds_05_clip'),
    'CU_02_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option = 'INTERSECT'
)

# frequency of points by watershed and species
arcpy.Frequency_analysis('CU_02_sjoin', 'CU_03_freq', ['EST_NO', 'SPECIES_QUALIFIED'])

# output cu frequency table
field_names = [f.name for f in arcpy.ListFields('CU_03_freq') if f.name not in ['OBJECTID']] 
cursor = arcpy.da.SearchCursor('CU_03_freq', field_names) 
cupts = pd.DataFrame(data=[row for row in cursor], columns=field_names)
cupts.to_csv(os.path.join(root, 'ecodata_salmon_CUs.csv'), index=False)

# output with grouped by EST_NO (this was done way later)
cu_test = cupts.groupby('EST_NO').sum('FREQUENCY').reset_index()
cu_test.to_csv(os.path.join(root, 'ecodata_salmon_CUs_TEMP.csv'), index=False)

# join with estuary points for future mapping purposes
# arcpy.FeatureToPoint_management(os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_01_ALL'), 'temp_estpts', 'INSIDE')
# arcpy.env.qualifiedFieldNames = False
# arcpy.MakeFeatureLayer_management('temp_estpts', 'temp_lyr')
# arcpy.AddJoin_management('temp_lyr', 'EST_NO', 'CU_03_freq', 'EST_NO')
# arcpy.CopyFeatures_management('temp_lyr', 'CU_04_estjoin')
# arcpy.Delete_management(['temp_lyr', 'temp_estpts'])


#########################################

# CU status

# DFO CU status dataset:
# https://open.canada.ca/data/en/dataset/1ac00a39-4770-443d-8a6b-9656c06df6a3#wb-auto-6

# Salmon explorer data:
# https://data.salmonwatersheds.ca/data-library/ResultDetails.aspx?id=101
# https://data.salmonwatersheds.ca/data-library/ResultDetails.aspx?id=380

# PSF status data:
# annyoingly no unique ID linking records of populations to precise locations. There are CU names, 
# but these aren't consistent in how they are written out (i.e. some added descriptors at the end of certain names).
# Also, the corresponding spatial data are broad polygons, unlike the DFO data which are points 
# (perhaps spawning locations?). The polygons unfortunately do not align with watersheds (sometimes 
# spawning multiple watersheds in our analysis which should not be linked by flow). Therefore, we 
# can't say for sure which estuary a population should be linked to. If the status csvs included the
# sitename, which is more precise than the CU Name then maybe we could link them, but the table 
# does not include this.
# Also, we wouldn't know which are duplicates among datasets since the exact population isn't
# specified.
# Therefore, I will just stick with the DFO status data.

# Chum download did not have any status data

cu_status_dir = os.path.join(root, '01_original/ecological_data_analysis/nuSEDS/CU_status')
out_gdb = os.path.join(cu_status_dir, 'cu_status_results.gdb')
salmon_dir = '{}_Salmon_CU_Status'
salmon = ['Chinook', 'Coho', 'Even_Year_Pink', 'Odd_Year_Pink', 'Lake_Type_Sockeye', 'River_Type_Sockeye']

# merge all shapefiles
salmon_shps = []
for s in salmon:
    path = os.path.join(cu_status_dir, salmon_dir.format(s))
    arcpy.env.workspace = path
    shp = arcpy.ListFiles('*.shp')
    salmon_shps.append(os.path.join(cu_status_dir, salmon_dir.format(s), shp[0]))
arcpy.env.workspace = out_gdb
arcpy.Merge_management(salmon_shps, 'CU_status')

# Join with watersheds
arcpy.MakeFeatureLayer_management('CU_status', 'temp_cu')
arcpy.env.qualifiedFieldNames = False
arcpy.SpatialJoin_analysis('temp_cu', wat, 'CU_status_sjoin', 'JOIN_ONE_TO_MANY', 'KEEP_COMMON')
arcpy.Delete_management('CU_status')

# Read into pandas df
field_names = ['CU_NAME', 'FULL_CU_IN', 'SP_QUAL', 'CU_TYPE', 'WSP_STATUS', 'EST_NO']
cursor = arcpy.da.SearchCursor('CU_status_sjoin', field_names)
df = pd.DataFrame(data=[row for row in cursor], columns=field_names)

# Read in cluster data and join
clusters = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\scripts\estuaries_plotting_R\clusters_estuaries.csv'
df_clusters = pd.read_csv(clusters)
df_clusters = df_clusters[['name', 'EST_NO', 'cluster']]
df_c = df.merge(df_clusters, on='EST_NO')

# Standardize status names. They are not consistent.
conditions = [
    df_c.WSP_STATUS.eq('TBD'),
    df_c.WSP_STATUS.eq('RED/AMBER/GREEN'),
    df_c.WSP_STATUS.eq('RED/AMBER'),
    df_c.WSP_STATUS.eq('RED'),
    df_c.WSP_STATUS.eq('GREEN/RED'),
    df_c.WSP_STATUS.eq('GREEN/AMBER'),
    df_c.WSP_STATUS.eq('GREEN'),
    df_c.WSP_STATUS.eq('DD'),
    df_c.WSP_STATUS.eq('AMBER/RED/GREEN'),
    df_c.WSP_STATUS.eq('AMBER/GREEN'),
    df_c.WSP_STATUS.eq('AMBER'),
]
choices = ['tbd', 'Amber', 'Amber/Red', 'Red', 'Amber', 'Green/Amber', 'Green', 'dd', 'Amber', 'Green/Amber', 'Amber']

df_c['status'] = np.select(conditions, choices, default='tbd')

# Out to csv
df_c.to_csv(os.path.join(root, 'ecodata_salmon_CUs_status.csv'), index=False)

# OF THE 87 CUs ASSESSED, only 61 have ratings (otherwise data deficient of TBD). 49 of these are in
# the Fraser River watershed.
# So, there is not very even survey distribution.
# Also, some have mixed ratings of all 3 ratings (RED/AMBER/GREEN). Not sure how to interpret this.



#############################################

# For Salmon biomass and richness:
# Already in attributes, output csv of species abundance and overall richness

field_names = [f.name for f in arcpy.ListFields(salmon_bio) if f.name.startswith('Sum')]
field_names.append('EST_NO')
field_names.append('Sp_richness')
cursor = arcpy.da.SearchCursor(salmon_bio, field_names) 
sbio = pd.DataFrame(data=[row for row in cursor], columns=field_names)
sbio = sbio[sbio.EST_NO>0]
sbio.loc[sbio.Sp_richness == -999999.0, 'Sp_richness'] = np.nan
sbio['Sum_Weight_KG_LOG'] = np.log10(sbio.Sum_Weight_KG)
sbio.loc[sbio.Sum_Weight_KG_LOG.isnull(), 'Sum_Weight_KG_LOG'] = 0
sbio.loc[np.isinf(sbio.Sum_Weight_KG_LOG), 'Sum_Weight_KG_LOG'] = 0
sbio.to_csv(os.path.join(root, 'ecodata_salmon_abundrich.csv'), index=False)



##############################################
# Eulachon
# We just want to know presence absence

# There appears to be two different surveys in here. The 2016 one is tied to the estuaries, but the
# 2006 one is more general polygons. For the most part they overlap the estuaries, but I can see one
# case where it is within 25 meters of an estuary that is not included in the dataset.
# Therefore, do a spatial select of the estuaries.

sel = arcpy.SelectLayerByLocation_management(
    os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_01_ALL'),
    'WITHIN_A_DISTANCE',
    eulachon,
    1000
)
arcpy.CopyFeatures_management(sel, 'eulachon_01_sel')

field_names = ['EST_NO'] 
cursor = arcpy.da.SearchCursor('eulachon_01_sel', field_names) 
eul = pd.DataFrame(data=[row for row in cursor], columns=field_names)
eul['eulachon_presence'] = 1
eul.to_csv(os.path.join(root, 'ecodata_eulachon.csv'), index=False)


##############################################
# Herring spawn index

# data: https://open.canada.ca/data/en/dataset/d892511c-d851-4f85-a0ec-708bc05d2810
# transects: https://open.canada.ca/data/en/dataset/abdd0890-bfa4-4b55-b76d-a4c3a7e93e92
# shiny app: https://github.com/grinnellm/FIND

# Investigating data:

# In the r package, I could not load the vignette as they said. I had to download the Rmd and 
# hardcode some file paths to get it to work.
# Also, the shiny app gives me an error. It will run, but it is referencing a column incorrectly.

# From the report:
# "Spawn numbers identify distinct spawns, which we define as a continuous stretch of shoreline with
# no detectable break in egg deposition. For a given spawn survey type x, a
# spawn s is the finest scale at which we calculate the spawn index. A break in
# egg deposition is determined by the absence of Pacific Herring spawn on two
# consecutive transects, or by a temporal gap in spawning. Most spawns are
# also characterized by longitude and latitude, as well as start and end dates of
# spawning. Surveyors usually collect longitude and latitude at the start and
# end of each transect; for surface spawn surveys that are not along transects
# (section 3.1), surveyors collect longitude and latitude at the start and end of
# the spawn (i.e., overall length and width).
# Pacific Herring spawns typically extend along the shore; from above,
# spawns are identified by a milky or turquoise discolouration of the ocean
# caused by the release of milt, and often appear as bands running parallel to
# the shore (Figure 4). Thus, spawn ‘length’ refers to distance parallel to the
# shore, and ‘width’ refers to distance perpendicular to the shore."

# For creating polys from points:
# There's no evidence that they do this for the spawning events. In the shiny app they buffer by
# width, but this just seems to be for mapping purposes.
# The longest length is 24km but most are below 1km.

# From Matt Thompson:
# "The sum of surface, macro and understory is what we use for the total spawning biomass, so you 
# are correct in lumping those together.  
# Our locations are made up of multiple transects and the biomass can only go down to the location 
# level (not the transect level).  So you will have to decide if a location fits within your estuary
# area."  
# "The lat/long from the spawn index csv are a centroid for that location.  For the transects that 
# make up a location we also have the start, middle and end lat and longs."
# From Jaclyn:
# "One more response: there isn’t any working spawn polygon code. We have a project 6-7 years ago  
# where we had a contractor develop a series of functions for creating spawn polygons but the 
# resulting polygons needed a lot of manual adjustments due to spawn going around islands. We 
# haven’t pursued this any further as it’s not needed by the program."

# Our general approach:
# Use data back to when dive surveys started
# Buffer by longer of half width or length.
# ( I thought of different ways to buffer an oval parallel to shoreline, but there are so many 
# places where this won't work well. In the end, I'm not trying to represent the exact area, I'm 
# just trying to generate individual search distances for potentially overlapping estuaries.)
# Clip by land and remove orphan pieces
# Associate with estuaries

herr = pd.read_csv(herring_csv)

# Select years > 1987
# Survey methodology changed at this point
herr = herr[herr.Year > 1987]

# check if there are any records without biomass values
test1 = herr.isna()
test2 = pd.crosstab(test1.Surface, [test1.Macrocystis, test1.Understory])
# there are 403

# sum three fields
herr['biomass_total'] = herr[['Surface', 'Macrocystis', 'Understory']].sum(axis=1)
herr[herr.biomass_total == 0.0]

# bring all into arcgis to investigate where the ones are that do not have values
x = np.array(np.rec.fromrecords(herr.values)) 
names = herr.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
sr = arcpy.SpatialReference(4326) 
arcpy.da.NumPyArrayToFeatureClass(x, os.path.join(arcpy.env.workspace, f'temp_herr_pts'), ('Longitude', 'Latitude'), sr) 

# make new column and calculate as the half of the larger of the length/width, for ones without
# values, give them a buffer of 10
arcpy.AddField_management('temp_herr_pts', 'buffer_dist', 'FLOAT')
with arcpy.da.UpdateCursor('temp_herr_pts', ['Length', 'Width', 'buffer_dist']) as cursor:
    for row in cursor:
        if row[0] is None and row[1] is None:
            row[2] = 10
        elif row[0] is None:
            row[2] = row[1] / 2.0
        elif row[1] is None:
            row[2] = row[0] / 2.0
        elif row[0] > row[1]:
            row[2] = row[0] / 2.0
        else:
            row[2] = row[1] / 2.0
        cursor.updateRow(row)

arcpy.Project_management('temp_herr_pts', 'temp_herr_project', 3005)

# Some points are on land. Adjust.
# Some are in the middle of an island. Some are way far inland and its hard to know where they 
# should go, so I think I need a threshold.
# (need to do select without making a feature layer, so that it edits just the ones selected, but
# maintains the rest of the points too)
#
# First, create a coastline that is buffered 10m from normal coastline so that the point is 
# definitively in water.
landsnap = os.path.join(root, '02_working/estuaries_watersheds.gdb/land_buffsnap')
if not arcpy.Exists(landsnap):
    arcpy.PairwiseBuffer_analysis(land, landsnap, 10, 'ALL')
sel = arcpy.SelectLayerByLocation_management('temp_herr_project', 'INTERSECT', land)
# There are 3000+ points so this take a while! (12 hours)
arcpy.Snap_edit(sel, [[landsnap, 'EDGE', '2000 Meters']])
# MAKE A COPY OF THIS _DONOTDELETE
gdb = os.path.join(eco_data_dir, 'herringSpawnIndex/hsi_DONOTDELETE.gdb')
if not arcpy.Exists(gdb):
    arcpy.CreateFileGDB_management(os.path.join(eco_data_dir, 'herringSpawnIndex'), 'hsi_DONOTDELETE.gdb')
arcpy.CopyFeatures_management('temp_herr_project', os.path.join(gdb, 'hsi_pointsSnapped'))

# Remove the remaining points that do not overlap
sel = arcpy.SelectLayerByLocation_management('temp_herr_project', 'INTERSECT', land, invert_spatial_relationship='INVERT')
arcpy.CopyFeatures_management(sel, 'temp_herr')

# IF the snap doesn't work...
# Do a prebuffer before I assign the uID. Spatially select any that are completely contained by land
# and get their IDs. Remove these from the points.
# I will inevitabley miss some, but that is how it goes.
# arcpy.PairwiseBuffer_analysis('temp_herr_project', 'temp_herr_buffer', 'buffer_dist')
# sel = arcpy.SelectLayerByLocation_management('temp_herr_buffer', 'COMPLETELY_WITHIN', land, invert_spatial_relationship='INVERT')
# oids = [row[0] for row in arcpy.da.SearchCursor(sel, ['OBJECTID'])]
# arcpy.MakeFeatureLayer_management('temp_herr_project', 'temp_lyr', where_clause='"OBJECTID" IN {}'.format(tuple(oids)))
# arcpy.CopyFeatures_management('temp_lyr', 'temp_herr')


##### buffer and remove orphan pieces #####

inname = 'temp_herr'
print('Removing orphan buffers')

# add a uID field
arcpy.AddField_management(inname, 'uID', 'LONG')
# get name of OID field
oid_field = arcpy.ListFields(inname, field_type='OID')[0].name
with arcpy.da.UpdateCursor(inname, [oid_field, 'uID']) as cursor:
    for row in cursor:
        row[1]=row[0]
        cursor.updateRow(row) 

arcpy.PairwiseBuffer_analysis(inname, 'memory/out', 'buffer_dist', dissolve_option='NONE')
arcpy.PairwiseErase_analysis('memory/out', land, 'memory/erase')
arcpy.MultipartToSinglepart_management('memory/erase', 'memory/multising')
arcpy.AddField_management('memory/multising', 'partID', 'SHORT')
oid_field = arcpy.ListFields('memory/multising', field_type='OID')[0].name
with arcpy.da.UpdateCursor('memory/multising', [oid_field, 'partID']) as cursor:
    for row in cursor:
        row[1]=row[0]
        cursor.updateRow(row)

arcpy.SetLogHistory(False) # trying to speed this up, not sure if it matters
features = []
uIDs = [row[0] for row in arcpy.da.SearchCursor(inname, ['uID'])]
arcpy.MakeFeatureLayer_management(inname, 'tmp_sg') # way faster creating layers
arcpy.MakeFeatureLayer_management('memory/multising', 'tmp_buff')
for i in uIDs:
    # first check if there is just one part per uID. I can skip the other selections if so.
    arcpy.SelectLayerByAttribute_management(
        'tmp_buff',
        'NEW_SELECTION',
        "uID = {}".format(i)
    ) 
    sel_count = int(arcpy.GetCount_management('tmp_buff')[0])
    if sel_count==1:
        with arcpy.da.SearchCursor('tmp_buff', ['partID']) as cursor:
            for row in cursor:
                features.append(row[0])
        arcpy.SelectLayerByAttribute_management('tmp_buff', 'CLEAR_SELECTION')
        continue
    arcpy.SelectLayerByAttribute_management('tmp_buff', 'CLEAR_SELECTION')
    # if not, then continue you with identifying overlapping features
    print('spatial select for feature {}, {} of {}'.format(str(i), str(uIDs.index(i)), str(len(uIDs))))
    arcpy.SelectLayerByAttribute_management( # get the origin piece
        'tmp_sg',
        'NEW_SELECTION',
        "uID = {}".format(i)
    )
    arcpy.SelectLayerByLocation_management( # get just the part that overlaps with the original feature
        'tmp_buff',
        'INTERSECT',
        'tmp_sg',
    )  
    arcpy.SelectLayerByAttribute_management( # get just the pieces that are associated with the origin feature
        'tmp_buff',
        'SUBSET_SELECTION',
        "uID = {}".format(i)
    ) 
    sel_count = int(arcpy.GetCount_management('tmp_buff')[0])
    if sel_count!=1: # check if somnething weird happened
        print(sel_count)
        print('more/less than 1 feature selected')
        break
    with arcpy.da.SearchCursor('tmp_buff', ['partID']) as cursor: # add those feature ids to list to keep
        for row in cursor:
            features.append(row[0])
    arcpy.SelectLayerByAttribute_management('tmp_buff', 'CLEAR_SELECTION')
    arcpy.SelectLayerByAttribute_management('tmp_sg', 'CLEAR_SELECTION')

feat_tup = tuple(features)
sel_feat = arcpy.SelectLayerByAttribute_management(
    'memory/multising',
    'NEW_SELECTION',
    "partID IN {}".format(feat_tup)
)
arcpy.CopyFeatures_management(sel_feat, inname+'_buff')
arcpy.Delete_management('tmp_buff')
arcpy.Delete_management('tmp_sg')
arcpy.Delete_management('memory/out')
arcpy.Delete_management('memory/erase')
arcpy.Delete_management('memory/multising')

######    ######


# Do a spatial join with the estuaries and keep common.
# Should I do a proportional biomass by overlap area thing?
#   No, I don't know the actual spatial arrangment, so I'm not really sure this makes sense.
#   I can justify this by saying that perhaps the estuarine environment support the overall 
#   spawning event, and I'm not saying it is exclusively about spatial overlap.
arcpy.MakeFeatureLayer_management(inname+'_buff', 'temp_est')
arcpy.env.qualifiedFieldNames = False
arcpy.SpatialJoin_analysis('temp_est', estuaries, inname+'_sjoin', 'JOIN_ONE_TO_MANY', 'KEEP_COMMON')

# Check how many remain that have no biomass values.
# In the end I will just keep these and code them as 0 anyways. They will at least contribute to the
# survey count metric.
# There are only 28

# Then, go back and do most of the heavy lifting in pandas
field_names = [i.name for i in arcpy.ListFields(inname+'_sjoin') if i.name in ['EST_NO', 'Year', 'LocationCode', 'biomass_total']] 
cursor = arcpy.da.SearchCursor(inname+'_sjoin', field_names) 
df = pd.DataFrame(data=[row for row in cursor], columns=field_names)
# For coincident locations: sum biomass within year, sum count within year (not spawn number)
df['survey_count'] = 1
df2 = df.groupby(['EST_NO', 'LocationCode', 'Year']).sum().reset_index()
# then sum AND average across years
df3 = df2.groupby('EST_NO').agg({'survey_count': 'sum', 'biomass_total':'mean'}).reset_index()
df3 = df3.rename(columns={'survey_count':'survey_count_SUM', 'biomass_total':'biomass_MEAN'})
df4 = df2.groupby('EST_NO').agg({'biomass_total':'sum'}).reset_index()
df4 = df4.rename(columns={'biomass_total':'biomass_SUM'})
df5 = df3.merge(df4, 'outer', 'EST_NO')

# Out to csv
df5.to_csv(os.path.join(root, 'ecodata_herringspawnindex.csv'), index=False)

# Join back to estuary points
x = np.array(np.rec.fromrecords(df5.values)) 
names = df5.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToTable(x, os.path.join(arcpy.env.workspace, f'temp_tbl'))
arcpy.FeatureToPoint_management(estuaries, 'temp_est_pts', 'INSIDE')
arcpy.MakeFeatureLayer_management('temp_est_pts', 'temp_est_pts_lyr')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_est_pts_lyr', 'EST_NO', 'temp_tbl', 'EST_NO')
arcpy.CopyFeatures_management('temp_est_pts_lyr', 'herring_est_pts')
arcpy.CopyFeatures_management('temp_herr_sjoin', 'herring_buffer_sjoin')

# Delete all intermediate data except one snapped to land and final sjoin
arcpy.Delete_management(['temp_herr_buff', 'temp_herr', 'temp_herr_pts', 'temp_herr_sjoin', 'temp_est_pts', 'temp_tbl'])



##############################################
# Pacific Sand Lance

# We were given two sets of data from Cliff Robinson:
# (1) Probability raster from 2021 paper from him and Bea. They predicted out to 150m depth(?)
# (2) Probability raster from 2022 paper from Jacqueline Huard. This was for just intertidal beach
# habitat.

# In our meeting with Cliff he mentioned something along the lines of Jacqueline not including estuary
# extents as part of her study area. It looks like she does in fact include this, but she says:
# "Within the estuary itself, silt content is high, which is not suitable for sand lance because 
# fine particles are thought to clog their gills (Wright et al. 2000)."
# So I guess this comes out in the model by including substrate sediment type, but any areas with sand
# and the areas just outside appear to be the most suitable.
# "In our intertidal model, the most important environmental variable across all model iterations 
# predicting the presence of suitable intertidal habitat for sand lance was distance to estuaries. 
# This layer was also a key variable in the subtidal model (Robinson et al. 2021). Estuaries are 
# major sources of terrestrial sand and the freshwater input contributes to water and sediment 
# circulation in the Salish Sea (Peterson et al. 1984; Mason et al. 2018; Earle 2019)."

# I won't combine the two models in any way. For now, find the mean, and upper and lower quantiles
# to get a sense of the range.
# We ideally want to include area adjacent to the estuary too to get a sense of importance. The
# buffer could vary based on a combination of stream order and depth, but I'm not sure how worth it
# it is to spend much time on this when the extent is only for the SoG.

# I will do a buffered and non-buffered version.

# For the buffered version I thought of a bunch of different way based on estuary area, depth, width,
# stream order, and they all have their difficulties and limitations.
# Ultimately, the range of influence of an estuary is a combination of stream order, bathymetry,
# and ocean currents. It would be complex to figure this out specifically for each estuary.
# I'll do just the non-buffered version for now, but if I decide to come back to do something more
# complex, here is what I propose:

# Find distance from estuary to nearest depth point that is > 150m deep (using bathymetry on 
# GISHub). Use this distance to buffer the estuary. Clip by the 150m contour and land and remove 
# orphan pieces. This can be my range of influence. I would average cells, and for any that are 
# NoData within this range I would want them to be 0 so they are part of the average. (Bounday bay 
# is unique exception, would need to clip to international border).


### Non-buffered version ###

# Can I go straight to a Zonal Statistics Table thing?
# Control nodata so that they don't get included.

# Cliff Robinson data
psl_cr = os.path.join(psl_dir, 'PSL_Burying_SOG/PSL_Burying_Maxent.tif')
psl_cr_rast = Raster(psl_cr)
# Jacqueline Huard data
psl_jh = os.path.join(psl_dir, 'PSl intertidal spawn habitat.tif')
arcpy.CopyRaster_management(psl_jh, 'temp_rast_psl_jh', nodata_value=-999)

# convert estuaries to raster with same cell size as each psl raster. This avoids missing cell
# centers with the feature polygon.
arcpy.CopyFeatures_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'priority', 1)

arcpy.env.SnapRaster = psl_cr_rast
arcpy.PolygonToRaster_conversion('temp_est', 'EST_NO', 'temp_est_rast_cr', 'MAXIMUM_AREA', 'priority', psl_cr_rast)
arcpy.env.SnapRaster = 'temp_rast_psl_jh'
arcpy.PolygonToRaster_conversion('temp_est', 'EST_NO', 'temp_est_rast_jh', 'MAXIMUM_AREA', 'priority', 'temp_rast_psl_jh')

outZSaT = ZonalStatisticsAsTable(
    'temp_est_rast_cr', 
    'Value', 
    psl_cr_rast, 
    'sandlance_estuaries_CRobinson', 
    'DATA', 
    'MIN_MAX_MEAN')
# change Value to EST_NO
arcpy.AlterField_management(
    'sandlance_estuaries_CRobinson',
    'Value',
    'EST_NO',
    'EST_NO')

outZSaT = ZonalStatisticsAsTable(
    'temp_est_rast_jh', 
    'Value', 
    'temp_rast_psl_jh', 
    'sandlance_estuaries_JHuard', 
    'DATA', 
    'MIN_MAX_MEAN')
# change Value to EST_NO
arcpy.AlterField_management(
    'sandlance_estuaries_JHuard',
    'Value',
    'EST_NO',
    'EST_NO')

arcpy.Delete_management(['temp_est', 'temp_est_rast_cr', 'temp_est_rast_jh', 'temp_rast_psl_jh'])

# output to csv
field_names = [f.name for f in arcpy.ListFields('sandlance_estuaries_CRobinson') if f.name not in ['OBJECTID', 'AREA']]
cursor = arcpy.da.SearchCursor('sandlance_estuaries_CRobinson', field_names) 
df1 = pd.DataFrame(data=[row for row in cursor], columns=field_names)
cols = df1.columns
for col in cols:
    if col != 'EST_NO':
        df1 = df1.rename(columns={col:col+'_CR'})

field_names = [f.name for f in arcpy.ListFields('sandlance_estuaries_JHuard') if f.name not in ['OBJECTID', 'AREA']]
cursor = arcpy.da.SearchCursor('sandlance_estuaries_JHuard', field_names) 
df2 = pd.DataFrame(data=[row for row in cursor], columns=field_names)
cols = df2.columns
for col in cols:
    if col != 'EST_NO':
        df2 = df2.rename(columns={col:col+'_JH'})

df3 = df1.merge(df2, 'outer', 'EST_NO')

df3.to_csv(os.path.join(root, 'ecodata_sandlanceHabitatProb.csv'), index=False)



###################################################################################################
# Invert data
# Other than crab, invert data was pulled from shellfish database in an R script
###################################################################################################


##############################################
# Dungeness crab

# data is from Jess Nephin's SDM

crab = os.path.join(root, eco_data_dir, 'Dungeness_crab/Mean_DungenessDetection.tif')
crab_rast = Raster(crab)
# or...
#arcpy.CopyRaster_management(crab, 'temp_rast_crab', nodata_value=-999)

# convert estuaries to raster with same cell size as each psl raster. This avoids missing cell
# centers with the feature polygon.
arcpy.CopyFeatures_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'priority', 1)

arcpy.env.SnapRaster = crab_rast
arcpy.PolygonToRaster_conversion('temp_est', 'EST_NO', 'temp_est_rast', 'MAXIMUM_AREA', 'priority', crab_rast)

outZSaT = ZonalStatisticsAsTable(
    'temp_est_rast', 
    'Value', 
    crab_rast, 
    'crab_estuaries', 
    'DATA', 
    'MIN_MAX_MEAN')
# change Value to EST_NO
arcpy.AlterField_management(
    'crab_estuaries',
    'Value',
    'EST_NO',
    'EST_NO')

arcpy.Delete_management(['temp_est', 'temp_est_rast'])

# output to csv
field_names = [f.name for f in arcpy.ListFields('crab_estuaries') if f.name not in ['OBJECTID', 'AREA']]
cursor = arcpy.da.SearchCursor('crab_estuaries', field_names) 
df = pd.DataFrame(data=[row for row in cursor], columns=field_names)
df.to_csv(os.path.join(root, 'ecodata_crabProb.csv'), index=False)



##############################################
# Prawn - Pandalus platyceros

root_i = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\scripts\inverts_database_R\Data'
csv1 = os.path.join(root_i, 'multispecies_smallmesh_trawl.csv')
csv2 = os.path.join(root_i, 'prawnshrimp_commercial.csv')
csv3 = os.path.join(root_i, 'prawntrap_processed.csv')

df1 = pd.read_csv(csv1)
df2 = pd.read_csv(csv2)
df3 = pd.read_csv(csv3)

# Some dfs contain multiple species. Filter for the correct ones
df1 = df1[df1['Pandalus platyceros']==1]
df2 = df2[df2.prawn==1]
df3 = df3[df3['Pandalus platyceros']==1]

# concatenate datasets with similar geometry. can get rid of most fields.
# also, some of the line datasets only have 1 set of coordinates. Separate these out.
df_13 = pd.concat([df1, df3])
df_lines = df_13[~df_13.End_latitude.isna()]
df_points = df_13[df_13.End_latitude.isna()]
df_points = df_points.rename(columns={'Start_latitude':'latitude', 'Start_longitude':'longitude'})
df_points = pd.concat([df_points, df2])
df_points = df_points[['latitude', 'longitude']]

# generate temporary gdb
arcpy.CreateFileGDB_management(root_i, 'TEMP.gdb')
tempgdb = os.path.join(root_i, 'TEMP.gdb')

# define spatial reference
sr = arcpy.SpatialReference(4326)

# for point datasets, convert to fc
x = np.array(np.rec.fromrecords(df_points.values)) 
names = df_points.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToFeatureClass(x, os.path.join(tempgdb, f'df_pts'), ('longitude', 'latitude'), sr) 

# for line survey data, create line fc from points
df_lines.to_csv(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.env.workspace = tempgdb
arcpy.XYToLine_management(os.path.join(root_i, 'TEMP_lines.csv'), 'df_lines', 'Start_longitude', 'Start_latitude', 'End_longitude', 'End_latitude', spatial_reference = sr, attributes='NO_ATTRIBUTES')
# Remove lines that are absurdly long and cross land and must be an error
sel = arcpy.SelectLayerByAttribute_management('df_lines', 'NEW_SELECTION', 'Shape_Length < 0.13')
arcpy.CopyFeatures_management(sel, 'df_lines_sel')
del sel

# spatial joins
arcpy.MakeFeatureLayer_management('df_lines_sel', 'temp_lyr')
arcpy.SpatialJoin_analysis(
    'temp_lyr',
    estuaries,
    'df_lines_sel_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

arcpy.MakeFeatureLayer_management('df_pts', 'temp_lyr_pts')
arcpy.SpatialJoin_analysis(
    'temp_lyr_pts',
    estuaries,
    'df_point_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

# out to pandas, groupby estuary, combine line and point, to csv
df_lines = pd.DataFrame.spatial.from_featureclass('df_lines_sel_sjoin')
df_pts = pd.DataFrame.spatial.from_featureclass('df_point_sjoin')

df_lines = df_lines[['EST_NO', 'EST_NAME']]
df_pts = df_pts[['EST_NO', 'EST_NAME']]
df = pd.concat([df_lines, df_pts])

df = df.groupby('EST_NO').agg(
    est_name=('EST_NAME', 'first'),
    records_count=('EST_NO', 'count')
).reset_index()

df.to_csv(os.path.join(root, 'ecodata_prawn.csv'))

# clean up temp files
arcpy.Delete_management(['temp_lyr', 'temp_lyr_pts'])
arcpy.Delete_management(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.Delete_management(['df_lines', 'df_lines_sel', 'df_lines_sel_sjoin', 'df_pts', 'df_point_sjoin'])
arcpy.Delete_management(tempgdb)


##############################################
# Prawn - ALL SURVEY LOCATIONS
# For the research document we ended up wanting to know all the locations that surveys occurred so
# that we can say which estuaries had no overlap with surveys

root_i = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\scripts\inverts_database_R\Data\survey_coverage_prawn'
csv1 = os.path.join(root_i, 'multispecies_smallmesh_trawl_ALL_LATLON.csv')
csv2 = os.path.join(root_i, 'prawnshrimp_commercial_ALL_LATLON.csv')
csv3 = os.path.join(root_i, 'prawntrap_processed_ALL_LATLON.csv')
df1 = pd.read_csv(csv1)
df2 = pd.read_csv(csv2)
df3 = pd.read_csv(csv3)

# concatenate datasets with similar geometry. can get rid of most fields.
# also, some of the line datasets only have 1 set of coordinates. Separate these out.
df_13 = pd.concat([df1, df3])
df_lines = df_13[~df_13.End_latitude.isna()]
df_lines = df_lines[['Start_latitude', 'Start_longitude', 'End_latitude', 'End_longitude']]
df_points = df_13[df_13.End_latitude.isna()]
df_points = df_points.rename(columns={'Start_latitude':'latitude', 'Start_longitude':'longitude'})
df_points = pd.concat([df_points, df2])
df_points = df_points[['latitude', 'longitude']]

# generate temporary gdb
arcpy.CreateFileGDB_management(root_i, 'TEMP.gdb')
tempgdb = os.path.join(root_i, 'TEMP.gdb')

# define spatial reference
sr = arcpy.SpatialReference(4326)

# for point datasets, convert to fc
x = np.array(np.rec.fromrecords(df_points.values)) 
names = df_points.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToFeatureClass(x, os.path.join(tempgdb, f'df_pts'), ('longitude', 'latitude'), sr) 

# for line survey data, create line fc from points
df_lines.to_csv(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.env.workspace = tempgdb
arcpy.XYToLine_management(os.path.join(root_i, 'TEMP_lines.csv'), 'df_lines', 'Start_longitude', 'Start_latitude', 'End_longitude', 'End_latitude', spatial_reference = sr, attributes='NO_ATTRIBUTES')
# Remove lines that are absurdly long and cross land and must be an error
sel = arcpy.SelectLayerByAttribute_management('df_lines', 'NEW_SELECTION', 'Shape_Length < 0.13')
arcpy.CopyFeatures_management(sel, 'df_lines_sel')
del sel

# spatial joins
arcpy.MakeFeatureLayer_management('df_lines_sel', 'temp_lyr')
arcpy.SpatialJoin_analysis(
    'temp_lyr',
    estuaries,
    'df_lines_sel_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

arcpy.MakeFeatureLayer_management('df_pts', 'temp_lyr_pts')
arcpy.SpatialJoin_analysis(
    'temp_lyr_pts',
    estuaries,
    'df_point_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

# out to pandas, groupby estuary, combine line and point, to csv
df_lines = pd.DataFrame.spatial.from_featureclass('df_lines_sel_sjoin')
df_pts = pd.DataFrame.spatial.from_featureclass('df_point_sjoin')

df_lines = df_lines[['EST_NO', 'EST_NAME']]
df_pts = df_pts[['EST_NO', 'EST_NAME']]
df = pd.concat([df_lines, df_pts])

df = df.groupby('EST_NO').agg(
    est_name=('EST_NAME', 'first'),
    records_count=('EST_NO', 'count')
).reset_index()

df.to_csv(os.path.join(root, 'ecodata_prawn_ALL_SURVEYS.csv'))

# clean up temp files
arcpy.Delete_management(['temp_lyr', 'temp_lyr_pts'])
arcpy.Delete_management(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.Delete_management(['df_lines', 'df_lines_sel', 'df_lines_sel_sjoin', 'df_pts', 'df_point_sjoin'])
arcpy.Delete_management(tempgdb)





##############################################
# Coonstripe/dock shrimp

root_i = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\scripts\inverts_database_R\Data'
csv1 = os.path.join(root_i, 'multispecies_smallmesh_trawl.csv')
csv2 = os.path.join(root_i, 'prawnshrimp_commercial.csv')
csv3 = os.path.join(root_i, 'prawntrap_processed.csv')

df1 = pd.read_csv(csv1)
df2 = pd.read_csv(csv2)
df3 = pd.read_csv(csv3)

# Some dfs contain multiple species. Filter for the correct ones
df1 = df1[df1['Pandalus danae']==1]
df2 = df2[df2.shrimp_coonstripe==1]
df3 = df3[df3['Pandalus danae']==1]

# concatenate datasets with similar geometry. can get rid of most fields.
# also, some of the line datasets only have 1 set of coordinates. Separate these out.
df_13 = pd.concat([df1, df3])
df_lines = df_13[~df_13.End_latitude.isna()]
df_points = df_13[df_13.End_latitude.isna()]
df_points = df_points.rename(columns={'Start_latitude':'latitude', 'Start_longitude':'longitude'})
df_points = pd.concat([df_points, df2])
df_points = df_points[['latitude', 'longitude']]

# generate temporary gdb
arcpy.CreateFileGDB_management(root_i, 'TEMP.gdb')
tempgdb = os.path.join(root_i, 'TEMP.gdb')

# define spatial reference
sr = arcpy.SpatialReference(4326)

# for point datasets, convert to fc
x = np.array(np.rec.fromrecords(df_points.values)) 
names = df_points.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToFeatureClass(x, os.path.join(tempgdb, f'df_pts'), ('longitude', 'latitude'), sr) 

# for line survey data, create line fc from points
df_lines.to_csv(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.env.workspace = tempgdb
arcpy.XYToLine_management(os.path.join(root_i, 'TEMP_lines.csv'), 'df_lines', 'Start_longitude', 'Start_latitude', 'End_longitude', 'End_latitude', spatial_reference = sr, attributes='NO_ATTRIBUTES')
# Remove lines that are absurdly long and cross land and must be an error
sel = arcpy.SelectLayerByAttribute_management('df_lines', 'NEW_SELECTION', 'Shape_Length < 0.13')
arcpy.CopyFeatures_management(sel, 'df_lines_sel')
del sel

# spatial joins
arcpy.MakeFeatureLayer_management('df_lines_sel', 'temp_lyr')
arcpy.SpatialJoin_analysis(
    'temp_lyr',
    estuaries,
    'df_lines_sel_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

arcpy.MakeFeatureLayer_management('df_pts', 'temp_lyr_pts')
arcpy.SpatialJoin_analysis(
    'temp_lyr_pts',
    estuaries,
    'df_point_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

# out to pandas, groupby estuary, combine line and point, to csv
df_lines = pd.DataFrame.spatial.from_featureclass('df_lines_sel_sjoin')
df_pts = pd.DataFrame.spatial.from_featureclass('df_point_sjoin')

df_lines = df_lines[['EST_NO', 'EST_NAME']]
df_pts = df_pts[['EST_NO', 'EST_NAME']]
df = pd.concat([df_lines, df_pts])

df = df.groupby('EST_NO').agg(
    est_name=('EST_NAME', 'first'),
    records_count=('EST_NO', 'count')
).reset_index()

df.to_csv(os.path.join(root, 'ecodata_dockshrimp.csv'))

# clean up temp files
arcpy.Delete_management(['temp_lyr', 'temp_lyr_pts'])
arcpy.Delete_management(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.Delete_management(['df_lines', 'df_lines_sel', 'df_lines_sel_sjoin', 'df_pts', 'df_point_sjoin'])
arcpy.Delete_management(tempgdb) # locked unless you reset



##############################################
# Sunflower sea start (Pycnopodia helianthoides)

root_i = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\scripts\inverts_database_R\Data'
csv1 = os.path.join(root_i, 'multispecies_smallmesh_trawl.csv')
csv2 = os.path.join(root_i, 'multispecies_pycno_geoduck.csv')

df1 = pd.read_csv(csv1)
df2 = pd.read_csv(csv2)

# Some dfs contain multiple species. Filter for the correct ones
df1 = df1[df1['Pycnopodia helianthoides']==1]
df2 = df2[df2.PYN==1]

# concatenate datasets with similar geometry. can get rid of most fields.
# also, some of the line datasets only have 1 set of coordinates. Separate these out.
df1 = df1.rename(columns={'Pycnopodia helianthoides':'PYN'})
df2 = df2.rename(columns={
    'LatDeep':'Start_latitude',
    'LonDeep':'Start_longitude',
    'LatShallow':'End_latitude',
    'LonShallow':'End_longitude'
})
df_12 = pd.concat([df1, df2])
df_lines = df_12[~df_12.End_latitude.isna()]

# generate temporary gdb
arcpy.CreateFileGDB_management(root_i, 'TEMP.gdb')
tempgdb = os.path.join(root_i, 'TEMP.gdb')

# define spatial reference
sr = arcpy.SpatialReference(4326)

# for line survey data, create line fc from points
df_lines.to_csv(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.env.workspace = tempgdb
arcpy.XYToLine_management(os.path.join(root_i, 'TEMP_lines.csv'), 'df_lines', 'Start_longitude', 'Start_latitude', 'End_longitude', 'End_latitude', spatial_reference = sr, attributes='NO_ATTRIBUTES')
# Remove lines that are absurdly long and cross land and must be an error
sel = arcpy.SelectLayerByAttribute_management('df_lines', 'NEW_SELECTION', 'Shape_Length < 0.13')
arcpy.CopyFeatures_management(sel, 'df_lines_sel')
del sel

# spatial joins
arcpy.MakeFeatureLayer_management('df_lines_sel', 'temp_lyr')
arcpy.SpatialJoin_analysis(
    'temp_lyr',
    estuaries,
    'df_lines_sel_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

# out to pandas, groupby estuary, combine line and point, to csv
df_lines = pd.DataFrame.spatial.from_featureclass('df_lines_sel_sjoin')

df = df_lines[['EST_NO', 'EST_NAME']]

df = df.groupby('EST_NO').agg(
    est_name=('EST_NAME', 'first'),
    records_count=('EST_NO', 'count')
).reset_index()

if len(df) > 0:
    df.to_csv(os.path.join(root, 'ecodata_sunflowerseastar.csv'))

# clean up temp files
arcpy.Delete_management(['temp_lyr'])
arcpy.Delete_management(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.Delete_management(['df_lines', 'df_lines_sel', 'df_lines_sel_sjoin'])
arcpy.Delete_management(tempgdb) # locked unless you reset

# NO RECORDS IN SPATIAL RANGE



##############################################
# Ochre sea start (Pisaster ochraceus)

root_i = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\scripts\inverts_database_R\Data'
csv1 = os.path.join(root_i, 'multispecies_smallmesh_trawl.csv')
csv2 = os.path.join(root_i, 'prawntrap_processed.csv')

df1 = pd.read_csv(csv1)
df2 = pd.read_csv(csv2)

# Some dfs contain multiple species. Filter for the correct ones
df1 = df1[df1['Pisaster ochraceus']==1]
df2 = df2[df2['Pisaster ochraceus']==1]

# concatenate datasets with similar geometry. can get rid of most fields.
# also, some of the line datasets only have 1 set of coordinates. Separate these out.
df_12 = pd.concat([df1, df2])
df_lines = df_12[~df_12.End_latitude.isna()]

# generate temporary gdb
arcpy.CreateFileGDB_management(root_i, 'TEMP.gdb')
tempgdb = os.path.join(root_i, 'TEMP.gdb')

# define spatial reference
sr = arcpy.SpatialReference(4326)

# for line survey data, create line fc from points
df_lines.to_csv(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.env.workspace = tempgdb
arcpy.XYToLine_management(os.path.join(root_i, 'TEMP_lines.csv'), 'df_lines', 'Start_longitude', 'Start_latitude', 'End_longitude', 'End_latitude', spatial_reference = sr, attributes='NO_ATTRIBUTES')
# Remove lines that are absurdly long and cross land and must be an error
sel = arcpy.SelectLayerByAttribute_management('df_lines', 'NEW_SELECTION', 'Shape_Length < 0.13')
arcpy.CopyFeatures_management(sel, 'df_lines_sel')
del sel

# spatial joins
arcpy.MakeFeatureLayer_management('df_lines_sel', 'temp_lyr')
arcpy.SpatialJoin_analysis(
    'temp_lyr',
    estuaries,
    'df_lines_sel_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

# out to pandas, groupby estuary, combine line and point, to csv
df_lines = pd.DataFrame.spatial.from_featureclass('df_lines_sel_sjoin')

df = df_lines[['EST_NO', 'EST_NAME']]

df = df.groupby('EST_NO').agg(
    est_name=('EST_NAME', 'first'),
    records_count=('EST_NO', 'count')
).reset_index()

if len(df) > 0:
    df.to_csv(os.path.join(root, 'ecodata_ochreseastar.csv'))

# clean up temp files
arcpy.Delete_management(['temp_lyr'])
arcpy.Delete_management(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.Delete_management(['df_lines', 'df_lines_sel', 'df_lines_sel_sjoin'])
arcpy.Delete_management(tempgdb) # locked unless you reset

# NO RECORDS IN SPATIAL RANGE


##############################################
# Littleneck clam (Leukoma staminea)

root_i = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\scripts\inverts_database_R\Data'
csv1 = os.path.join(root_i, 'clam_processed.csv')
df1 = pd.read_csv(csv1)

# Some dfs contain multiple species. Filter for the correct ones
df1 = df1[df1['Leukoma staminea']==1]

df_points = df1[['Latitude', 'Longitude']]

# generate temporary gdb
arcpy.CreateFileGDB_management(root_i, 'TEMP.gdb')
tempgdb = os.path.join(root_i, 'TEMP.gdb')

# define spatial reference
sr = arcpy.SpatialReference(4326)

# for point datasets, convert to fc
x = np.array(np.rec.fromrecords(df_points.values)) 
names = df_points.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToFeatureClass(x, os.path.join(tempgdb, f'df_pts'), ('longitude', 'latitude'), sr) 

arcpy.MakeFeatureLayer_management('df_pts', 'temp_lyr_pts')
arcpy.SpatialJoin_analysis(
    'temp_lyr_pts',
    estuaries,
    'df_point_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

# out to pandas, groupby estuary, combine line and point, to csv
df_pts = pd.DataFrame.spatial.from_featureclass('df_point_sjoin')

df = df_pts.groupby('EST_NO').agg(
    est_name=('EST_NAME', 'first'),
    records_count=('EST_NO', 'count')
).reset_index()

df.to_csv(os.path.join(root, 'ecodata_littlneckclam.csv'))

# clean up temp files
arcpy.Delete_management(['temp_lyr_pts'])
arcpy.Delete_management(['df_point', 'df_point_sjoin'])
arcpy.Delete_management(tempgdb) # locked unless you reset


##############################################
# Nuttall's cockle (Clinocardium nuttallii)

root_i = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\scripts\inverts_database_R\Data'
csv1 = os.path.join(root_i, 'multispecies_smallmesh_trawl.csv')
csv2 = os.path.join(root_i, 'clam_processed.csv')

df1 = pd.read_csv(csv1)
df2 = pd.read_csv(csv2)

# Some dfs contain multiple species. Filter for the correct ones
df_lines = df1[df1['Clinocardium']==1]
df_points = df2[df2['Clinocardium nuttallii']==1]

# generate temporary gdb
arcpy.CreateFileGDB_management(root_i, 'TEMP.gdb')
tempgdb = os.path.join(root_i, 'TEMP.gdb')

# define spatial reference
sr = arcpy.SpatialReference(4326)

# for line survey data, create line fc from points
df_lines.to_csv(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.env.workspace = tempgdb
arcpy.XYToLine_management(os.path.join(root_i, 'TEMP_lines.csv'), 'df_lines', 'Start_longitude', 'Start_latitude', 'End_longitude', 'End_latitude', spatial_reference = sr, attributes='NO_ATTRIBUTES')
# Remove lines that are absurdly long and cross land and must be an error
sel = arcpy.SelectLayerByAttribute_management('df_lines', 'NEW_SELECTION', 'Shape_Length < 0.13')
arcpy.CopyFeatures_management(sel, 'df_lines_sel')
del sel

# for point datasets, convert to fc
x = np.array(np.rec.fromrecords(df_points.values)) 
names = df_points.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToFeatureClass(x, os.path.join(tempgdb, f'df_pts'), ('longitude', 'latitude'), sr) 

arcpy.MakeFeatureLayer_management('df_pts', 'temp_lyr_pts')
arcpy.SpatialJoin_analysis(
    'temp_lyr_pts',
    estuaries,
    'df_point_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

# spatial joins
arcpy.MakeFeatureLayer_management('df_lines_sel', 'temp_lyr')
arcpy.SpatialJoin_analysis(
    'temp_lyr',
    estuaries,
    'df_lines_sel_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

# out to pandas, groupby estuary, combine line and point, to csv
df_lines = pd.DataFrame.spatial.from_featureclass('df_lines_sel_sjoin')
df_pts = pd.DataFrame.spatial.from_featureclass('df_point_sjoin')

df_lines = df_lines[['EST_NO', 'EST_NAME']]
df_pts = df_pts[['EST_NO', 'EST_NAME']]
df = pd.concat([df_lines, df_pts])

df = df.groupby('EST_NO').agg(
    est_name=('EST_NAME', 'first'),
    records_count=('EST_NO', 'count')
).reset_index()

if len(df) > 0:
    df.to_csv(os.path.join(root, 'ecodata_nuttallscockle.csv'))

# clean up temp files
arcpy.Delete_management(['temp_lyr', 'temp_lyr_pts'])
arcpy.Delete_management(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.Delete_management(['df_lines', 'df_lines_sel', 'df_lines_sel_sjoin', 'df_pts', 'df_point_sjoin'])
arcpy.Delete_management(tempgdb) # locked unless you reset



##############################################
# Horse clams - Fat Gaper / Pacific Gaper (Tresus capax/nuttallii)
# Grouping these together since most records are by genus.

root_i = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\scripts\inverts_database_R\Data'
csv1 = os.path.join(root_i, 'multispecies_smallmesh_trawl.csv')
csv2 = os.path.join(root_i, 'geoduck_horseclam.csv')
csv3 = os.path.join(root_i, 'clam_processed.csv')

df1 = pd.read_csv(csv1)
df2 = pd.read_csv(csv2)
df3 = pd.read_csv(csv3)

# Some dfs contain multiple species. Filter for the correct ones
df1 = df1[df1['Tresus']==1]
df2 = df2[df2.HSC==1]
df3 = df3[(df3['Tresus capax']==1) | (df3['Tresus nuttallii']==1)]

# concatenate datasets with similar geometry. can get rid of most fields.
# also, some of the line datasets only have 1 set of coordinates. Separate these out.
df1['HSC'] = 1
df2 = df2.rename(columns={
    'LatDeep':'Start_latitude',
    'LonDeep':'Start_longitude',
    'LatShallow':'End_latitude',
    'LonShallow':'End_longitude'
})
df_12 = pd.concat([df1, df2])
df_lines = df_12[~df_12.End_latitude.isna()]

df_points = df_12[df_12.End_latitude.isna()]
df_points = df_points.rename(columns={'Start_latitude':'Latitude', 'Start_longitude':'Longitude'})
df3['HSC'] = 1
df_points = pd.concat([df_points, df3])
df_points = df_points[['Latitude', 'Longitude']]

# generate temporary gdb
arcpy.CreateFileGDB_management(root_i, 'TEMP.gdb')
tempgdb = os.path.join(root_i, 'TEMP.gdb')

# define spatial reference
sr = arcpy.SpatialReference(4326)

# for point datasets, convert to fc
x = np.array(np.rec.fromrecords(df_points.values)) 
names = df_points.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToFeatureClass(x, os.path.join(tempgdb, f'df_pts'), ('longitude', 'latitude'), sr) 

# for line survey data, create line fc from points
df_lines.to_csv(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.env.workspace = tempgdb
arcpy.XYToLine_management(os.path.join(root_i, 'TEMP_lines.csv'), 'df_lines', 'Start_longitude', 'Start_latitude', 'End_longitude', 'End_latitude', spatial_reference = sr, attributes='NO_ATTRIBUTES')
# Remove lines that are absurdly long and cross land and must be an error
sel = arcpy.SelectLayerByAttribute_management('df_lines', 'NEW_SELECTION', 'Shape_Length < 0.13')
arcpy.CopyFeatures_management(sel, 'df_lines_sel')
del sel

# spatial joins
arcpy.MakeFeatureLayer_management('df_lines_sel', 'temp_lyr')
arcpy.SpatialJoin_analysis(
    'temp_lyr',
    estuaries,
    'df_lines_sel_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

arcpy.MakeFeatureLayer_management('df_pts', 'temp_lyr_pts')
arcpy.SpatialJoin_analysis(
    'temp_lyr_pts',
    estuaries,
    'df_point_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT',
    search_radius='500 meters'
)

# out to pandas, groupby estuary, combine line and point, to csv
df_lines = pd.DataFrame.spatial.from_featureclass('df_lines_sel_sjoin')
df_pts = pd.DataFrame.spatial.from_featureclass('df_point_sjoin')

df_lines = df_lines[['EST_NO', 'EST_NAME']]
df_pts = df_pts[['EST_NO', 'EST_NAME']]
df = pd.concat([df_lines, df_pts])

df = df.groupby('EST_NO').agg(
    est_name=('EST_NAME', 'first'),
    records_count=('EST_NO', 'count')
).reset_index()

df.to_csv(os.path.join(root, 'ecodata_horseclam.csv'))

# clean up temp files
arcpy.Delete_management(['temp_lyr', 'temp_lyr_pts'])
arcpy.Delete_management(os.path.join(root_i, 'TEMP_lines.csv'))
arcpy.Delete_management(['df_lines', 'df_lines_sel', 'df_lines_sel_sjoin', 'df_pts', 'df_point_sjoin'])
arcpy.Delete_management(tempgdb)


# IN GENERAL,
# ONLY the prawn data appears to have come from coastwide survey/catch data that is sufficient
# enough coverage where we could say something about overlap with estuaries.

