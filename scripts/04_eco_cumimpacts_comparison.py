#
# Get cumulative impacts scores from Clarke-Murray 2015 analysis that overlaps estuaries
# We will compare these values to our threat values in different ways.
#


import arcpy
import os
import pandas as pd


#########################################
# Data

root = r'C:/Users/cristianij/Documents/Projects/Estuary_threats/spatial'

eco_data_dir = os.path.join(root, '01_original/ecological_data_analysis')

# 2015 cumulative impacts data (this was given to me in a map package)
ci_mpkg = os.path.join(eco_data_dir,'CumulativeImpacts2015_identifying_thresholds.mpk')

# 2023 cumulative impacts DRAFT data
ci_2023 = os.path.join(eco_data_dir, 'CIM_Prelim.gdb/c_cumul_impact_total_draft')

# 2024
ci_2024 = os.path.join(eco_data_dir, 'Cumulative_Impacts_Pacific_Canada_2024.gdb/Cumulative_Impacts_Pacific_Canada')

# working gdb
working_gdb = os.path.join(root, r'02_working/ecological_data_analysis.gdb')

arcpy.env.workspace = working_gdb


#########################################
# 2015 data

# Extract map package
ci = os.path.join(eco_data_dir, 'cumulativeimpacts2015.gdb/cumulativeimpacts')
if not arcpy.Exists(ci):
    arcpy.ExtractPackage_management(ci_mpkg, eco_data_dir)

# Spatial join to estuaries
arcpy.env.qualifiedFieldNames = False
arcpy.SpatialJoin_analysis(
    os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_01_ALL'),
    ci,
    'cumimpacts2015_01_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option = 'INTERSECT',
    search_radius = 2000
)

# Dissolve on EST_NO and average impact_c_cumulative
arcpy.Dissolve_management('cumimpacts2015_01_sjoin', 'cumimpacts2015_02_dissolve', 'EST_NO', [['impact_c_cumulative', 'MEAN']])

# Output to point for mapping
arcpy.FeatureToPoint_management('cumimpacts2015_02_dissolve', 'cumimpacts2015_03_pt', 'INSIDE')


#########################################
# 2023 data

# Spatial join to estuaries
arcpy.env.qualifiedFieldNames = False
arcpy.SpatialJoin_analysis(
    os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_01_ALL'),
    ci_2023,
    'cumimpacts2023_01_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option = 'INTERSECT'
)
# No search distance needed. They expanded their grid. The only one not selected is part of a lake.
# Interstingly though, is that that lake one was part of the 2015 data. I wonder if the definition
# of the coastline changed since then to not include that as a tidal area.

# Dissolve on EST_NO and average impact_c_cumulative
arcpy.Dissolve_management(
    'cumimpacts2023_01_sjoin', 
    'cumimpacts2023_02_dissolve', 
    'EST_NO', 
    [
        ['Cumul_Impact_ALL', 'MEAN'],
        ['Cumul_Impact_sp', 'MEAN'],
        ['Cumul_Impact_dp', 'MEAN'],
        ['Cumul_Impact_bh', 'MEAN'],
        ['Cumul_Impact_eg', 'MEAN'],
        ['Cumul_Impact_sr', 'MEAN'],
        ['Cumul_Impact_kp', 'MEAN'],
        ])

# "one feature class that has an attribute for total cumulative impact for each 1 km grid cell, and 
# an attribute for cumulative impact of each constituent habitat (Benthic, Shallow Pelagic, Deep 
# Pelagic, Sponge Reef, Kelp Forest, Eelgrass) for each 1 km grid cell.""


# Output to point for mapping
arcpy.FeatureToPoint_management('cumimpacts2023_02_dissolve', 'cumimpacts2023_03_pt', 'INSIDE')



#########################################
# 2024 data

# Spatial join to estuaries
arcpy.env.qualifiedFieldNames = False
arcpy.SpatialJoin_analysis(
    os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_01_ALL'),
    ci_2024,
    'cumimpacts2024_01_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option = 'INTERSECT'
)
# No search distance needed. They expanded their grid. The only one not selected is part of a lake.
# Interstingly though, is that that lake one was part of the 2015 data. I wonder if the definition
# of the coastline changed since then to not include that as a tidal area.

# Dissolve on EST_NO and average impact_c_cumulative
arcpy.Dissolve_management(
    'cumimpacts2024_01_sjoin', 
    'cumimpacts2024_02_dissolve', 
    'EST_NO', 
    [
        ['Cumul_Impact_ALL', 'MEAN'],
        ['Cumul_Impact_sp', 'MEAN'],
        ['Cumul_Impact_dp', 'MEAN'],
        ['Cumul_Impact_bh', 'MEAN'],
        ['Cumul_Impact_eg', 'MEAN'],
        ['Cumul_Impact_sr', 'MEAN'],
        ['Cumul_Impact_kp', 'MEAN'],
        ])

# "one feature class that has an attribute for total cumulative impact for each 1 km grid cell, and 
# an attribute for cumulative impact of each constituent habitat (Benthic, Shallow Pelagic, Deep 
# Pelagic, Sponge Reef, Kelp Forest, Eelgrass) for each 1 km grid cell.""


# Output to point for mapping
arcpy.FeatureToPoint_management('cumimpacts2024_02_dissolve', 'cumimpacts2024_03_pt', 'INSIDE')



##########################################################################################################################


# Output to csv
field_names = [f.name for f in arcpy.ListFields('cumimpacts2015_02_dissolve') if f.name not in ['OBJECTID', 'Shape', 'priority', 'Shape_Length', 'Shape_Area']] 
cursor = arcpy.da.SearchCursor('cumimpacts2015_02_dissolve', field_names) 
est_pts = pd.DataFrame(data=[row for row in cursor], columns=field_names)
est_pts = est_pts.rename(columns={'MEAN_impact_c_cumulative':'2015_MEAN_impact_c_cumulative'})

field_names = [f.name for f in arcpy.ListFields('cumimpacts2023_02_dissolve') if f.name not in ['OBJECTID', 'Shape', 'priority', 'Shape_Length', 'Shape_Area']] 
cursor = arcpy.da.SearchCursor('cumimpacts2023_02_dissolve', field_names)
fnames_update = []
for field in field_names:
    if field=="EST_NO":
        fnames_update.append(field)
    else:
        fnames_update.append('2023_'+field)
est_pts_2023 = pd.DataFrame(data=[row for row in cursor], columns=fnames_update) 

field_names = [f.name for f in arcpy.ListFields('cumimpacts2024_02_dissolve') if f.name not in ['OBJECTID', 'Shape', 'priority', 'Shape_Length', 'Shape_Area']] 
cursor = arcpy.da.SearchCursor('cumimpacts2024_02_dissolve', field_names)
fnames_update = []
for field in field_names:
    if field=="EST_NO":
        fnames_update.append(field)
    else:
        fnames_update.append('2024_'+field)
est_pts_2024 = pd.DataFrame(data=[row for row in cursor], columns=fnames_update) 


est_pts = est_pts.merge(est_pts_2023, how='outer', on='EST_NO')
est_pts = est_pts.merge(est_pts_2024, how='outer', on='EST_NO')

est_pts.to_csv(os.path.join(root, 'ecodata_cumimpacts.csv'), index=False)
