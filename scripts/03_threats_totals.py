# John Cristiani john.cristiani@dfo-mpo.gc.ca
# October 2022

# This turned into a bit of a mess as functionality was added at different times.

# Summarize threat data by watershed/estuary

# We have now decided that our output will be arcsinh(activity/log10(area))
# We log watershed area, but not shoreline or estuary area.

import arcpy
import os
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import copy


#########################################
# Paths

# IN
root = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\spatial'
gdb_threats = os.path.join(root, '02_working/threats_02estuaries.gdb') # processed threat data
estuaries = os.path.join(root, r'02_working\estuaries_watersheds.gdb\estuaries_01_ALL')
watersheds = os.path.join(root, r'02_working\estuaries_watersheds.gdb\watersheds_05_clip')
shorebuff = os.path.join(root, r'02_working\estuaries_watersheds.gdb\shorezone_03_erase') # see notes in first script about how we changed this and how it is used
wat_erase = os.path.join(root, r'02_working\estuaries_watersheds.gdb\watersheds_shobufferase')

# OUT
gdb_out = os.path.join(root, '02_working/threats_03associate.gdb')

arcpy.env.workspace = gdb_threats

# Log areas?
log_areas = True

# arcsinh individual activities?
arcsinh_areas = False

# arcsinh after dividing by log area?
arcsinh_after = True


#########################################
# get dataframe of areas

field_names = ['EST_NO', 'Shape_Area'] 

cursor = arcpy.da.SearchCursor(estuaries, field_names) 
est_areas = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', 'area'])
# if log_areas:
#     est_areas['area_log'] = np.log10(est_areas.area)
cursor = arcpy.da.SearchCursor(watersheds, field_names) 
wts_areas = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', 'area'])
if log_areas:
    wts_areas['area_log'] = np.log10(wts_areas.area)
cursor = arcpy.da.SearchCursor(shorebuff, field_names) 
sho_areas = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', 'area'])
# if log_areas:
#     sho_areas['area_log'] = np.log10(sho_areas.area)
cursor = arcpy.da.SearchCursor(wat_erase, field_names) 
wer_areas = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', 'area'])
if log_areas:
    wer_areas['area_log'] = np.log10(wer_areas.area)
    
# create plots of logged and non-logged areas
'''
ax = (est_areas.area / 1000000).plot.hist(bins=100)
ax.set(xlabel='estuary area km2')
fig = ax.get_figure()
fig.savefig('plots/area_estuary.jpg')
ax.cla()
ax = (wts_areas.area / 1000000).plot.hist(bins=100)
ax.set(xlabel='watershed area km2')
fig = ax.get_figure()
fig.savefig('plots/area_wts.jpg')
ax.cla()
if log_areas:
    ax = (est_areas.area_log).plot.hist(bins=100)
    ax.set(xlabel='log10 estuary area m2')
    fig = ax.get_figure()
    fig.savefig('plots/area_estuary_log.jpg')
    ax.cla()
    ax = (wts_areas.area_log).plot.hist(bins=100)
    ax.set(xlabel='log10 watershed area m2')
    fig = ax.get_figure()
    fig.savefig('plots/area_wts_log.jpg')
    ax.cla()
'''

wts_areas['area_km'] = wts_areas.area / 1000000
params = {'mathtext.default': 'regular'}
plt.rcParams.update(params)
fig, (ax1, ax2) = plt.subplots(figsize=(12, 6), ncols=2, sharex=False, sharey=False)
sns.set_theme(style="white", palette=None)
sns.set_context('paper', rc={"axes.labelsize":18,"xtick.labelsize":16,'ytick.labelsize':16})
f = sns.histplot(wts_areas, x='area_km', bins=40, log_scale=False, ax=ax1)
f.set(xlabel = 'Watershed area $km^2$')
ax1.set_yscale('log')
f.xaxis.labelpad = 17
f.minorticks_off()
f.annotate("a", xy=(-0.1, 1.05), xycoords="axes fraction", fontsize=16)
f2 = sns.histplot(wts_areas, x='area_km', log_scale=True, ax=ax2)
f2.set(xlabel = 'Watershed area log10 $km^2$')
f2.minorticks_off()
f2.annotate("b", xy=(-0.1, 1.05), xycoords="axes fraction", fontsize=16)
fig.tight_layout(pad=5.0)
plt.savefig('plots/est_wts_areas.png', bbox_inches='tight')
plt.cla()
plt.clf()



# if logging, rename these to area
if log_areas:
    # est_areas = est_areas[['EST_NO', 'area_log']]
    # est_areas = est_areas.rename(columns={'area_log':'area'})
    wts_areas = wts_areas[['EST_NO', 'area_log']]
    wts_areas = wts_areas.rename(columns={'area_log':'area'})
    # sho_areas = sho_areas[['EST_NO', 'area_log']]
    # sho_areas = sho_areas.rename(columns={'area_log':'area'})
    wer_areas = wer_areas[['EST_NO', 'area_log']]
    wer_areas = wer_areas.rename(columns={'area_log':'area'})

#########################################
# exceptions

# most mar features will be divided by estuary area
# most ter features will be divided by watershed area
# sho features will be divided by shoreline-estuary zone area

# thrt_ter_generaldevelopment is the only feature divided by the watershed_erase feature
# climate features are not divided by area
# thrt_ter_debrislitter is not divided by area

# NOTE: in the tables, the common field is called "SUM", but the might include data that was
# averaged or feature counts. SUM was just the common name that was carried through.
fname = 'SUM'

# we are now also building an option to arcsinh/log the threat data as well


#########################################
# compile threat data and divide by watershed/estuary area

# dataframe of just EST_NO to join to
est_threats = est_areas[['EST_NO']]

features = arcpy.ListTables()
# We are not longer including population
features.remove('thrt_ter_population')

for f in features:

    print(f"Processing {f}")
    type = f.split('_')[1]

    fc = f

    field_names = ['EST_NO', fname]
    cursor = arcpy.da.SearchCursor(fc, field_names)
    df = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', f+'_nonadj'])

    # merge to appropriate area df
    if type == 'mar':
        df = est_areas.merge(df, how='left', on='EST_NO')
    elif f == 'thrt_ter_generaldevelopment':
        df = wer_areas.merge(df, how='left', on='EST_NO')
    elif f in ['thrt_ter_debrislitter'] or type == 'cli':
        pass
    elif type == 'ter':
        df = wts_areas.merge(df, how='left', on='EST_NO')
    elif type == 'sho':
        df = sho_areas.merge(df, how='left', on='EST_NO')

    # divide by area
    if not (f in ['thrt_ter_debrislitter'] or type == 'cli'):
        if arcsinh_areas:
            df[f] = np.arcsinh(df[f+'_nonadj']) / (df.area)
        else:
            df[f] = df[f+'_nonadj'] / (df.area)
    else:
        df[f] = df[f+'_nonadj']
    df = df[['EST_NO', f]]

    est_threats = est_threats.merge(df, how='left', on='EST_NO')


#########################################
# outputs

est_threats = est_threats.fillna(0)

# apply arcsinh to appropriate columns
if arcsinh_after:
    thrt_cols = list(est_threats.columns)
    thrts = [col for col in thrt_cols if ('_cli_' not in col and col not in ['thrt_ter_debrislitter'])]
    thrts.remove('EST_NO')
    est_threats[thrts] = np.arcsinh(est_threats[thrts])


name = 'estuary_threats'
if log_areas:
    name = 'estuary_threats_log'

# output to gdb table and join to estuaries
x = np.array(np.rec.fromrecords(est_threats.values)) 
names = est_threats.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToTable(x, os.path.join(gdb_out, f'estuary_threats_tbl'))

arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_est', 'EST_NO', os.path.join(gdb_out, 'estuary_threats_tbl'), 'EST_NO')
arcpy.CopyFeatures_management('temp_est', os.path.join(gdb_out, name))
arcpy.Delete_management('temp_est')
arcpy.env.workspace = gdb_out
arcpy.Delete_management('estuary_threats_tbl')

arcpy.FeatureToPoint_management(name, name+'_pt', 'INSIDE')
arcpy.AddXY_management(name+'_pt')
# output as csv
field_names = [f.name for f in arcpy.ListFields(name+'_pt') if f.name not in ['OBJECTID', 'Shape', 'priority', 'OBJECTID_1', 'EST_NO_1', 'ORIG_FID']] 
cursor = arcpy.da.SearchCursor(name+'_pt', field_names) 
est_pts = pd.DataFrame(data=[row for row in cursor], columns=field_names) 
est_pts.to_csv(os.path.join(root, name+'.csv'), index=False)


###############################################
# output a version of the raw data with the areas merged

field_names = ['EST_NO', 'Shape_Area'] 
cursor = arcpy.da.SearchCursor(estuaries, field_names) 
est_areas = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', 'area'])
cursor = arcpy.da.SearchCursor(watersheds, field_names) 
wts_areas = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', 'area'])
cursor = arcpy.da.SearchCursor(shorebuff, field_names) 
sho_areas = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', 'area'])
cursor = arcpy.da.SearchCursor(wat_erase, field_names) 
wer_areas = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', 'area'])

arcpy.env.workspace = gdb_threats
est_raw = est_areas[['EST_NO']]
features = arcpy.ListTables()
# We are not longer including population
features.remove('thrt_ter_population')
for f in features:
    field_names = ['EST_NO', fname]
    cursor = arcpy.da.SearchCursor(f, field_names)
    df = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', f+'_nonadj'])
    if not (f in ['thrt_ter_debrislitter'] or type == 'cli'):
        df[f] = df[f+'_nonadj']
    else:
        df[f] = df[f+'_nonadj']
    df = df[['EST_NO', f]]
    est_raw = est_raw.merge(df, how='left', on='EST_NO')

est_raw = est_raw.fillna(0)

est_raw = est_raw.merge(wts_areas, 'left', 'EST_NO')
est_raw = est_raw.rename(columns={'area':'area_watershed'})
est_raw = est_raw.merge(est_areas, 'left', 'EST_NO')
est_raw = est_raw.rename(columns={'area':'area_estuary'})
est_raw = est_raw.merge(sho_areas, 'left', 'EST_NO')
est_raw = est_raw.rename(columns={'area':'area_shorelinebuff'})
est_raw = est_raw.merge(wer_areas, 'left', 'EST_NO')
est_raw = est_raw.rename(columns={'area':'area_watershedErase'})

est_raw.to_csv(os.path.join(root, 'estuary_threats_RAW.csv'), index=False)




#########################################
#########################################
#########################################
# Plotting to compare difference scenarios of transforming data

'''
#########################################
# Threat plots

# Plot activites against estuary/watershed area

# dataframe of threats not divided by watershed/estuary area
arcpy.env.workspace = gdb_threats
est_raw = est_areas[['EST_NO']]
features = arcpy.ListTables()
for f in features:
    field_names = ['EST_NO', fname]
    cursor = arcpy.da.SearchCursor(f, field_names)
    df = pd.DataFrame(data=[row for row in cursor], columns=['EST_NO', f+'_nonadj'])
    if not (f in ['thrt_ter_debrislitter'] or type == 'cli'):
        if arcsinh_areas:
            df[f] = np.arcsinh(df[f+'_nonadj'])
        else:
            df[f] = df[f+'_nonadj']
    else:
        df[f] = df[f+'_nonadj']
    df = df[['EST_NO', f]]
    est_raw = est_raw.merge(df, how='left', on='EST_NO')

est_raw = est_raw.fillna(0)

# terrestrial
ter_cols = [col for col in est_raw.columns if ('_ter_' in col and col!='thrt_ter_debrislitter')]
id_cols = ter_cols.copy()
ter_cols.append('EST_NO')
df_ter = est_raw[ter_cols]
df_ter = pd.melt(df_ter, id_vars='EST_NO', value_vars=id_cols, var_name='activity', value_name='activity_area')
df_ter = df_ter.merge(wts_areas, how='left', on='EST_NO')

sns.set(rc={'figure.figsize':(11.7,8.27)})
sns.set_style('white')
ax = sns.scatterplot(data=df_ter, x='area', y='activity_area', hue='activity')
ax.set(xlabel='watershed area')
fig = ax.get_figure()
if arcsinh_areas:
    fig.savefig('plots/activities_ter_arcsinh')
else:
    fig.savefig('plots/activities_ter')
ax.cla()

# marine
mar_cols = [col for col in est_raw.columns if '_mar_' in col]
id_cols = mar_cols.copy()
mar_cols.append('EST_NO')
df_mar = est_raw[mar_cols]
df_mar = pd.melt(df_mar, id_vars='EST_NO', value_vars=id_cols, var_name='activity', value_name='activity_area')
df_mar = df_mar.merge(est_areas, how='left', on='EST_NO')

sns.set(rc={'figure.figsize':(11.7,8.27)})
sns.set_style('white')
ax = sns.scatterplot(data=df_mar, x='area', y='activity_area', hue='activity')
ax.set(xlabel='estuary area')
sns.move_legend(ax, "upper left", bbox_to_anchor=(1, 1))
fig = ax.get_figure()
if arcsinh_areas:
    fig.savefig('plots/activities_mar_arcsinh', bbox_inches='tight')
else:
    fig.savefig('plots/activities_mar', bbox_inches='tight')
ax.cla()


###############################################
# Facet plots

# We want to compare the distribution of activity values for:
#(1) arcsinh(activity)/log(area)
#vs
#(2) arcsinh(activity/log(area))

if arcsinh_areas is True and log_areas is True:
    # I can use est_threats df as is
    ter_cols = [col for col in est_threats.columns if ('_ter_' in col and col!='thrt_ter_debrislitter')]
    id_cols = ter_cols.copy()
    ter_cols.append('EST_NO')
    df_ter = est_threats[ter_cols]
    df_ter = pd.melt(df_ter, id_vars='EST_NO', value_vars=id_cols, var_name='activity', value_name='activity_area')

    g = sns.FacetGrid(df_ter, col='activity', col_wrap=3)
    g.map_dataframe(sns.histplot, x='activity_area')
    g.savefig('plots/activities_ter_arcsinhbefore')

if arcsinh_areas is False and log_areas is True:
    # First, do arcsinh on all "thrt" columns
    ter_cols = [col for col in est_threats.columns if ('_ter_' in col and col!='thrt_ter_debrislitter')]
    id_cols = ter_cols.copy()
    ter_cols.append('EST_NO')
    df_ter = est_threats[ter_cols]
    df_ter = pd.melt(df_ter, id_vars='EST_NO', value_vars=id_cols, var_name='activity', value_name='activity_area')
    df_ter['activity_area'] = np.arcsinh(df_ter.activity_area)

    g = sns.FacetGrid(df_ter, col='activity', col_wrap=3)
    g.map_dataframe(sns.histplot, x='activity_area')
    g.savefig('plots/activities_ter_arcsinafter')
'''


##############################################################
# Get a count of activities by type for mapping purposes

arcpy.env.workspace = gdb_out
est_pts = 'estuary_threats_log_pt'

### Terrestrial
fields = [f.name for f in arcpy.ListFields(est_pts, 'thrt_ter*')]
fields.append('thrt_sho_shorelinedevelopment')
total = len(fields)
arcpy.AddField_management(est_pts, 'ter_count', 'SHORT')
arcpy.AddField_management(est_pts, 'ter_count_prop', 'FLOAT')
fields_and = copy.deepcopy(fields)
fields_and.append('ter_count')

with arcpy.da.UpdateCursor(est_pts, fields_and) as cursor:
    for row in cursor:
        count = 0
        for i in range(0, total):
            if row[i] is None:
                continue
            if row[i] > 0:
                count += 1
        row[-1] = count
        cursor.updateRow(row)

with arcpy.da.UpdateCursor(est_pts, ['ter_count', 'ter_count_prop']) as cursor:
    for row in cursor:
        row[1] = row[0] / total
        cursor.updateRow(row)

### Marine
fields = [f.name for f in arcpy.ListFields(est_pts, 'thrt_mar*')]
total = len(fields)
arcpy.AddField_management(est_pts, 'mar_count', 'SHORT')
arcpy.AddField_management(est_pts, 'mar_count_prop', 'FLOAT')
fields_and = copy.deepcopy(fields)
fields_and.append('mar_count')
# get index of shipping since this one will be accounted for differently
ship_i = fields.index('thrt_mar_shipping')

with arcpy.da.UpdateCursor(est_pts, fields_and) as cursor:
    for row in cursor:
        count = 0
        for i in range(0, total):
            if row[i] is None:
                continue
            # Initially I was using these thresholds to account for shipping and very small values.
            # However, I am doing this tallying with data divided by area, not raw values.
            # For example, there is recreational boating clearly present in the Fraser estuary, but
            # because of the estuary size it gets diluted to a very small number which would then
            # get counted as zero with this number. But in the report, we use these activity counts
            # just to know what is present, we aren't as concerned with effective presence over the
            # entire estuary area.
            # if i == ship_i:
            #     if row[i] > 0.5:
            #         count += 1
            #     continue
            #if row[i] > 0.0000001:
            if row[i] > 0:
                count += 1
        row[-1] = count
        cursor.updateRow(row)

with arcpy.da.UpdateCursor(est_pts, ['mar_count', 'mar_count_prop']) as cursor:
    for row in cursor:
        row[1] = row[0] / total
        cursor.updateRow(row)

# Total
arcpy.AddField_management(est_pts, 'count_total', 'SHORT')
arcpy.AddField_management(est_pts, 'count_prop_all', 'FLOAT')

fields = [f.name for f in arcpy.ListFields(est_pts, 'thrt_ter*')]
fields.append('thrt_sho_shorelinedevelopment')
total_t = len(fields)
fields = [f.name for f in arcpy.ListFields(est_pts, 'thrt_mar*')]
total_m = len(fields)
total_possible = total_t + total_m

with arcpy.da.UpdateCursor(est_pts, ['mar_count', 'ter_count', 'count_total', 'count_prop_all']) as cursor:
    for row in cursor:
        row[2] = row[0] + row[1]
        row[3] = row[2] / total_possible
        cursor.updateRow(row)


# Symbolizing:
# https://support.esri.com/en/technical-article/000021785
