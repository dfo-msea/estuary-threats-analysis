# Cluster analysis
# pulled from Patrick's markdown script

# Reproducing here to output clusters for use in other plotting scripts


library(sf)
library(tidyverse)
library(vegan)
library(GGally)
library(patchwork)
library(dendextend)
library(english)
library(bcmaps)
library(RColorBrewer)
library(grid)
library(ggplotify)
library(NbClust)
library(indicspecies)
library(conover.test)
library(ggtext)


### Get Patrick's plotting theme object
source("plt_plot_theme.R")
theme_set(plt_theme)


### Inverse hyperbolic sine
ihs <- function(x) {
  y <- log(x + sqrt(x ^ 2 + 1))
  return(y)}


### Read in and format activity data
new_data <- read.csv("estuary_threats_log.csv")
new_data_sf <- st_as_sf(new_data, coords = c("POINT_X", "POINT_Y"), crs = "EPSG:3005")
bbox <- st_bbox(new_data_sf) + c(-20000, -20000, 20000, 20000)
coast_line <- bc_bound()
est_names <- new_data_sf$EST_NAME 
select_data <- new_data %>% select(-POINT_X, -POINT_Y, -EST_NO, -EST_NAME, -estuary_area) %>%
  select(-thrt_cli_tempchange, -thrt_cli_sealevelrise, -thrt_cli_precipchange, -thrt_cli_streamtempchange)
threats <- str_remove(str_remove(colnames(select_data), "thrt_"), "_adj")
threat_type <- sub(pattern = "_.*", replacement = "", x = threats)
threat_type[threat_type == "sho"] <- "ter"
threat_type <- factor(threat_type)
threats <- sub(pattern = "*..._", replacement = "", x = threats)
rownames(select_data) <- est_names
colnames(select_data) <- threats
select_data[is.na(select_data)] <- 0
rownames(select_data) <- est_names


### Standardize data and run PCA
select_data_stand <- select_data %>%
  decostand(method = "standardize") 
rownames(select_data_stand) <- est_names
est_pca <- rda(select_data_stand)
est_attributes <- data.frame(scores(est_pca, display = "species", choices = c(1:4)))
est_attributes$attribute <- rownames(est_attributes)


### Cluster
set.seed(7)
clusters <- as.dendrogram(hclust(d = dist(select_data_stand, method = "canberra"), method = "ward.D2"))
saveRDS(clusters, file='clusters.rds')
clust_number <- NbClust(data = select_data_stand, distance = "canberra", min.nc = 5, max.nc = 12, method = "ward.D2")
cluster_n <- 5
#colV <- c("dodgerblue3", "mediumpurple","#56B4E9", "grey50", "black", "forestgreen", "#33D1A6", "hotpink", "#D55E00", "#E69F00", "#F0E442")[1:cluster_n]
#colV <- c("grey50", "hotpink","black", "#F0E442", "#E69F00", "forestgreen", "dodgerblue3", "#56B4E9")
colV <- c("black", "hotpink", "#F0E442", "dodgerblue3","forestgreen")
clusters <- color_branches(clusters, k = cluster_n, col = colV)
clusters <- color_labels(clusters, k = cluster_n, col = colV)
cut_tree <- clusters %>% cutree(k = cluster_n)
# Manually change cluster numbers for plotting purposes
clust_numbers <- data.frame(name = names(cut_tree), orig_cluster = c(cut_tree), cluster = c(cut_tree)) %>% 
  mutate(cluster = ifelse(cluster == 2, 3,
                          ifelse(cluster == 3, 2,
                                 ifelse(cluster == 4, 5,
                                        ifelse(cluster== 5, 4, cluster))))) 
rownames(clust_numbers) <- NULL
clust_numbers <- left_join(clust_numbers, new_data %>% select(name = EST_NAME, EST_NO))
clus_colors <- labels_colors(clusters)[clust_numbers %>% arrange(cluster) %>% pull(name)]
clust_labs <- clus_colors %>% unique()
clusters <- clusters %>% rotate(clust_numbers %>% arrange(cluster) %>% pull(name))
# out to csv
write.csv(clust_numbers, 'clusters_estuaries.csv', row.names=FALSE)




### RESULTS ###



### Cluster figures
est_scores <- data.frame(scores(est_pca, display = "sites", choices = c(1:4), scaling = 3))
est_scores$name <- rownames(est_scores)
est_scores <- left_join(est_scores, clust_numbers)
est_scores$cluster <- as.factor(est_scores$cluster)

activity_scores <- data.frame(scores(est_pca, display = "species", choices = c(1:4), scaling = 3))
activity_scores$name <- rownames(activity_scores)
activity_scores$type <- threat_type
keep_thresh <- 0.9
activity_scores <- activity_scores %>% 
  mutate(PC1_select = ifelse(PC1 > quantile(PC1, probs = keep_thresh) | PC1 < quantile(PC1, probs = 1-keep_thresh), TRUE, FALSE)) %>% 
  mutate(PC2_select = ifelse(PC2 > quantile(PC2, probs = keep_thresh) | PC2 < quantile(PC2, probs = 1-keep_thresh), TRUE, FALSE)) %>% 
  mutate(PC3_select = ifelse(PC3 > quantile(PC3, probs = keep_thresh) | PC3 < quantile(PC3, probs = 1-keep_thresh), TRUE, FALSE)) %>% 
  mutate(PC4_select = ifelse(PC4 > quantile(PC4, probs = keep_thresh) | PC4 < quantile(PC4, probs = 1-keep_thresh), TRUE, FALSE)) %>% 
  mutate(any_select = (PC1_select + PC2_select + PC3_select + PC4_select)>0)

hull12 <- est_scores %>% 
  group_by(cluster) %>% 
  slice(chull(PC1, PC2))

hull34 <- est_scores %>% 
  group_by(cluster) %>% 
  slice(chull(PC3, PC4))

orig_data_points <- select(new_data_sf, geometry, EST_NAME)
orig_data_points <- left_join(orig_data_points, clust_numbers, by = c("EST_NAME" = "name"))

par(mar=c(0,0,0,0))
dendro_plot <- as.grob(~plot(clusters, leaflab = "none", axes = FALSE))


select_data_stand <- left_join(select_data_stand %>% mutate(name = rownames(select_data_stand)), clust_numbers %>% select(-orig_cluster))
select_data_stand <- select_data_stand %>%
  mutate(cluster = factor(cluster))

cluster_loadings <- select_data_stand %>% 
  group_by(cluster) %>% 
  summarise_all(mean) %>% 
  gather(key = name, value = value, -cluster) %>% 
  left_join(select_data_stand %>% 
              group_by(cluster) %>% 
              summarise_all(function(x) {sd(x, na.rm = TRUE)/sqrt(n())}) %>% 
              gather(key = name, value = SE, -cluster)) %>% 
  left_join(activity_scores)

max_cluster <- cluster_loadings %>% 
  group_by(name, type) %>% 
  summarise(max_value = max(value), max_cluster = cluster[value == max(value)]) %>% 
  arrange(max_cluster, desc(max_value))

cluster_loadings <- cluster_loadings %>% 
  left_join(max_cluster) %>% 
  arrange(max_cluster, max_value)

cluster_loadings$name <- factor(cluster_loadings$name, levels = unique(cluster_loadings$name), ordered = TRUE) 

type_col_all <- data.frame(name = unique(cluster_loadings$name), 
                           colour = c("navy", "forestgreen")[cluster_loadings %>% 
                                                                        select(name, type) %>% 
                                                                        unique() %>% 
                                                                        pull(type) %>% 
                                                                        factor() %>% 
                                                                        as.numeric()]) %>% 
  left_join(cluster_loadings %>% select(name, max_cluster) %>% unique())

dend <- wrap_elements(dendro_plot)

ggplot(data.frame(votes = clust_number$Best.nc[1,]) %>% 
         mutate(selected = ifelse(votes == cluster_n, TRUE, FALSE)) %>% filter(votes>3) %>% 
         mutate(votes = factor(votes, levels = 4:12, ordered = TRUE)), aes(x = votes, fill = selected))+
  geom_histogram(stat = "count")+
  xlab("# of clusters")+
  ylab("# of supporting indices")+
  scale_fill_manual(values = c("grey30", "red"), guide = "none")+
  theme_classic()+
  scale_y_continuous(breaks = seq(0, 9, by = 3))+
  scale_x_discrete(drop = FALSE)+
  coord_cartesian(expand = FALSE)+
  
  dend +
  
  plot_annotation(tag_levels = "a")





### PCA figures
(ggplot(est_scores, aes(x = PC1, y = PC2))+
    geom_point(aes(fill = cluster), pch = 21, color = "white", stroke = 0)+
    geom_polygon(data = hull12, aes(fill = cluster), color = NA, alpha = 0.2)+
    geom_text(data = activity_scores %>% filter(PC1_select == TRUE), aes(label = name, color = type))+
    geom_text(data = activity_scores %>% filter(PC2_select == TRUE), aes(label = name, color = type))+
    theme_classic()+
    scale_shape_manual(values = 21:26, name = "Cluster")+
    scale_fill_manual(values = clust_labs, name = "Cluster")+
    scale_color_manual(values = c("dodgerblue3","forestgreen"), name = "", guide = "none", drop = FALSE)+
    scale_x_continuous(limits = c(-3, 3))+
    coord_equal())/

  (ggplot(est_scores, aes(x = PC4, y = PC3))+
     geom_point(aes(fill = cluster), pch = 21, color = "white", stroke = 0)+
     geom_polygon(data = hull34 %>% filter(cluster!=9), aes(fill = cluster), color = NA, alpha = 0.2)+
     geom_text(data = activity_scores %>% filter(PC3_select == TRUE), aes(label = name, color = type))+
     geom_text(data = activity_scores %>% filter(PC4_select == TRUE), aes(label = name, color = type))+
     theme_classic()+
     scale_shape_manual(values = 21:26, name = "Cluster", guide='none')+
     scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
     scale_color_manual(values = c("dodgerblue3","forestgreen"), name = "Cluster", guide = "none", drop = FALSE)+
     scale_x_continuous(limits = c(-2.5, 2.5))+
     coord_equal())+

  plot_annotation(tag_levels = "a")




### Activity data by cluster
set.seed(7)
ind_act <- multipatt(x = select_data , cluster = select_data_stand$cluster, func = "IndVal.g", max.order = 1)
ind_act.df <- ind_act$sign  %>% filter(p.value<0.05)
ind_act_no_cluster.df <- ind_act$sign  %>% filter(p.value>=0.05)
ind_act.df <- data.frame(data.matrix(ind_act.df %>% select(s.1:s.5)), index = ind_act.df$index)
ind_act.df <- ind_act.df %>%
  arrange(index)
ind_act.df$activity <- rownames(ind_act.df)
ind_act.df <- left_join(ind_act.df, type_col_all, by = c("activity" = "name"))
ind_act.df <- ind_act.df %>%
  select(s.1:s.5, activity, index, colour) %>%
  gather(key = cluster, value = correlation, -activity, -index, -colour) %>%
  filter(correlation > 0) %>%
  mutate(cluster = gsub(pattern = "s.", replacement = "",cluster)) %>% 
  mutate(cluster = factor(cluster, levels = 1:cluster_n, ordered = TRUE))
ind_act.df$activity <- factor(ind_act.df$activity, levels = ind_act.df$activity, ordered = TRUE)
ind_act.df <- ind_act.df %>% mutate(type = ifelse(ind_act.df$colour == "navy", "marine", "terrestrial"))

ggplot(ind_act.df, aes(x = cluster, y = activity, fill = cluster))+
  geom_tile()+
  scale_fill_manual(values = clust_labs, guide = "none", drop = FALSE)+
  ylab("Anthropogenic activity")+
  scale_x_discrete(drop = FALSE)+
  facet_grid(cluster~., scales = "free_y", space = "free_y")+
  theme(
    strip.background = element_blank(),
    strip.text.y = element_blank())+
  geom_point(aes(x = 0.5, color = type), size = 3)+
  scale_color_manual(values = c("dodgerblue", "forestgreen"), "Activity type")+
  theme(legend.position = "bottom")



### Map of clusters
ggplot()+
  geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
  geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
  geom_sf(data = orig_data_points, aes(fill = factor(cluster), shape = factor(cluster)), stroke = 0.2, size = 2)+
  scale_fill_manual(values = clust_labs, name = "Cluster")+
  scale_shape_manual(values = 21:26, name = "Cluster")+
  coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
  guides(fill = guide_legend(override.aes = list(size=3)))+
  theme_bw()



###############################


### Maps of activity data
# (NOT TESTED YET SINCE POST CSAS MEETING CODE CHANGES. Would need to change shapes, as above.)


# Human activities that are significantly associated with cluster 1 (black). The
# activity names are labelled above the individual panels. Row (a) shows the 
# activities maped by estuary with the size of the points showing the relative 
# value of the activity. Row (b) shows the distribution of values across 
# estuaries in each cluster. All values have been scaled between their highest 
# and lowest values to facilitate comparison across activities.

# data_long_sf <- new_data_sf %>% 
#   left_join(clust_numbers %>% select(-orig_cluster), by = c("EST_NAME" = "name", "EST_NO")) %>% 
#   gather(key = activity, value = value, -cluster, -EST_NO, -EST_NAME, -geometry) %>% 
#   mutate(activity = str_remove(str_remove(activity, "thrt_"), "_adj")) %>% 
#   mutate(type = sub(pattern = "_.*", replacement = "", x = activity)) %>% 
#   mutate(type = ifelse(type %in% c("sho", "ter"), "terrestrial", ifelse(type == "mar", "marine", "climate"))) %>% 
#   mutate(activity = sub(pattern = "*..._", replacement = "", x = activity)) %>% 
#   group_by(activity) %>% 
#   mutate(value_range = decostand(value, "range")) %>% 
#   left_join(ind_act.df %>% select(activity, cluster_association = cluster)) %>% 
#   mutate(cluster_association = ifelse(is.na(cluster_association), 0, cluster_association))
# 
# ggplot(data_long_sf %>% filter(cluster_association == 1))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(cluster_association == 1), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")
# 
# 
# # Human activities that are significantly associated with cluster 2 (grey). The 
# # activity names are labelled above the individual panels. Row (a) shows the 
# # activities maped by estuary with the size of the points showing the relative 
# # value of the activity. Row (b) shows the distribution of values across 
# # estuaries in each cluster. All values have been scaled between their highest 
# # and lowest values to facilitate comparison across activities.
# 
# ggplot(data_long_sf %>% filter(cluster_association == 2))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(cluster_association == 2), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")
# 
# 
# # Human activities that are significantly associated with cluster 3 (pink). The activity names are labelled above the individual panels. Row (a) shows the activities maped by estuary with the size of the points showing the relative value of the activity. Row (b) shows the distribution of values across estuaries in each cluster. All values have been scaled between their highest and lowest values to facilitate comparison across activities.
# 
# ggplot(data_long_sf %>% filter(cluster_association == 3))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(cluster_association == 3), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")
# 
# 
# 
# # Human activities that are significantly associated with cluster 4 (orange). The activity names are labelled above the individual panels. Row (a) shows the activities maped by estuary with the size of the points showing the relative value of the activity. Row (b) shows the distribution of values across estuaries in each cluster. All values have been scaled between their highest and lowest values to facilitate comparison across activities.
# ggplot(data_long_sf %>% filter(cluster_association == 4))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(cluster_association == 4), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")
# 
# 
# 
# # Human activities that are significantly associated with cluster 5 (yellow). The activity names are labelled above the individual panels. Row (a) shows the activities maped by estuary with the size of the points showing the relative value of the activity. Row (b) shows the distribution of values across estuaries in each cluster. All values have been scaled between their highest and lowest values to facilitate comparison across activities.
# ggplot(data_long_sf %>% filter(cluster_association == 5))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(cluster_association == 5), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")
# 
# 
# 
# # Human activities that are significantly associated with cluster 7 (light blue). The activity names are labelled above the individual panels. Row (a) shows the activities maped by estuary with the size of the points showing the relative value of the activity. Row (b) shows the distribution of values across estuaries in each cluster. All values have been scaled between their highest and lowest values to facilitate comparison across activities.
# ggplot(data_long_sf %>% filter(cluster_association == 7))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(cluster_association == 7), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")
# 
# 
# 
# # Human activities that are significantly associated with cluster 8 (dark blue). The activity names are labelled above the individual panels. Row (a) shows the activities maped by estuary with the size of the points showing the relative value of the activity. Row (b) shows the distribution of values across estuaries in each cluster. All values have been scaled between their highest and lowest values to facilitate comparison across activities.
# ggplot(data_long_sf %>% filter(activity %in% (data_long_sf %>% filter(cluster_association == 8) %>% pull(activity) %>% unique())[1:5]))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(activity %in% (data_long_sf %>% filter(cluster_association == 8) %>% pull(activity) %>% unique())[1:5]), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")
# 
# 
# 
# # First five human activities that are not significantly associated with any cluster (see next two figures for other activities that fall into this category). The activity names are labelled above the individual panels. Row (a) shows the activities maped by estuary with the size of the points showing the relative value of the activity. Row (b) shows the distribution of values across estuaries in each cluster. All values have been scaled between their highest and lowest values to facilitate comparison across activities.
# ggplot(data_long_sf %>% filter(activity %in% (data_long_sf %>% filter(cluster_association == 0) %>% pull(activity) %>% unique())[1:5]))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(activity %in% (data_long_sf %>% filter(cluster_association == 0) %>% pull(activity) %>% unique())[1:5]), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")
# 
# 
# 
# # Second five human activities that are not significantly associated with any cluster (see previous and next figures for other activities that fall into this category). The activity names are labelled above the individual panels. Row (a) shows the activities maped by estuary with the size of the points showing the relative value of the activity. Row (b) shows the distribution of values across estuaries in each cluster. All values have been scaled between their highest and lowest values to facilitate comparison across activities.
# ggplot(data_long_sf %>% filter(activity %in% (data_long_sf %>% filter(cluster_association == 0) %>% pull(activity) %>% unique())[6:10]))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(activity %in% (data_long_sf %>% filter(cluster_association == 0) %>% pull(activity) %>% unique())[6:10]), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")
# 
# 
# 
# # Second five human activities that are not significantly associated with any cluster (see previous two figures for other activities that fall into this category). The activity names are labelled above the individual panels. Row (a) shows the activities maped by estuary with the size of the points showing the relative value of the activity. Row (b) shows the distribution of values across estuaries in each cluster. All values have been scaled between their highest and lowest values to facilitate comparison across activities.
# ggplot(data_long_sf %>% filter(activity %in% (data_long_sf %>% filter(cluster_association == 0) %>% pull(activity) %>% unique())[11:15]))+
#   geom_sf(data = bc_neighbours() %>% filter(name %in% c("Washington", "Alaska", "British Columbia")) %>% st_crop(bbox), fill = "grey85")+
#   geom_sf(data = watercourses_5M() %>% st_crop(bbox), color = "grey40")+
#   geom_sf(aes(fill = factor(cluster), size = value_range), shape = 21, stroke = 0.2)+
#   scale_fill_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   scale_radius("Scaled value")+
#   coord_sf(xlim = bbox[c(1,3)], ylim = bbox[c(2,4)], expand = FALSE)+
#   theme_bw()+
#   theme(strip.background = element_blank())+
#   facet_wrap(~activity, nrow = 1)+
#   theme(legend.position = "bottom")+
#   
#   ggplot(data_long_sf %>% filter(activity %in% (data_long_sf %>% filter(cluster_association == 0) %>% pull(activity) %>% unique())[11:15]), aes(x = factor(cluster), y = value_range, color = factor(cluster)))+
#   geom_boxplot(outlier.shape = NA)+
#   geom_jitter(width = 0.2, height = 0)+
#   scale_color_manual(values = clust_labs, name = "Cluster", guide = "none")+
#   xlab("Cluster")+
#   ylab("Scaled value")+
#   theme_bw()+
#   facet_wrap(~activity, nrow = 1)+
#   theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), strip.background = element_blank())+
#   
#   plot_layout(nrow = 2) + plot_annotation(tag_levels = "a")






