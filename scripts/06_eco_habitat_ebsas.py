

import arcpy
import os
import pandas as pd
import numpy as np
import shutil
arcpy.CheckOutExtension("Spatial")
from arcpy.ia import *
from arcpy.sa import *



#########################################
# Data

root = r'C:/Users/cristianij/Documents/Projects/Estuary_threats/spatial'

eco_data_dir = os.path.join(root, '01_original/ecological_data_analysis')

# threat data - original
threats_dir = os.path.join(root, '01_original/threat_data')

land = os.path.join(root, r'02_working/estuaries_watersheds.gdb/land_erase')
estuaries = os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_01_ALL')

# working gdb
working_gdb = os.path.join(root, r'02_working/ecological_data_analysis.gdb')

arcpy.env.workspace = working_gdb



#########################################
# EBSAs
# https://open.canada.ca/data/en/dataset/d2d6057f-d7c4-45d9-9fd9-0a58370577e0
# Identify EBSAS those that overlap estuaries, 
# then look at EBSA reports for criteria used to identify each site for relevance to estuaries

ebsas_shp = os.path.join(eco_data_dir, 'DFO_EBSA/DFO_EBSA.shp')
ebsas_reports = os.path.join(eco_data_dir, 'DFO_EBSA/ebsas_reports.xlsx') # This is where I have
# manually made notes based one ebsa reports

# clip to estuaries to get a unique list
arcpy.PairwiseClip_analysis(ebsas_shp, estuaries, 'temp_ebsas_clip')
# spatial join to associate with estuaries
arcpy.SpatialJoin_analysis(estuaries, ebsas_shp, 'temp_ebsas_sjoin', 'JOIN_ONE_TO_MANY', 'KEEP_COMMON')

# output sjoin fc to pandas
field_names = ['EST_NO', 'ID', 'Name', 'Bioregion'] 
cursor = arcpy.da.SearchCursor('temp_ebsas_sjoin', field_names) 
ebsas = pd.DataFrame(data=[row for row in cursor], columns=field_names)
# out to csv
ebsas.to_csv(os.path.join(root, 'ecodata_ebsas.csv'), index=False)

# move xlsx sheet to out folder:
shutil.copyfile(ebsas_reports, os.path.join(root, 'ecodata_ebsas_reports.xlsx'))

arcpy.Delete_management(['temp_ebsas_clip', 'temp_ebsas_sjoin'])



#########################################
# Eeglrass polygons

sg = os.path.join(eco_data_dir, 'eelgrass_BC_2022_July.gdb/eelgrass_BC_polygons_explode')

# polygons have significant errors in them
# repair takes a while
arcpy.CopyFeatures_management(sg, 'sgpolys_01_repairgeo')
arcpy.RepairGeometry_management('sgpolys_01_repairgeo')
arcpy.CheckGeometry_management('sgpolys_01_repairgeo')

# Intersect with estuaries (clip doesn't work here since there are two estuaries that border each
# other and clip doesn't separate them)
arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'estuary_area', '!Shape_Area!') # if you do it this way without adding field first then it makes it a text field. I correct this later.
arcpy.PairwiseIntersect_analysis(['sgpolys_01_repairgeo', 'temp_est'], 'sgpolys_02_intersect')

# dissolve
arcpy.PairwiseDissolve_analysis('sgpolys_02_intersect', 'sgpolys_03_dissolve', 'EST_NO', [['estuary_area', 'FIRST']])

# Calculate field as sg area divided by estuary area
arcpy.AddField_management('sgpolys_03_dissolve', 'proportion_eelgrass', 'FLOAT')
with arcpy.da.UpdateCursor('sgpolys_03_dissolve', ['FIRST_estuary_area', 'Shape_Area', 'proportion_eelgrass']) as cursor:
    for row in cursor:
        row[2] = row[1]/float(row[0])
        cursor.updateRow(row)

# out to csv
field_names = ['EST_NO', 'proportion_eelgrass'] 
cursor = arcpy.da.SearchCursor('sgpolys_03_dissolve', field_names) 
sgpolys = pd.DataFrame(data=[row for row in cursor], columns=field_names)
sgpolys.to_csv(os.path.join(root, 'ecodata_eelgrassPolys.csv'), index=False)

arcpy.Delete_management(['temp_est', 'sgpolys_02_intersect'])



#########################################
# Eeglrass biobands

# There should be new data from CORI (Nov 2022), but we will still need to use the data as well to 
# get full coverage.
# For any overlaps, just take the newer one after clipping and spatially joining to estuaries.
# http://www.coastalandoceans.com/FormRepository/main.asp

shorezone_old_ln = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/Unit_lines')
shorezone_old_tb = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/BioBand')
shorezone_new_ln = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/Unit_lines')
shorezone_new_tb = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/BioBand')

arcpy.MakeFeatureLayer_management(shorezone_old_ln, 'temp_szoldln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_szoldln', 'PHY_IDENT', shorezone_old_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_szoldln', 'temp')
where = """ZOS IN ('P', 'C')""" # patchy or continuous
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezoneold')
arcpy.Delete_management(['temp_szoldln', 'temp'])

arcpy.MakeFeatureLayer_management(shorezone_new_ln, 'temp_sznewln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_sznewln', 'PHY_IDENT', shorezone_new_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_sznewln', 'temp')
where = """EELG IN ('P', 'C')"""
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezonenew')
arcpy.Delete_management(['temp_sznewln', 'temp'])

# add old/new field for distinguishing later
arcpy.AddField_management('temp_shorezoneold', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezoneold', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'OLD'
        cursor.updateRow(row)
arcpy.AddField_management('temp_shorezonenew', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezonenew', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'NEW'
        cursor.updateRow(row)

# SHOREZONE notes:
# You can have two identical lines for eelgrass presence that have the same PHY_IDENT, and they only
# differ based on the CROSS_LINK field. This will match the PHY_IDENT except for the last two
# slashes. These refer to subtidal, intertidal and supratidal, and then the number breaks these down
# even further (see page 15 in newer 2017 manual).
# Therefore, when I go add by estuary, I should keep both of these lines since they represent
# presence in two different spots.

arcpy.Merge_management(['temp_shorezoneold', 'temp_shorezonenew'], 'temp_shorezone_merge')
arcpy.Delete_management(['temp_shorezonenew', 'temp_shorezoneold'])

# add field of estuary area
# buffer estuaries by x meters to account for differing coastlines
arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'estuary_area', '!Shape_Area!')
arcpy.PairwiseBuffer_analysis('temp_est', 'temp_est_buff', 100)

# intersect with buffered estuaries
arcpy.PairwiseIntersect_analysis(['temp_est_buff', 'temp_shorezone_merge'], 'temp_sz_intersect')

# Dissolve
# the unit length field will account for the crosslink issue
arcpy.AddField_management('temp_sz_intersect','unit_length', 'DOUBLE')
arcpy.CalculateField_management('temp_sz_intersect', 'unit_length', '!Shape_Length!')
arcpy.PairwiseDissolve_analysis('temp_sz_intersect', 'temp_sz_dissolve', ['EST_NO', 'new_old'], [['unit_length', 'SUM'], ['estuary_area', 'FIRST']])

# do some spot checks where they both exist in an estuary
# if both new and old exist in an estuary, remove the old
fields = ['OBJECTID', 'EST_NO', 'new_old']
cursor = arcpy.da.SearchCursor('temp_sz_dissolve', fields) 
sz = pd.DataFrame(data=[row for row in cursor], columns=fields)
sz['dup'] = sz.duplicated(subset=['EST_NO'], keep=False)
oids_toDelete = list(sz[(sz.dup == True) & (sz.new_old=='OLD')].OBJECTID)

arcpy.MakeFeatureLayer_management('temp_sz_dissolve', 'temp_lyr', '"OBJECTID" NOT IN {}'.format(str(tuple(oids_toDelete))))
arcpy.CopyFeatures_management('temp_lyr', 'eelgrass_biobands_shorezone')

# create new field and normalize length by estuary area
arcpy.AddField_management('eelgrass_biobands_shorezone', 'length_div_estArea', 'DOUBLE')
with arcpy.da.UpdateCursor('eelgrass_biobands_shorezone', ['length_div_estArea', 'SUM_unit_length', 'FIRST_estuary_area']) as cursor:
    for row in cursor:
        row[0] = row[1] / float(row[2])
        cursor.updateRow(row)

# out to csv
field_names = ['EST_NO', 'length_div_estArea'] 
cursor = arcpy.da.SearchCursor('eelgrass_biobands_shorezone', field_names) 
eg = pd.DataFrame(data=[row for row in cursor], columns=field_names)
eg.to_csv(os.path.join(root, 'ecodata_eelgrassBiobands.csv'), index=False)

arcpy.Delete_management(['temp_shorezone_merge', 'temp_sz_dissolve', 'temp_sz_intersect', 'temp_lyr', 'temp_est'])



#########################################
# Saltmarsh biobands

# Carrie thinks the "bioband groupings" table is new. In the past they did just PUC. 
# https://alaskafisheries.noaa.gov/mapping/DataDictionary/
# What I'm finding is that there are actually multiple codes for one type depending on if it 
# is Alaska, BC or Washington.
# See table on page 54 of the 2017 manual.
# With the data I have now, it seems that SAL was the old code and SAMB is the new one.
# PUC and SAMA no longer seem to exist or have records.

shorezone_old_ln = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/Unit_lines')
shorezone_old_tb = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/BioBand')
shorezone_new_ln = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/Unit_lines')
shorezone_new_tb = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/BioBand')

arcpy.MakeFeatureLayer_management(shorezone_old_ln, 'temp_szoldln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_szoldln', 'PHY_IDENT', shorezone_old_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_szoldln', 'temp')
where = """SAL IN ('P', 'C')""" # patchy or continuous
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezoneold')
arcpy.Delete_management(['temp_szoldln', 'temp'])

arcpy.MakeFeatureLayer_management(shorezone_new_ln, 'temp_sznewln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_sznewln', 'PHY_IDENT', shorezone_new_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_sznewln', 'temp')
where = """SAMB IN ('P', 'C')"""
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezonenew')
arcpy.Delete_management(['temp_sznewln', 'temp'])

# add old/new field for distinguishing later
arcpy.AddField_management('temp_shorezoneold', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezoneold', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'OLD'
        cursor.updateRow(row)
arcpy.AddField_management('temp_shorezonenew', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezonenew', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'NEW'
        cursor.updateRow(row)

arcpy.Merge_management(['temp_shorezoneold', 'temp_shorezonenew'], 'temp_shorezone_merge')
arcpy.Delete_management(['temp_shorezonenew', 'temp_shorezoneold'])

# add field of estuary area
# buffer estuaries by x meters to account for differing coastlines
arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'estuary_area', '!Shape_Area!')
arcpy.PairwiseBuffer_analysis('temp_est', 'temp_est_buff', 100)

# intersect with buffered estuaries
arcpy.PairwiseIntersect_analysis(['temp_est_buff', 'temp_shorezone_merge'], 'temp_sz_intersect')

# Dissolve
# the unit length field will account for the crosslink issue
arcpy.AddField_management('temp_sz_intersect','unit_length', 'DOUBLE')
arcpy.CalculateField_management('temp_sz_intersect', 'unit_length', '!Shape_Length!')
arcpy.PairwiseDissolve_analysis('temp_sz_intersect', 'temp_sz_dissolve', ['EST_NO', 'new_old'], [['unit_length', 'SUM'], ['estuary_area', 'FIRST']])

# do some spot checks where they both exist in an estuary
# if both new and old exist in an estuary, remove the old
fields = ['OBJECTID', 'EST_NO', 'new_old']
cursor = arcpy.da.SearchCursor('temp_sz_dissolve', fields) 
sz = pd.DataFrame(data=[row for row in cursor], columns=fields)
sz['dup'] = sz.duplicated(subset=['EST_NO'], keep=False)
oids_toDelete = list(sz[(sz.dup == True) & (sz.new_old=='OLD')].OBJECTID)

arcpy.MakeFeatureLayer_management('temp_sz_dissolve', 'temp_lyr', '"OBJECTID" NOT IN {}'.format(str(tuple(oids_toDelete))))
arcpy.CopyFeatures_management('temp_lyr', 'saltmarsh_biobands_shorezone')

# create new field and normalize length by estuary area
arcpy.AddField_management('saltmarsh_biobands_shorezone', 'length_div_estArea', 'DOUBLE')
with arcpy.da.UpdateCursor('saltmarsh_biobands_shorezone', ['length_div_estArea', 'SUM_unit_length', 'FIRST_estuary_area']) as cursor:
    for row in cursor:
        row[0] = row[1] / float(row[2])
        cursor.updateRow(row)

# out to csv
field_names = ['EST_NO', 'length_div_estArea'] 
cursor = arcpy.da.SearchCursor('saltmarsh_biobands_shorezone', field_names) 
eg = pd.DataFrame(data=[row for row in cursor], columns=field_names)
eg.to_csv(os.path.join(root, 'ecodata_saltmarshBiobands.csv'), index=False)

arcpy.Delete_management(['temp_shorezone_merge', 'temp_sz_dissolve', 'temp_sz_intersect', 'temp_lyr', 'temp_est', 'temp_est_buff'])




#########################################
# Kelp - canopy biobands
# combine giant and bull kelp

shorezone_old_ln = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/Unit_lines')
shorezone_old_tb = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/BioBand')
shorezone_new_ln = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/Unit_lines')
shorezone_new_tb = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/BioBand')

arcpy.MakeFeatureLayer_management(shorezone_old_ln, 'temp_szoldln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_szoldln', 'PHY_IDENT', shorezone_old_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_szoldln', 'temp')
where = """MAC IN ('P', 'C') or NER IN ('C', 'P')""" # patchy or continuous
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezoneold')
arcpy.Delete_management(['temp_szoldln', 'temp'])

arcpy.MakeFeatureLayer_management(shorezone_new_ln, 'temp_sznewln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_sznewln', 'PHY_IDENT', shorezone_new_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_sznewln', 'temp')
where = """BUKE IN ('P', 'C') or GIKE IN ('P', 'C')"""
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezonenew')
arcpy.Delete_management(['temp_sznewln', 'temp'])

# add old/new field for distinguishing later
arcpy.AddField_management('temp_shorezoneold', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezoneold', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'OLD'
        cursor.updateRow(row)
arcpy.AddField_management('temp_shorezonenew', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezonenew', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'NEW'
        cursor.updateRow(row)

arcpy.Merge_management(['temp_shorezoneold', 'temp_shorezonenew'], 'temp_shorezone_merge')
arcpy.Delete_management(['temp_shorezonenew', 'temp_shorezoneold'])

# add field of estuary area
# buffer estuaries by x meters to account for differing coastlines
arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'estuary_area', '!Shape_Area!')
arcpy.PairwiseBuffer_analysis('temp_est', 'temp_est_buff', 100)

# intersect with buffered estuaries
arcpy.PairwiseIntersect_analysis(['temp_est_buff', 'temp_shorezone_merge'], 'temp_sz_intersect')

# Dissolve
# the unit length field will account for the crosslink issue
arcpy.AddField_management('temp_sz_intersect','unit_length', 'DOUBLE')
arcpy.CalculateField_management('temp_sz_intersect', 'unit_length', '!Shape_Length!')
arcpy.PairwiseDissolve_analysis('temp_sz_intersect', 'temp_sz_dissolve', ['EST_NO', 'new_old'], [['unit_length', 'SUM'], ['estuary_area', 'FIRST']])

# do some spot checks where they both exist in an estuary
# if both new and old exist in an estuary, remove the old
fields = ['OBJECTID', 'EST_NO', 'new_old']
cursor = arcpy.da.SearchCursor('temp_sz_dissolve', fields) 
sz = pd.DataFrame(data=[row for row in cursor], columns=fields)
sz['dup'] = sz.duplicated(subset=['EST_NO'], keep=False)
oids_toDelete = list(sz[(sz.dup == True) & (sz.new_old=='OLD')].OBJECTID)

arcpy.MakeFeatureLayer_management('temp_sz_dissolve', 'temp_lyr', '"OBJECTID" NOT IN {}'.format(str(tuple(oids_toDelete))))
arcpy.CopyFeatures_management('temp_lyr', 'kelpBrownCanopy_biobands_shorezone')

# create new field and normalize length by estuary area
arcpy.AddField_management('kelpBrownCanopy_biobands_shorezone', 'length_div_estArea', 'DOUBLE')
with arcpy.da.UpdateCursor('kelpBrownCanopy_biobands_shorezone', ['length_div_estArea', 'SUM_unit_length', 'FIRST_estuary_area']) as cursor:
    for row in cursor:
        row[0] = row[1] / float(row[2])
        cursor.updateRow(row)

# out to csv
field_names = ['EST_NO', 'length_div_estArea'] 
cursor = arcpy.da.SearchCursor('kelpBrownCanopy_biobands_shorezone', field_names) 
eg = pd.DataFrame(data=[row for row in cursor], columns=field_names)
eg.to_csv(os.path.join(root, 'ecodata_kelpBrownCanopyBiobands.csv'), index=False)

arcpy.Delete_management(['temp_shorezone_merge', 'temp_sz_dissolve', 'temp_sz_intersect', 'temp_lyr', 'temp_est', 'temp_est_buff'])




#########################################
# Kelp - brown biobands

shorezone_old_ln = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/Unit_lines')
shorezone_old_tb = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/BioBand')
shorezone_new_ln = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/Unit_lines')
shorezone_new_tb = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/BioBand')

arcpy.MakeFeatureLayer_management(shorezone_old_ln, 'temp_szoldln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_szoldln', 'PHY_IDENT', shorezone_old_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_szoldln', 'temp')
where = """CHB IN ('P', 'C') or SBR IN ('C', 'P')""" # patchy or continuous
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezoneold')
arcpy.Delete_management(['temp_szoldln', 'temp'])

arcpy.MakeFeatureLayer_management(shorezone_new_ln, 'temp_sznewln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_sznewln', 'PHY_IDENT', shorezone_new_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_sznewln', 'temp')
where = """DABK IN ('P', 'C') or SOBK IN ('P', 'C')"""
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezonenew')
arcpy.Delete_management(['temp_sznewln', 'temp'])

# add old/new field for distinguishing later
arcpy.AddField_management('temp_shorezoneold', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezoneold', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'OLD'
        cursor.updateRow(row)
arcpy.AddField_management('temp_shorezonenew', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezonenew', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'NEW'
        cursor.updateRow(row)

arcpy.Merge_management(['temp_shorezoneold', 'temp_shorezonenew'], 'temp_shorezone_merge')
arcpy.Delete_management(['temp_shorezonenew', 'temp_shorezoneold'])

# add field of estuary area
# buffer estuaries by x meters to account for differing coastlines
arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'estuary_area', '!Shape_Area!')
arcpy.PairwiseBuffer_analysis('temp_est', 'temp_est_buff', 100)

# intersect with buffered estuaries
arcpy.PairwiseIntersect_analysis(['temp_est_buff', 'temp_shorezone_merge'], 'temp_sz_intersect')

# Dissolve
# the unit length field will account for the crosslink issue
arcpy.AddField_management('temp_sz_intersect','unit_length', 'DOUBLE')
arcpy.CalculateField_management('temp_sz_intersect', 'unit_length', '!Shape_Length!')
arcpy.PairwiseDissolve_analysis('temp_sz_intersect', 'temp_sz_dissolve', ['EST_NO', 'new_old'], [['unit_length', 'SUM'], ['estuary_area', 'FIRST']])

# do some spot checks where they both exist in an estuary
# if both new and old exist in an estuary, remove the old
fields = ['OBJECTID', 'EST_NO', 'new_old']
cursor = arcpy.da.SearchCursor('temp_sz_dissolve', fields) 
sz = pd.DataFrame(data=[row for row in cursor], columns=fields)
sz['dup'] = sz.duplicated(subset=['EST_NO'], keep=False)
oids_toDelete = list(sz[(sz.dup == True) & (sz.new_old=='OLD')].OBJECTID)

arcpy.MakeFeatureLayer_management('temp_sz_dissolve', 'temp_lyr', '"OBJECTID" NOT IN {}'.format(str(tuple(oids_toDelete))))
arcpy.CopyFeatures_management('temp_lyr', 'kelpBrown_biobands_shorezone')

# create new field and normalize length by estuary area
arcpy.AddField_management('kelpBrown_biobands_shorezone', 'length_div_estArea', 'DOUBLE')
with arcpy.da.UpdateCursor('kelpBrown_biobands_shorezone', ['length_div_estArea', 'SUM_unit_length', 'FIRST_estuary_area']) as cursor:
    for row in cursor:
        row[0] = row[1] / float(row[2])
        cursor.updateRow(row)

# out to csv
field_names = ['EST_NO', 'length_div_estArea'] 
cursor = arcpy.da.SearchCursor('kelpBrown_biobands_shorezone', field_names) 
eg = pd.DataFrame(data=[row for row in cursor], columns=field_names)
eg.to_csv(os.path.join(root, 'ecodata_kelpBrownBiobands.csv'), index=False)

arcpy.Delete_management(['temp_shorezone_merge', 'temp_sz_dissolve', 'temp_sz_intersect', 'temp_lyr', 'temp_est', 'temp_est_buff'])




#########################################
# Kelp - green (Ulva) biobands

shorezone_old_ln = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/Unit_lines')
shorezone_old_tb = os.path.join(eco_data_dir, r'shorezone/BC_historical_ShoreZone_Mapping_17aug20.gdb/BioBand')
shorezone_new_ln = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/Unit_lines')
shorezone_new_tb = os.path.join(eco_data_dir, r'shorezone/BC_ShoreZone_Mapping_17nov22.gdb/BioBand')

arcpy.MakeFeatureLayer_management(shorezone_old_ln, 'temp_szoldln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_szoldln', 'PHY_IDENT', shorezone_old_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_szoldln', 'temp')
where = """ULV IN ('P', 'C')""" # patchy or continuous
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezoneold')
arcpy.Delete_management(['temp_szoldln', 'temp'])

arcpy.MakeFeatureLayer_management(shorezone_new_ln, 'temp_sznewln')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_sznewln', 'PHY_IDENT', shorezone_new_tb, 'PHY_IDENT')
arcpy.CopyFeatures_management('temp_sznewln', 'temp')
where = """GRAL IN ('P', 'C')"""
sel = arcpy.SelectLayerByAttribute_management('temp', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezonenew')
arcpy.Delete_management(['temp_sznewln', 'temp'])

# add old/new field for distinguishing later
arcpy.AddField_management('temp_shorezoneold', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezoneold', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'OLD'
        cursor.updateRow(row)
arcpy.AddField_management('temp_shorezonenew', 'new_old', 'TEXT')
with arcpy.da.UpdateCursor('temp_shorezonenew', ['new_old']) as cursor:
    for row in cursor:
        row[0] = 'NEW'
        cursor.updateRow(row)

arcpy.Merge_management(['temp_shorezoneold', 'temp_shorezonenew'], 'temp_shorezone_merge')
arcpy.Delete_management(['temp_shorezonenew', 'temp_shorezoneold'])

# add field of estuary area
# buffer estuaries by x meters to account for differing coastlines
arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'estuary_area', '!Shape_Area!')
arcpy.PairwiseBuffer_analysis('temp_est', 'temp_est_buff', 100)

# intersect with buffered estuaries
arcpy.PairwiseIntersect_analysis(['temp_est_buff', 'temp_shorezone_merge'], 'temp_sz_intersect')

# Dissolve
# the unit length field will account for the crosslink issue
arcpy.AddField_management('temp_sz_intersect','unit_length', 'DOUBLE')
arcpy.CalculateField_management('temp_sz_intersect', 'unit_length', '!Shape_Length!')
arcpy.PairwiseDissolve_analysis('temp_sz_intersect', 'temp_sz_dissolve', ['EST_NO', 'new_old'], [['unit_length', 'SUM'], ['estuary_area', 'FIRST']])

# do some spot checks where they both exist in an estuary
# if both new and old exist in an estuary, remove the old
fields = ['OBJECTID', 'EST_NO', 'new_old']
cursor = arcpy.da.SearchCursor('temp_sz_dissolve', fields) 
sz = pd.DataFrame(data=[row for row in cursor], columns=fields)
sz['dup'] = sz.duplicated(subset=['EST_NO'], keep=False)
oids_toDelete = list(sz[(sz.dup == True) & (sz.new_old=='OLD')].OBJECTID)

arcpy.MakeFeatureLayer_management('temp_sz_dissolve', 'temp_lyr', '"OBJECTID" NOT IN {}'.format(str(tuple(oids_toDelete))))
arcpy.CopyFeatures_management('temp_lyr', 'kelpGreenUlva_biobands_shorezone')

# create new field and normalize length by estuary area
arcpy.AddField_management('kelpGreenUlva_biobands_shorezone', 'length_div_estArea', 'DOUBLE')
with arcpy.da.UpdateCursor('kelpGreenUlva_biobands_shorezone', ['length_div_estArea', 'SUM_unit_length', 'FIRST_estuary_area']) as cursor:
    for row in cursor:
        row[0] = row[1] / float(row[2])
        cursor.updateRow(row)

# out to csv
field_names = ['EST_NO', 'length_div_estArea'] 
cursor = arcpy.da.SearchCursor('kelpGreenUlva_biobands_shorezone', field_names) 
eg = pd.DataFrame(data=[row for row in cursor], columns=field_names)
eg.to_csv(os.path.join(root, 'ecodata_kelpGreenUlvaBiobands.csv'), index=False)

arcpy.Delete_management(['temp_shorezone_merge', 'temp_sz_dissolve', 'temp_sz_intersect', 'temp_lyr', 'temp_est', 'temp_est_buff'])



#########################################
# Kelp polys

# use kelp polys from DST

# CANOPY
bullkelp = r'C:\Users\cristianij\Documents\Projects\DST_pilot\spatial\01_original\DSTpilot_ecologicalData.gdb\mpatt_eco_plants_bullkelp_polygons_data'
giantkelp = r'C:\Users\cristianij\Documents\Projects\DST_pilot\spatial\01_original\DSTpilot_ecologicalData.gdb\mpatt_eco_plants_giantkelp_polygons_data'
# GENERAL - is bull/giant, but they couldn't distinguish, so they just put it into a separate dataset
generalkelp = r'C:\Users\cristianij\Documents\Projects\DST_pilot\spatial\01_original\DSTpilot_ecologicalData.gdb\mpatt_eco_plants_generalkelp_polygons_data'

arcpy.Merge_management([bullkelp, giantkelp, generalkelp], 'kc_00_merge')
arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'estuary_area', '!Shape_Area!')
arcpy.PairwiseIntersect_analysis(['kc_00_merge', 'temp_est'], 'kc_01_intersect')
arcpy.PairwiseDissolve_analysis('kc_01_intersect', 'kelpBrownCanopy_polys', 'EST_NO', [['estuary_area', 'FIRST']])
# Calculate field as sg area divided by estuary area
arcpy.AddField_management('kelpBrownCanopy_polys', 'proportion_kelpcanopy', 'FLOAT')
with arcpy.da.UpdateCursor('kelpBrownCanopy_polys', ['FIRST_estuary_area', 'Shape_Area', 'proportion_kelpcanopy']) as cursor:
    for row in cursor:
        row[2] = row[1]/float(row[0])
        cursor.updateRow(row)
field_names = ['EST_NO', 'proportion_kelpcanopy'] 
cursor = arcpy.da.SearchCursor('kelpBrownCanopy_polys', field_names) 
sgpolys = pd.DataFrame(data=[row for row in cursor], columns=field_names)
sgpolys.to_csv(os.path.join(root, 'ecodata_kelpBrownCanopyPolys.csv'), index=False)
arcpy.Delete_management(['kc_00_merge', 'temp_est', 'kc_01_intersect'])



#########################################
# Rugosity

# Rugosity layers are on GIS hub. Search for rugosity, which turns up bottom patch stuff, but there
# are regional layers called "predictor layers", which includes rugosity, and were used for creating
# the bottom patch layers.
# https://www.gis-hub.ca/dataset/substrate-preds-wcvi-20m
# https://www.gis-hub.ca/dataset/substrate-preds-qcs-20m
# https://www.gis-hub.ca/dataset/substrate-preds-sog-20m
# https://www.gis-hub.ca/dataset/substrate-preds-hg-20m
# https://www.gis-hub.ca/dataset/substrate-preds-ncc-20m

rugo_rast = os.path.join(eco_data_dir, 'rugosity/substrate_20m_preds_{}/rugosity.tif')
rasters = ['hg', 'ncc', 'qcs', 'sog', 'wcvi']

# It's not working by combining (ia.merge) all the rasters, its keep crashing.
# I can run them separately, but I worry about where they overlap.
# There is one estuary between QCS and SOG where they overlap.
# There is one estuary between SOG and WCVI where they overlap.
# And one estuary between WCVI and QCS where they overlap.
# Annoyingly, the values differ slightly. Not sure why.
# Also, the cell sizes are the same, but I think the cell alignment is slightly different between
# some of them.

# convert estuaries to raster with same cell size as rugosity raster. This avoids missing cell
# centers with the feature polygon.
arcpy.CopyFeatures_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'priority', 1)

rug_ALL = pd.DataFrame(columns=['EST_NO', 'MEAN'])

for rast in rasters:

    print(f'processing {rast}')    
    r = arcpy.Raster(rugo_rast.format(rast))

    arcpy.env.SnapRaster = r
    arcpy.PolygonToRaster_conversion('temp_est', 'EST_NO', 'temp_est_rast', 'MAXIMUM_AREA', 'priority', r)

    outZSaT = ZonalStatisticsAsTable(
        'temp_est_rast', 
        'Value', 
        r, 
        f'rugosity_estuaries_{rast}', 
        'DATA', 
        'MEAN')

    # change Value to EST_NO
    arcpy.AlterField_management(
        f'rugosity_estuaries_{rast}',
        'Value',
        'EST_NO',
        'EST_NO')

    arcpy.Delete_management('temp_est_rast')

    field_names = ['EST_NO', 'MEAN'] 
    cursor = arcpy.da.SearchCursor(f'rugosity_estuaries_{rast}', field_names) 
    rug = pd.DataFrame(data=[row for row in cursor], columns=field_names)
    rug_ALL = pd.concat([rug_ALL, rug])

# groupby and take average for those with multiple records
rug_summ = rug_ALL.groupby('EST_NO').agg({'MEAN':'mean'}).reset_index()

rug_summ.to_csv(os.path.join(root, 'ecodata_rugosityMean.csv'), index=False)

arcpy.Delete_management(['temp_est'])
# tbls = arcpy.ListTables('rugosity_estuaries*')
# for t in tbls:
#     arcpy.Delete_management(t)




#########################################
# Substrate types
# use 20m dataset from GIS Hub

substrate_rast = os.path.join(eco_data_dir, 'substrate_20m/{}_substrate_20m.tif')
rasters = ['HG', 'NCC', 'QCS', 'SOG', 'WCVI']

# Since we are dealing with categories we can't do Zonal Statistics

# convert rasters to poly and clip to estuaries
polys = []
for rast in rasters:
    print(f'processing {rast}')    
    r = arcpy.Raster(substrate_rast.format(rast))
    arcpy.RasterToPolygon_conversion(r, 'memory/temp', 'NO_SIMPLIFY')
    # clip to cut down on future processing
    arcpy.PairwiseClip_analysis('memory/temp', estuaries, f'temp_substrate_{rast}')
    polys.append(f'temp_substrate_{rast}')
    arcpy.Delete_management('memory/temp')

arcpy.Merge_management(polys, 'temp_substrate_merge')
arcpy.PairwiseDissolve_analysis('temp_substrate_merge', 'temp_substrate_dissolve', 'gridcode', multi_part='SINGLE_PART')

arcpy.MakeFeatureLayer_management(estuaries, 'temp_est')
arcpy.CalculateField_management('temp_est', 'estuary_area', '!Shape_Area!')
arcpy.PairwiseIntersect_analysis(['temp_est', 'temp_substrate_dissolve'], 'temp_substrate_intersect')
arcpy.PairwiseDissolve_analysis('temp_substrate_intersect', 'substrate20m_polys', ['EST_NO', 'gridcode'], [['estuary_area', 'FIRST']])

arcpy.AddField_management('substrate20m_polys', 'substrate_type', 'TEXT')
with arcpy.da.UpdateCursor('substrate20m_polys', ['gridcode', 'substrate_type']) as cursor:
    for row in cursor:
        if row[0]==1:
            row[1]='Rock'
        elif row[0]==2:
            row[1]='Mixed'
        elif row[0]==3:
            row[1]='Sand'
        elif row[0]==4:
            row[1]='Mud'
        cursor.updateRow(row)

# Calculate field as sg area divided by estuary area
arcpy.AddField_management('substrate20m_polys', 'proportion_substrateType', 'DOUBLE')
with arcpy.da.UpdateCursor('substrate20m_polys', ['FIRST_estuary_area', 'Shape_Area', 'proportion_substrateType']) as cursor:
    for row in cursor:
        row[2] = row[1]/float(row[0])
        cursor.updateRow(row)
field_names = ['EST_NO', 'gridcode', 'substrate_type', 'proportion_substrateType'] 
cursor = arcpy.da.SearchCursor('substrate20m_polys', field_names) 
sub = pd.DataFrame(data=[row for row in cursor], columns=field_names)
sub.to_csv(os.path.join(root, 'ecodata_substrateType.csv'), index=False)

arcpy.Delete_management(['temp_est', 'temp_substrate_merge', 'temp_substrate_dissolve'])
for poly in polys:
    arcpy.Delete_management(poly)



#########################################
# Habitat type count per estuary

# Read in the csvs for eelgrass, kelp types, and saltmarsh
# If it has a record for either polys and biobands then that counts, but I won't distinguish between the two.
# I will distinguish between kelp types though.

csv = os.path.join(root, 'ecodata_{}.csv')
eg_bb = csv.format('eelgrassBiobands')
eg_pl = csv.format('eelgrassPolys')
kb_bb = csv.format('kelpBrownBiobands')
kc_bb = csv.format('kelpBrownCanopyBiobands')
kc_pl = csv.format('kelpBrownCanopyPolys')
ku_bb = csv.format('kelpGreenUlvaBiobands')
sm_bb = csv.format('saltmarshBiobands')
substrate_csv = os.path.join(root, 'ecodata_substrateType.csv')
rugo = csv.format('rugosityMean')

# get df of all estuary numbers to join to
field_names = ['EST_NO'] 
cursor = arcpy.da.SearchCursor(estuaries, field_names) 
est = pd.DataFrame(data=[row for row in cursor], columns=field_names)

# read in df one by one, change to 1/0 in new field, drop field, merge
df = pd.read_csv(eg_bb)
df.loc[df.length_div_estArea > 0, 'eg_bb'] = 1
df = df[['EST_NO', 'eg_bb']]
est = est.merge(df, 'left', 'EST_NO')

df = pd.read_csv(eg_pl)
df.loc[df.proportion_eelgrass > 0, 'eg_pl'] = 1
df = df[['EST_NO', 'eg_pl']]
est = est.merge(df, 'left', 'EST_NO')

df = pd.read_csv(kb_bb)
df.loc[df.length_div_estArea > 0, 'kb_bb'] = 1
df = df[['EST_NO', 'kb_bb']]
est = est.merge(df, 'left', 'EST_NO')

df = pd.read_csv(kc_bb)
df.loc[df.length_div_estArea > 0, 'kc_bb'] = 1
df = df[['EST_NO', 'kc_bb']]
est = est.merge(df, 'left', 'EST_NO')

df = pd.read_csv(kc_pl)
df.loc[df.proportion_kelpcanopy > 0, 'kc_pl'] = 1
df = df[['EST_NO', 'kc_pl']]
est = est.merge(df, 'left', 'EST_NO')

df = pd.read_csv(ku_bb)
df.loc[df.length_div_estArea > 0, 'ku_bb'] = 1
df = df[['EST_NO', 'ku_bb']]
est = est.merge(df, 'left', 'EST_NO')

df = pd.read_csv(sm_bb)
df.loc[df.length_div_estArea > 0, 'sm_bb'] = 1
df = df[['EST_NO', 'sm_bb']]
est = est.merge(df, 'left', 'EST_NO')

# create new eelgrass and kelp canopy fields and check for redundant scores between biobands and polys
est.loc[(est.eg_bb == 1.0)|(est.eg_pl == 1.0), 'eg'] = 1
est = est.drop(columns=['eg_bb', 'eg_pl'])
est.loc[(est.kc_bb == 1.0)|(est.kc_pl == 1.0), 'kc'] = 1
est = est.drop(columns=['kc_bb', 'kc_pl'])

df = pd.read_csv(substrate_csv)
df2 = df.groupby('EST_NO')['substrate_type'].count().reset_index()
df2 = df2.rename(columns={'substrate_type':'substrateType_count'})
df2 = df2[['EST_NO', 'substrateType_count']]
est = est.merge(df2, 'left', 'EST_NO')

df = pd.read_csv(rugo)
df.loc[df.MEAN > 1.000647783279419, 'rugo_high'] = 1  # threshold (calculated below)
df = df[['EST_NO', 'rugo_high']]
est = est.merge(df, 'left', 'EST_NO')


# add across into new field
est['habitat_richness'] = est.iloc[:, 1:].sum(axis=1)

# out to csv
est.to_csv(os.path.join(root, 'ecodata_habitatCount.csv'), index=False)



#########################################
# Substrate type per estuary

# substrate_csv = os.path.join(root, 'ecodata_substrateType.csv')
# df = pd.read_csv(substrate_csv)

# df2 = df.groupby('EST_NO')['substrate_type'].count().reset_index()
# df2 = df2.rename(columns={'substrate_type':'substrateType_count'})

# df2.to_csv(os.path.join(root, 'ecodata_substrateTypeCount.csv'), index=False)



#########################################
# Estuary importance rankings
# Importance of estuary for bird habitat.

original_estuaries = os.path.join(root, '01_original/PECP_Estuary_Shapefiles_PUBLIC/PECP_estuary_polys_ranked_2019_PUBLIC.shp')

field_names = ['EST_NO', 'EST_NAME', 'IMP_CL2019'] 
cursor = arcpy.da.SearchCursor(original_estuaries, field_names) 
est = pd.DataFrame(data=[row for row in cursor], columns=field_names)
est.to_csv(os.path.join(root, 'ecodata_estuaryImportanceBirds.csv'), index=False)



#########################################
# Rugosity quintile
# We are now looking at the top 20% of rugosity values as a distinct habitat type for the habitat
# richness figure.
# The quintile is based on ALL values though, not just the estuary pixels.
# I just need the value for filtering the estuary rugosity values, so use np.percentile

rugo_rast = os.path.join(eco_data_dir, 'rugosity/substrate_20m_preds_{}/rugosity.tif')
rasters = ['hg', 'ncc', 'qcs', 'sog', 'wcvi']

out_folder = os.path.join(eco_data_dir, 'rugosity')

# arcgis has a size limit on raster to numpy, so need to split
for rast in rasters:
    if not arcpy.Exists(os.path.join(out_folder, f'rug_{rast}1.TIF')):
        print(f'processing {rast}') 
        r = arcpy.Raster(rugo_rast.format(rast))
        arcpy.SplitRaster_management(r, out_folder, f"rug_{rast}", "NUMBER_OF_TILES", "TIFF", "NEAREST", "5 5") 
        # the number and arrangement of tiles really mattered. If I need to run this again, just alter these 
        # for each section until it works. For instance, for some I can do '2 1', but for wcvi, I had to do '5 5'

a = np.array([np.nan])
arcpy.env.workspace = out_folder
rst = arcpy.ListRasters('rug_*')
for rs in rst:
    print(f'processing {rs}') 
    b = arcpy.RasterToNumPyArray(rs)
    c = b[b>=0]
    a = np.concatenate((a,c), axis=None)

# upper quintile
quint = np.nanquantile(a, 0.8)
print(quint)
# 1.000647783279419

# delete rasters
# for rs in rst:
#     arcpy.Delete_management(rs)

arcpy.env.workspace = working_gdb
