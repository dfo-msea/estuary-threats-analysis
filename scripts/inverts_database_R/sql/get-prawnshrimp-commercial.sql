SELECT
H.H_Key AS HKey,
H.line_num as line_num,
H.tdate AS date,
H.lat_deg + H.lat_min / 60 AS latitude,
-(H.long_deg + H.long_min / 60) AS longitude,
CASE WHEN H.prawn_wgt > 0 THEN 1 ELSE 0 END AS prawn,
CASE WHEN H.coonstripe_wgt > 0 THEN 1 ELSE 0 END AS shrimp_coonstripe
FROM Shellfish_Logs.dbo.PrawnCatch H
WHERE H.lat_deg IS NOT NULL AND H.long_deg IS NOT NULL
ORDER BY H.H_Key, H.line_num
