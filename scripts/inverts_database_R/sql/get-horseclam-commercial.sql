SELECT
H.counter AS counter,
H.species AS species,
H.Lat_deg + H.Lat_min / 60 AS latitude,
-(H.Long_deg + H.Long_min / 60) AS longitude
FROM Shellfish_Logs.dbo.DiveLogs H
WHERE H.Lat_deg IS NOT NULL AND H.Species IN ('76D', '76E', '76F')
ORDER BY H.counter;
