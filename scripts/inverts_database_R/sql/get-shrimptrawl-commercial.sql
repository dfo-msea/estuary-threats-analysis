SELECT
H.ID AS id,
H.tdate AS date,
H.lat_deg + H.lat_min / 60 AS latitude,
-(H.long_deg + H.long_min / 60) AS longitude,
CASE WHEN H.prawns > 0 THEN 1 ELSE 0 END AS prawn,
CASE WHEN H.docks > 0 THEN 1 ELSE 0 END AS shrimp_coonstripe
FROM Shellfish_Logs.dbo.ShrimpLogs H
WHERE H.lat_deg IS NOT NULL AND H.long_deg IS NOT NULL
ORDER BY H.id
