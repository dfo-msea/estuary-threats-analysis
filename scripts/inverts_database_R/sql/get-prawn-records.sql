SELECT
H.[Key] AS HKey,
H.Source_type  AS Source_type,
H.set_num AS Set_ID,
H.year AS Year,
H.month AS Month,
H.day AS Day,
H.stat_area AS Major_stat_area_code,
H.sub_area AS Stat_subarea_code,
H.startLAT AS Start_latitude,
H.startLONG AS Start_longitude,
H.finishLAT AS End_latitude,
H.finishLONG AS End_longitude,
(H.min_depth + H.max_depth)/2  AS Depth_m,
H.depth_unit AS Depth_unit,
C.species AS Species_code,
S.species_scientific_nme AS Scientific_name,
S.species_common_nme AS Common_name,
S.grouping AS Species_group,
C.weight AS Catch_weight,
C.num_caught AS Catch_count
FROM Shellfish_Bio_Shrimp_Prawn.dbo.PrawnHeaders H
    INNER JOIN 
    Shellfish_Bio_Shrimp_Prawn.dbo.PrawnTotCatch C ON H.[Key] = C.H_key
    INNER JOIN
    Shellfish_Bio_Shrimp_Prawn.dbo.Specieshrt S ON C.species = S.species_cde
WHERE H.startLAT IS NOT NULL 
    AND H.startLONG IS NOT NULL 
    AND (S.grouping = 'I')
ORDER BY H.year, H.[Key], H.set_num;
