SELECT
H.[Key] AS HKey,
H.Source AS Survey,
H.set_num AS Set_ID,
G.Description AS Trawl_type,
H.start_year AS Year,
H.start_mon AS Month,
H.start_day AS Day,
H.stat_area AS Major_stat_area_code,
H.sub_area AS Stat_subarea_code,
H.startLAT AS Start_latitude,
H.startLONG AS Start_longitude,
H.finishLAT AS End_latitude,
H.finishLONG AS End_longitude,
(H.start_depth + H.finish_depth)/2  AS Depth_m,
H.distance AS Distance_nm,
C.species AS Species_code,
S.species_scientific_nme AS Scientific_name,
S.species_common_nme AS Common_name,
S.grouping AS Species_group,
C.weight AS Catch_weight,
C.num_caught AS Catch_count
FROM Shellfish_Bio_Shrimp_Prawn.dbo.ShrimpHeaders H
    INNER JOIN 
    Shellfish_Bio_Shrimp_Prawn.dbo.ShrimpCatch C ON H.[Key] = C.H_key
    INNER JOIN
    Shellfish_Bio_Shrimp_Prawn.dbo.ShrimpluGearCodes G ON H.gear_code = G.gearCode
    INNER JOIN
    Shellfish_Bio_Shrimp_Prawn.dbo.Specieshrt S ON C.species = S.species_cde
WHERE H.startLAT IS NOT NULL 
    AND H.startLONG IS NOT NULL 
    AND H.Source='R'
    AND (S.grouping = 'I' OR S.grouping = 'F')
    AND (H.cond_code = 0 OR H.cond_code = 11 OR H.cond_code = 12 OR H.cond_code = 13 OR H.cond_code = 14)
ORDER BY H.start_year, H.[Key], H.set_num;
