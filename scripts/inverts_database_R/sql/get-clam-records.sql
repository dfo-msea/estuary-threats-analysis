SELECT
H.[Key] AS HKey,
H.Source AS Source_type,
H.Year AS Year,
H.MonthEnd AS Month,
H.DayEnd AS Day,
H.StatArea AS Major_stat_area_code,
H.SubArea AS Stat_subarea_code,
H.BeachCode AS BeachCode,
B.Latitude AS Latitude,
B.Longitude AS Longitude,
I.H2_Key AS H2_Key,
C.species AS Species_code,
S.species_scientific_nme AS Scientific_name,
S.species_common_nme AS Common_name,
S.grouping AS Species_group,
(C.WgtLegals + C.WgtSubLegals) AS Catch_weight,
(C.NumLegals + C.NumSubLegals) AS Catch_count
FROM Shellfish_Bio_Other.dbo.ClamHeaderSurvey H
    INNER JOIN
    Shellfish_Bio_Other.dbo.ClamBeaches B ON H.BeachCode=B.BeachCode
    INNER JOIN
    Shellfish_Bio_Other.dbo.ClamHeaderSamples I ON H.[Key]=I.H1_Key
    INNER JOIN 
    Shellfish_Bio_Other.dbo.ClamNumWgts C ON I.H2_Key = C.H2_key
    INNER JOIN
    Shellfish_Bio_Shrimp_Prawn.dbo.Specieshrt S ON C.species = S.species_cde
WHERE H.BeachCode IS NOT NULL 
    AND (S.grouping = 'I')
ORDER BY H.year, H.[Key];
