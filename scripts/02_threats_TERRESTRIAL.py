# John Cristiani john.cristiani@dfo-mpo.gc.ca
# October 2022

# Objective:
# Process threat data for overlay analysis with estuaries.
# Each feature requires different cleanup, so unfortunately it is a bit hard to standardize this
# into common functions. Therefore, its a bit verbose.


import arcpy
import os
import pandas as pd
import numpy as np
import copy

arcpy.CheckOutExtension("Spatial")
from arcpy.sa import *




#########################################
# Data

# IN
root = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\spatial'
# threat data - original
threats_dir = os.path.join(root, '01_original/threat_data')

# OUT
# In general, 01scratch is a staging area of the data with any initial preprocessing required.
# 02estuaries gdb is with the data associated to the estuaries.
gdb_clean = os.path.join(root, '02_working/threats_01scratch.gdb')
gdb_out = os.path.join(root, '02_working/threats_02estuaries.gdb')


# estuaries
estuaries = os.path.join(root, '02_working/estuaries_watersheds.gdb/estuaries_01_ALL')
# watersheds
wat = os.path.join(root, '02_working/estuaries_watersheds.gdb/watersheds_05_clip')
# combined estuaries and watersheds
est_wat = os.path.join(root, '02_working/estuaries_watersheds.gdb/EST_WTR_02_dissolve')
# shorline buffered areas
# NOTE: we changed the shoreline zone in script 1. It only includes the shore area (plus any 
# slivers between shore and estuary). However, we are not using it to clip in this script, we just
# need to for the area. There were almost no terrestrial features that overlapped the estuaries, so
# it is not really necessary.
shorebuff = os.path.join(root, '02_working/estuaries_watersheds.gdb/shoreline_03_integrate')
# land
land = os.path.join(root, r'02_working/estuaries_watersheds.gdb/land_erase')

arcpy.env.workspace = gdb_clean
arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(3005)



#########################################

# Intersect with watersheds and calculate totals
def intersectCalcTotal(feat, intFeat, field_name, outname):
    arcpy.PairwiseIntersect_analysis([feat, intFeat], 'temp_intersect')
    arcpy.AddField_management('temp_intersect', 'SUM', 'DOUBLE')
    arcpy.CalculateField_management('temp_intersect', 'SUM', '!{}!'.format(field_name))
    # dissolve and add
    arcpy.PairwiseDissolve_analysis('temp_intersect',outname,'EST_NO', [['SUM', 'SUM']])
    arcpy.Delete_management('temp_intersect')

# Output to table and change to consistent field name
def toTable(outname, field):
    arcpy.TableToTable_conversion(outname, gdb_out, outname)
    arcpy.AlterField_management(os.path.join(gdb_out, outname), field, 'SUM', 'SUM')




#########################################
#
# Terrestrial threats
#
#########################################


#########################################
#########################################
# Watershed Population

outname = 'thrt_ter_population'

pop = os.path.join(threats_dir, 'gpw-v4-population-count-rev11_2020_30_sec_tif/gpw_v4_population_count_rev11_2020_30_sec.tif')

# create a poly from the land extent to use for clipping the raster to make processing quicker
arcpy.MinimumBoundingGeometry_management(est_wat, 'temp_mbr', 'RECTANGLE_BY_AREA', 'ALL')

# clip raster to poly
arcpy.env.outputCoordinateSystem = pop
outRast = ExtractByMask(pop, 'temp_mbr')
outRast.save('population_01_clip')

# We want to adjust cell values based on area of overlap, so we need to convert to polys.
# You can't just do raster to poly because it will combine similar values.
# https://support.esri.com/en/technical-article/000012696
# Open the tool in ArcPro, put in the clipped raster as the default to get the values.
arcpy.RasterToPoint_conversion('population_01_clip', 'population_02_pts')
arcpy.CreateFishnet_management(
    'population_03_fishnet',
    '-139.308333 46.800000',
    '-139.308333 62.800000',
    None, None,
    1920, 3116,
    '-113.341667 62.800000',
    'LABELS',
    None,
    'POLYGON')
arcpy.FeatureToPolygon_management(
    'population_03_fishnet',
    'population_04_values',
    label_features='population_02_pts')

arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(3005)

arcpy.MakeFeatureLayer_management('population_04_values', 'temp_sel', """grid_code > 0""")
arcpy.CopyFeatures_management('temp_sel', 'population_05_values_sel')

# Clip to coastline
# This is important to do before I do an intersect and recalculate population
# based on the new area. For cells that overlap the coastline and have a lot of
# water in them, if I readjust based on this new area the I am reducing the pop
# by too much. Think of it as a big 1km cell that is over a very small island
# that has just a few people. If I clip to that island and then reduce the pop
# by that new area then I will be getting a very small fraction of people present.
arcpy.PairwiseClip_analysis('population_05_values_sel', land, 'population_06_clip')
arcpy.MultipartToSinglepart_management('population_06_clip', 'population_07_multising')

# project so that I can get areas in meters
arcpy.Project_management('population_07_multising', 'population_08_project', 3005)
# add area field so that this carries over to the next step
arcpy.AddField_management('population_08_project', 'area_orig', 'DOUBLE')
with arcpy.da.UpdateCursor('population_08_project', ['Shape_Area', 'area_orig']) as cursor:
    for row in cursor:
        row[1] = row[0]
        cursor.updateRow(row)

# intersect with watersheds
arcpy.PairwiseIntersect_analysis(['population_08_project', est_wat], 'population_09_intersect')

# calc population per piece
arcpy.AddField_management('population_09_intersect', 'pop_adjusted', 'DOUBLE')
with arcpy.da.UpdateCursor('population_09_intersect', ['grid_code', 'area_orig', 'Shape_Area', 'pop_adjusted']) as cursor:
    for row in cursor:
        row[3] = row[0] * (row[2]/row[1])
        cursor.updateRow(row)

# dissolve by EST_NO and add pop_adjusted
arcpy.PairwiseDissolve_analysis(
    'population_09_intersect', 
    'population_10_dissolve',
    'EST_NO',
    [['pop_adjusted', 'SUM']]
    )

# Not all watersheds have pop polys, so join back and calculate area as zero.
arcpy.MakeFeatureLayer_management(est_wat, 'temp_estwat')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_estwat', 'EST_NO', 'population_10_dissolve', 'EST_NO')
arcpy.CopyFeatures_management('temp_estwat', outname)
with arcpy.da.UpdateCursor(outname, ['SUM_pop_adjusted']) as cursor:
    for row in cursor:
        if row[0] is None:
            row[0] = 0
        cursor.updateRow(row)

# TableToTable
# AlterField
arcpy.TableToTable_conversion(outname, gdb_out, outname)
arcpy.AlterField_management(os.path.join(gdb_out, outname), 'SUM_pop_adjusted', 'SUM', 'SUM')

# clean up
arcpy.Delete_management('population_01_clip')
fcs = arcpy.ListFeatureClasses('temp_*')
for fc in fcs:
    arcpy.Delete_management(fc)
fcs = arcpy.ListFeatureClasses('population_*')
for fc in fcs:
    arcpy.Delete_management(fc) 



#########################################
#########################################
# Agriculture

# https://www.arcgis.com/home/item.html?id=cfcb7609de5f478eb7666240902d4d3d
# for working with the landuse dataset from Esri, it is an image service and you are limited
# to accessing 5000 rows and columns at a time with the Make Image Service Layer tool. Even when I
# access smaller portions, copying this layer to a raster takes forever, as does creating a mosaic.
# I then found that I can download images directly from here:
# https://www.arcgis.com/apps/instant/media/index.html?appid=fc92d38533d440078f17678ebc20e8e2
# however, creating a mosaic takes forever and I didn't wait for it to finish. At this point, I will
# just loop through each raster and pull and convert the relevant data.

outname = 'thrt_ter_agriculture'

indir = os.path.join(threats_dir, 'Sentinel_Land_Use_Esri')
arcpy.env.workspace = indir
rasters = arcpy.ListRasters()
arcpy.env.workspace = gdb_clean

# this works but the Con on the big raster is a bit slow
for rast in rasters:

    print(rast)
    raster = os.path.join(indir, rast)

    # create raster with only agriculture, all other cells null
    inRas = Raster(raster)
    outRas = Con(inRas, inRas, None, 'Value = 5')
    outRas.save('temp_cropland')

    # raster to polygon
    arcpy.RasterToPolygon_conversion('temp_cropland', rast.split('-')[0], 'NO_SIMPLIFY')
    arcpy.Delete_management('temp_cropland')
    del inRas, outRas


# merge
listfc = arcpy.ListFeatureClasses('*_20210101')
arcpy.Merge_management(listfc, 'temp_merge')

intersectCalcTotal('temp_merge', wat, 'Shape_Area', outname)
toTable(outname, 'SUM_SUM')

arcpy.Delete_management('temp_merge')
for rast in listfc:
    arcpy.Delete_management(rast)

# I don't think a shoreline zone makes sense for this feature



#########################################
#########################################
# Forestry cutblocks

outname = 'thrt_ter_forestrycutblocks'

# select cutblocks from the last 15 years (this was suggested by Nick)
# Free-to-grow status is usually reached in 11-20 years
cutblocks = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/Consolidated_Cutblocks_BL_polygons')
where = 'HARVESTYR >= 2013'
cb_sel = arcpy.SelectLayerByAttribute_management(cutblocks, where_clause=where)
arcpy.CopyFeatures_management(cb_sel, 'temp_sel')

intersectCalcTotal('temp_sel', wat, 'Shape_Area', outname)
toTable(outname, 'SUM_SUM')

arcpy.Delete_management('temp_sel')
del cb_sel



#########################################
#########################################
# Burned areas

outname = 'thrt_ter_burnedareas'

# select areas from the last 10 years
current = os.path.join(threats_dir, r'fire/PROT_CURRENT_FIRE_POLYS_SP.gdb/WHSE_LAND_AND_NATURAL_RESOURCE_PROT_CURRENT_FIRE_POLYS_SP')
historical = os.path.join(threats_dir, r'fire/PROT_HISTORICAL_FIRE_POLYS_SP.gdb/WHSE_LAND_AND_NATURAL_RESOURCE_PROT_HISTORICAL_FIRE_POLYS_SP')
where = 'FIRE_YEAR >= 2013'
fsel = arcpy.SelectLayerByAttribute_management(historical, where_clause=where)
arcpy.Merge_management([current, fsel], 'temp_fire')

intersectCalcTotal('temp_fire', wat, 'Shape_Area', outname)
toTable(outname, 'SUM_SUM')

arcpy.Delete_management('temp_fire')
del fsel



#########################################
#########################################
# Dams
# (now splitting these out from the other freshwater obstrucctions)

outname = 'thrt_ter_dams'

fo = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/FWA_OBSTRUCTIONS_SP')
where = """OBSTRUCTION_TYPE IN ('Dam')"""
fo_sel = arcpy.SelectLayerByAttribute_management(fo, where_clause=where)
arcpy.CopyFeatures_management(fo_sel, 'temp_sel')

arcpy.CalculateField_management('temp_sel', 'COUNT', 1)
intersectCalcTotal('temp_sel', wat, 'COUNT', outname)
toTable(outname, 'SUM_SUM')

arcpy.Delete_management('temp_sel')
del fo_sel



#########################################
#########################################
# Freshwater obstructions

outname = 'thrt_ter_freshwatobstructions'

# PATH dataset (Program Activity Tracking for Habitat system)
# Path data - last 10 years:
# Most of the records are for small scale construction, repair or maintenance events. It's not to
# note the presence of a feature, but instead to note an event. If we wanted to use the data to 
# capture for things like culverts, it would then be a wildly incomplete dataset since its not about
# documenting where all the culverts are.
# For events, we could possibly incorporate these into shoreline and general development since we
# just dissolve the footprint anyways, but if the repair event only produces a stressor for a short 
# period of time then this may not be relevant.
# We will use the dataset only for dredging and freshwater obstruction points.
# Go through individual freshwater obstructions by work and impact. There are a number of different
# categories, but I'm looking for cases where flow would actually be altered (e.g. watercourse 
# realignment). Compare to existing dataset to avoid duplicates. 
path_csv = os.path.join(threats_dir, 'PATH/2023-01-23_CSAS_Referrals_V2.xlsx')
arcpy.conversion.ExcelToTable(path_csv, 'PATH_01_tbl')
cs = arcpy.SpatialReference(4326)
arcpy.XYTableToPoint_management('path_01_tbl', 'path_02_pt', 'Decimal_Longitude', 'Decimal_Latitude','', cs)
arcpy.PairwiseClip_analysis('path_02_pt', wat, 'path_03_terrestrial')

# Selecting:
# I think just a def query on Primary Impact should work
# It's difficult to say which ones should and shouldn't be inlcuded, for instance, if it is just
# culvert replacement, is that a negative impact?
# However, keep in mind that there is a "no impact" option, so by having something listed, that
# means there was an impact.
# It can also be useful for identifying streams that have human intervention, e.g. wee aren't 
# getting ALL culverts that exist, but we are at least getting ones that were significant enough to 
# make it into this database.
where = """Primary_Impact IN ('Changes in Flows/Water Levels', 'Fish Passage', 'Watercourse Alteration')"""
arcpy.MakeFeatureLayer_management('path_03_terrestrial', 'temp_lyr', where)
arcpy.CopyFeatures_management('temp_lyr', 'path_04_sel')
arcpy.Delete_management('temp_lyr')

# PSCIS Habitat Confirmations dataset. The Provincial Stream Crossing Information System (PSCIS) is
# the provinicial repository for all culvert fish passage data.
# https://catalogue.data.gov.bc.ca/dataset/pscis-habitat-confirmations
# This is a new edition post-CSAS meeting in an attempt to include more culvert/modification data.
# This dataset is the 2nd of 4 datasets where road stream crossings were evaluated for having
# negative effects to fish habitat and water flow. The 2nd dataset is the one that notes the details.
# We are including it because it is similar to PATH in that it notes effects to fish. I don't think
# its possible to include ALL culverts. None of the other datasets seem complete.

# There is also a major culverts dataset, but it only has culverts >3m, it seems to only follow the
# major road network, and it makes no note about effects to fish habitat.

# I've done a spatial select with the PATH dataset and there does not seem to be any overlap
# (closest points are 400m away).

pscis = os.path.join(threats_dir, 'PSCIS_HABITAT_CONFIRMATION_SVW.gdb/WHSE_FISH_PSCIS_HABITAT_CONFIRMATION_SVW')

# There does not appear to be any overlap between the two datasets.
# Merge
arcpy.Merge_management([pscis, 'path_04_sel'], 'temp_fo_merge')

arcpy.CalculateField_management('temp_fo_merge', 'COUNT', 1)
intersectCalcTotal('temp_fo_merge', wat, 'COUNT', outname)
toTable(outname, 'SUM_SUM')

arcpy.Delete_management(['path_01_tbl', 'path_02_pt', 'path_03_terrestrial', 'path_04_sel', 'temp_fo_merge'])



#########################################
#########################################
# Debris/litter

outname = 'thrt_ter_debrislitter'

dl = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/debris_ratings_5km')

# the line does not trace the coastline exactly and is quite distant in some areas
# Spatial join with a 1km search distance
arcpy.SpatialJoin_analysis(
    estuaries,
    dl,
    'temp_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_ALL',
    match_option='INTERSECT',
    search_radius=1000
)
arcpy.Dissolve_management('temp_sjoin', outname, 'EST_NO', [['DEBRIS_RAT', 'MEAN']])
toTable(outname, 'MEAN_DEBRIS_RAT')

arcpy.Delete_management('temp_sjoin')



#########################################
#########################################
# Wastewater outflow

# Selina and Craig added compound values, splitting them into organic and inorganic compounds.
# Do for the high estimates. The _0 ones are low estimates.

# It's hard to tell what is a outflow pipe and what is another kind of structure (I've tried
# different kinds of selections), and also how accurate the location is. For example the pipe near 
# Ansel point shows it on land, but I know from diving that the outflow in the water 20m deep.

# Therefore, get points that overlap the est-wat AND shoreline zone, and also what is within 2km of 
# just the estuary. This will account for some spread in the ocean and for the uncertainty in some
# locations.

outname_i = 'thrt_ter_wstwtroutflowinorganic'
outname_o = 'thrt_ter_wstwtroutfloworganic'

ww = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/PAWPIT_Data')
pawpit_glossary = os.path.join(threats_dir, 'PAWPIT_glossary.csv')
# The original one with additional tables from Craig and Selina is here:
# ...Estuary_threats\CCM_Selina_Craig

pg = pd.read_csv(pawpit_glossary)
pgi = pg[pg.Organic_Inorganic == 'I'].Acronym.to_list()
pgo = pg[pg.Organic_Inorganic == 'O'].Acronym.to_list()

# get fields to add
# fields = arcpy.ListFields(ww)
# compounds = []
# write = 0
# for field in fields:
#     if write == 0:
#         if field.name == 'Ammonia':
#             compounds.append(field.name)
#             write = 1
#             continue
#     if write == 1:
#         compounds.append(field.name)
#         if field.name == 'TL':
#             write = 0

# First, get the ones that overlap the estuary-watersheds.
arcpy.SpatialJoin_analysis(
    ww,
    est_wat,
    'temp_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='INTERSECT'
)

# However, there are many points on a shoreline or in the water directly adjacent to an estuary that
# would very likely have an effect but they don't overlap the shoreline. I don't want to buffer ALL 
# points and select by overlap with all watersheds because if there is one further inland that is in
# a different watershed, I don't want it included, even if it is close. 
# Therefore, select those point that are within 2000 METERS of JUST the ESTUARIES.
# CCM did 5000m impact distance. Since we are not doing a distance decay 2000m should be an
# appropriate conservative effect distance.
arcpy.SpatialJoin_analysis(
    ww,
    estuaries,
    'temp_sjoin_buff',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option='WITHIN_A_DISTANCE',
    search_radius=2000
)

arcpy.Merge_management(['temp_sjoin', 'temp_sjoin_buff'], 'temp_merge')
arcpy.CopyFeatures_management('temp_merge', 'temp_delIdentical')
arcpy.DeleteIdentical_management('temp_delIdentical', ['PointID', 'EST_NO'])
#arcpy.CopyFeatures_management('temp_delIdentical', outname)

# add fields
arcpy.AddField_management('temp_delIdentical', 'organic_TOTAL', 'DOUBLE')
arcpy.AddField_management('temp_delIdentical', 'inorganic_TOTAL', 'DOUBLE')
fields_add_i = copy.deepcopy(pgi)
fields_add_o = copy.deepcopy(pgo)
fields_add_o.append('organic_TOTAL')
fields_add_i.append('inorganic_TOTAL')

with arcpy.da.UpdateCursor('temp_delIdentical', fields_add_o) as cursor:
    for row in cursor:
        row[-1] = sum(row[:-1])
        cursor.updateRow(row)
with arcpy.da.UpdateCursor('temp_delIdentical', fields_add_i) as cursor:
    for row in cursor:
        row[-1] = sum(row[:-1])
        cursor.updateRow(row)

# dissolve
arcpy.Dissolve_management('temp_delIdentical', outname_i, 'EST_NO', [['inorganic_TOTAL', 'SUM']])
arcpy.Dissolve_management('temp_delIdentical', outname_o, 'EST_NO', [['organic_TOTAL', 'SUM']])

toTable(outname_i, 'SUM_inorganic_TOTAL')
toTable(outname_o, 'SUM_organic_TOTAL')

fcs = arcpy.ListFeatureClasses('temp_*')
for fc in fcs:
    arcpy.Delete_management(fc)



#########################################
#########################################
# Mining

# New approach post CSAS meeting:
# We are no longer quantifying footprints of the tenures and permitted mine polygons. Instead we
# will use the MINFILE points and tonnes mined to match what Cathryn Murray does.

outname = 'thrt_ter_mining'

minfile = os.path.join(threats_dir, 'mining/MINFIL_PRODUCT.gdb/WHSE_MINERAL_TENURE_MINFIL_PRODUCT')
intersectCalcTotal(minfile, wat, 'MINED_TONNES', outname)
toTable(outname, 'SUM_SUM')


# NOTE: this may dataset may change further once we hear from Kathleen Moore. They digitized
# footprints. Even if do get those, they can just go in the shoreline and then general development
# datasets, but not the general industry datasets.



#########################################
#########################################
# Built up land

# this dataset is part of shoreline and watershed development datasets

# It is not complete enough to replace other datasets e.g., it doesn't include all roads, even major
# ones, and its not great for small footprints like buildings surrounded by wilderness. It only
# seems good for continuous built up areas.

indir = os.path.join(threats_dir, 'Sentinel_Land_Use_Esri')
arcpy.env.workspace = indir
rasters = arcpy.ListRasters()
arcpy.CreateFileGDB_management(indir, 'builtupland.gdb')
arcpy.env.workspace = os.path.join(indir, 'builtupland.gdb')

# this works but the Con on the big raster is a bit slow
for rast in rasters:

    print(rast)
    raster = os.path.join(indir, rast)

    # create raster with only agriculture, all other cells null
    inRas = Raster(raster)
    outRas = Con(inRas, inRas, None, 'Value = 7')
    outRas.save('temp_builtarea')

    # raster to polygon
    arcpy.RasterToPolygon_conversion('temp_builtarea', rast.split('-')[0], 'NO_SIMPLIFY')
    arcpy.Delete_management('temp_builtarea')
    del inRas, outRas

listfc = arcpy.ListFeatureClasses('*_20210101')
arcpy.Merge_management(listfc, 'builtupland_merge')
arcpy.env.workspace = gdb_clean



#########################################
#########################################
# Road and Rail
# Preprocessing to include in the next datasets

# I'm including all road and rail together because 
# (1) the stressors are the same: sediment input and pollution. There would of course be different 
# levels for each, but we won't make that distinction on first pass, and
# (2) there are some duplicates between the two datasets, and it is just easier to do it this way.

# In the end, this is just going to be a part of the general development datasets anyways.

# Post CSAS meeting updates:
# For roads we will use the full Digital Roads Atlas dataset. The dataset Bea had in the gdb seemed
# to be a subset. The full version includes for of the forestry roads as well as alleys.
# However, the forestry roads dataset appears to include newer roads that the DRA does not have, so
# I will still include them.

# How to deal with overlap between DRA and forestry roads datasets:
# Since I am buffering and dissolve polygons this should take care of most overlap, however, there
# are some areas where the lines are perfectly coincident. This will result in overcounting area,
# but I will make the assumption that this is evenly distributed among watersheds.

roads = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/TRANSPORT_LINE')
rail = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/NRWN_BC_2_0_TRACK')
roads_forestry = os.path.join(threats_dir, 'Estuary_CSAS_Threat_Data.gdb/FTN_RD_SGM_line')

# filter out ferry, water routes, trails, and overgrown roads
where = """TRANSPORT_LINE_TYPE_CODE NOT IN ('T', 'TD', 'TR', 'F', 'RWA') AND TRANSPORT_LINE_SURFACE_CODE NOT IN ('B', 'O')"""
arcpy.MakeFeatureLayer_management(roads, 'temp_roads_sel', where_clause=where)
where = """TRACKCLASS NOT IN ('Ferry Route')"""
arcpy.MakeFeatureLayer_management(rail, 'temp_rail_sel', where_clause=where)

# large datasets, so first clip datasets to est-sho-wat
arcpy.Merge_management([est_wat, shorebuff], 'temp_estwatsho')
arcpy.Dissolve_management('temp_estwatsho', 'temp_estwatshodiss')
arcpy.PairwiseClip_analysis(roads_forestry, 'temp_estwatshodiss', 'temp_roadsforestry_clip')
arcpy.PairwiseClip_analysis('temp_roads_sel', 'temp_estwatshodiss', 'temp_roads_clip')
arcpy.PairwiseClip_analysis('temp_rail_sel', 'temp_estwatshodiss', 'temp_rail_clip')

# New field of lane count * 2.5m estimated lane width
arcpy.AddField_management('temp_roads_clip', 'buffer_m', 'FLOAT')
with arcpy.da.UpdateCursor('temp_roads_clip', ['TOTAL_NUMBER_OF_LANES', 'buffer_m']) as cursor:
    for row in cursor:
        row[1] = row[0] * 2.5
        cursor.updateRow(row)

# Merge all line data and deal with fields
# There are A LOT of fields, but I only need one of them. For a dataset this size it was actually
# extremely slow to do DeleteField for the ones I don't need. Instead, I will try with fieldmappings
# on the merge.

# Create the required FieldMap and FieldMappings objects
fm_buffm = arcpy.FieldMap()
fms = arcpy.FieldMappings()
# Add fields to their corresponding FieldMap objects
fm_buffm.addInputField('temp_roads_clip', 'buffer_m')
# Set the output field properties for the FieldMap object
buffm_name = fm_buffm.outputField
buffm_name.name = 'buffer_m'
fm_buffm.outputField = buffm_name
# Add the FieldMap object to the FieldMappings object
fms.addFieldMap(fm_buffm)

arcpy.Merge_management(['temp_roads_clip', 'temp_roadsforestry_clip', 'temp_rail_clip'], 'temp_roads_merge_line', fms)
# yes, way fasting with field mappings

with arcpy.da.UpdateCursor('temp_roads_merge_line', ['buffer_m']) as cursor:
    for row in cursor:
        if row[0] is None:
            row[0] = 2.5
        cursor.updateRow(row)

# do initial dissolve by buffer width to hopefully speed up processing once I combine datasets
arcpy.PairwiseDissolve_analysis('temp_roads_merge_line', 'temp_roads_diss_line', 'buffer_m')


# Buffering and Dissolving
# Dissolving with this many features keeps failing. Apparently there is a limit on the number of
# I will need to break it up and dissolve each piece and then merge back together and dissolve again.

# mbg
# split into pieces
# create new gdb
# split by polygon into different fcs
# buffer and dissolve each one
# merge dissolved features
# dissolve again
# copy back
# delete gdb
arcpy.MinimumBoundingGeometry_management('temp_roads_diss_line', 'temp_roadsmbg', 'CONVEX_HULL', 'ALL')
arcpy.SubdividePolygon_management('temp_roadsmbg', 'temp_roadsmbg_split', 'NUMBER_OF_EQUAL_PARTS', 10, subdivision_type='STACKED_BLOCKS')
arcpy.AddField_management('temp_roadsmbg_split', 'name', 'TEXT')
# have to have a field with a character
with arcpy.da.UpdateCursor('temp_roadsmbg_split', ['OBJECTID', 'name']) as cursor:
    for row in cursor:
        row[1] = 'split_' + str(row[0])
        cursor.updateRow(row)
arcpy.Split_analysis('temp_roads_diss_line', 'temp_roadsmbg_split', 'name', gdb_clean) # This step takes a while
fcs = arcpy.ListFeatureClasses('split_*')
for fc in fcs:
    print('Processing '+fc)
    arcpy.PairwiseBuffer_analysis(fc, fc+'_splitbuff', 'buffer_m') # with pairwise buffer you can't do line end type "FLAT"
    arcpy.PairwiseDissolve_analysis(fc+'_splitbuff', fc+'_splitdiss')
fcs = arcpy.ListFeatureClasses('*_splitdiss')
arcpy.Merge_management(fcs, 'temp_roadssplitmerge')
# arcpy.PairwiseDissolve_analysis('temp_roadssplitmerge', 'temp_roadsmulti')
# Dissolve here fails, most likely a memory issue. However, the dissolve is only for the road 
# segements that overlap on the borders of the 10 subdivided polygons. This is not a lot of area and
# it just something that I will have to live with. You can do end type "FLAT" with PairwiseBuffer.
#arcpy.MultipartToSinglepart_management('temp_roadsmulti', 'temp_roads')
arcpy.Rename_management('temp_roadssplitmerge', 'roads_DONOTDELETE')

fcs = arcpy.ListFeatureClasses('split_*')
arcpy.Delete_management(fcs)

arcpy.Delete_management(['temp_roads_sel', 'temp_rail_sel', 'temp_roadsforestry_clip', 
'temp_roads_clip', 'temp_roadsforestry_clip', 'temp_rail_clip', 'temp_roads_merge_line', 
'temp_roads_diss_line', 'temp_estwatsho', 'temp_estwatshodiss', 'temp_roadsmbg', 'temp_roadsmbg_split'])

# OLD APPROACH when I was keeping things as lines:
# Some roads are duplicated between datasets. Dissolve doesn't work, even with a high xy tolerance.
# Use Intergrate tool. This isn't perfect, but it takes care of most overlaps.
# arcpy.CopyFeatures_management('temp_clip', 'temp_integrate')
# arcpy.PairwiseIntegrate_analysis('temp_integrate', "5 meters")



#########################################
#########################################
# Shoreline and watershed general datasets:

# We need to figure out how to group activities that have overlapping stressors and may not need to
# be their own dataset.

# Generally, I am doing a watershed and an estuary shoreline zone to distinguish between pollution/
# sedimentation in the whole watershed and habitat modification for activities directly adjacent to
# or overlapping the estuary. The watershed area can overlap the buffered estuary zone and not be
# double counting because I consider the stressors to be different (e.g. a mill generating pollution
# vs. the sedimentation/modifciation of that mill footprint). However, I do something different with
# the 3rd dataset.

# I will do 3 datasets:
# (1) Shoreline-estuary general development
# (2) General industry in the watershed
# (3) General built up areas in the watershed

# (1) Shoreline-estuary general development:
# Built-up land raster, dikes, buffered roads, shorezone, tenure data, industry monitoring point
# dataset, mine footprints(?), pipelines.
# Every is merged and dissolved, so it is ok if things initially overlap.

# (2) General industry in the watershed:
# This is separate from the general built up development dataset because I consider industry to have
# different pollution types, even we don't specify these exactly. However, I will erase the next
# dataset with this one, so there won't be double counting.
# Will include: tenures, industry points, timber processing, pipelines (maybe mine footprints).

# (3) General built up areas in the watershed:
# Built up land raster AND buffered roads with the previous 2 datasets erased from it.
# BUT in THIS case, I am NOT including the shoreline zone because this stressor would be already
# included there.



#########################################
#########################################
# Shoreline developement

outname = 'thrt_sho_shorelinedevelopment'

### Dikes
di = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/Dikes')
arcpy.Buffer_analysis(di, 'temp_dikes', 10)

### Timber processing
# Most of the large mills will be accounted for in the built up area dataset, but many of the small
# ones do not seem to be there, hence the small buffer distance.
timb = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/BC_Major_Timber_Processing_Facilities')
arcpy.Buffer_analysis(timb, 'temp_buff', 100)
arcpy.Erase_analysis('temp_buff', estuaries, 'temp_timbprocess')
arcpy.Delete_management('temp_buff')

### Built up land areas
buland = os.path.join(threats_dir, 'Sentinel_Land_Use_Esri/builtupland.gdb/builtupland_merge')

### Mining
# Mining footprints - use all except 'not_started' - we will assume closed ones still require
# reclamation.
# These are just the footprints. We will still use these to capture any development that is not 
# picked up by the built-up land raster. For instance, it classifies some mines as "bare ground" and
# tailings ponds as "water".
# ACTUALLY, none of these intersect with the shoreline zones, so lets not even include them. Avoid
# any criticism.
# mine_ten = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/TA_CRT_SVW_polygon')
# mine_ftp = os.path.join(threats_dir, 'mining/HSP_MJR_MINES_PERMTTD_AREAS_SP.gdb/WHSE_MINERAL_TENURE_HSP_MJR_MINES_PERMTTD_AREAS_SP')
# where = """TEN_PURPOS = 'INDUSTRIAL' AND TEN_SUBPRP = 'MINERAL PRODUCTION'"""
# ten_sel = arcpy.SelectLayerByAttribute_management(mine_ten, where_clause=where)
# arcpy.Merge_management([ten_sel, mine_ftp], 'temp_mining')

### Tenures (no log handling or mining)
tenures = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/TA_CRT_SVW_polygon')
where = """TEN_PURPOS = 'INDUSTRIAL' And TEN_SUBPRP NOT IN ('LOG HANDLING/STORAGE', 'MINERAL PRODUCTION')"""
arcpy.MakeFeatureLayer_management(tenures, 'temp_out', where_clause=where)
arcpy.CopyFeatures_management('temp_out', 'temp_tenures')
arcpy.Delete_management('temp_out')

### Industry monitoring point dataset
whse_pts = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/WHSE_ENVIRONMENTAL_MONITORING_CHRA_INDUSTRIES_POINT')
where = """DESCRIPTION NOT IN ('Log Booming', 'Log Booms', 'Log Dump/Sort', 'Log Dump')"""
sel = arcpy.SelectLayerByAttribute_management(whse_pts, where_clause=where)
arcpy.Buffer_analysis(sel, 'temp_industrycoastpts', 100, dissolve_option='ALL')

### Shorezone
# lines - join and select to get manmade features
# Buffer by 10 meters to make polygons - assume that this would be a band of "hardening" and that
# the other datasets can represent other features in the watershed
shorezone_old_ln = os.path.join(threats_dir, r'29-BC_ShoreZone_CoastwideHabitatMapping_June2021\BC_historical_ShoreZone_Mapping_17aug20.gdb\Unit_lines')
shorezone_old_tb = os.path.join(threats_dir, r'29-BC_ShoreZone_CoastwideHabitatMapping_June2021\BC_historical_ShoreZone_Mapping_17aug20.gdb\Unit')
shorezone_new_ln = os.path.join(threats_dir, r'29-BC_ShoreZone_CoastwideHabitatMapping_June2021\BC_ShoreZone_Mapping_08jun21.gdb\Unit_lines')
shorezone_new_tb = os.path.join(threats_dir, r'29-BC_ShoreZone_CoastwideHabitatMapping_June2021\BC_ShoreZone_Mapping_08jun21.gdb\Unit')

arcpy.MakeFeatureLayer_management(shorezone_old_ln, 'temp_szoldln')
arcpy.AddJoin_management('temp_szoldln', 'PHY_IDENT', shorezone_old_tb, 'PHY_IDENT')
where = """COASTAL_CLASS IN (32, 33)"""
sel = arcpy.SelectLayerByAttribute_management('temp_szoldln', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezoneold')

arcpy.MakeFeatureLayer_management(shorezone_new_ln, 'temp_sznewln')
arcpy.AddJoin_management('temp_sznewln', 'PHY_IDENT', shorezone_new_tb, 'PHY_IDENT')
where = """SHORETYPE IN (32, 33)"""
sel = arcpy.SelectLayerByAttribute_management('temp_sznewln', where_clause=where)
arcpy.CopyFeatures_management(sel, 'temp_shorezonenew')
arcpy.Delete_management(['temp_szoldln', 'temp_sznewln'])

arcpy.Merge_management(['temp_shorezoneold', 'temp_shorezonenew'], 'temp_shorezone_merge')
arcpy.Buffer_analysis('temp_shorezone_merge', 'temp_shorezone_buffer', 10, dissolve_option='ALL')
arcpy.MultipartToSinglepart_management('temp_shorezone_buffer', 'temp_shorezone')
arcpy.Delete_management(['temp_shorezone_merge', 'temp_shorezone_buffer', 'temp_shorezonenew', 'temp_shorezoneold'])

### Pipelines
# https://www.bcogc.ca/data-reports/data-centre/
prow = os.path.join(threats_dir, 'pipelines/Pipeline_Rights_of_Way_(Permitted).shp')
pseg = os.path.join(threats_dir, 'pipelines/Pipeline_Segments_(Permitted).shp')
# together these datasets are the pipeline as well as the footprint of the surrounding infrastructure
arcpy.Buffer_analysis(pseg, 'temp_buff', 2.5)
arcpy.Merge_management(['temp_buff', prow], 'temp_pipelines')
arcpy.Delete_management('temp_buff')

### Roads
roads_all = 'roads_DONOTDELETE'


# Merge and Dissolve and Clip
arcpy.Merge_management(
    ['temp_dikes', 'temp_timbprocess', buland, 'temp_tenures', 'temp_industrycoastpts', 'temp_shorezone', 'temp_pipelines', roads_all], 
    'temp_shoreline_merge')
arcpy.PairwiseClip_analysis('temp_shoreline_merge', shorebuff, 'temp_shoreline_clip')
arcpy.PairwiseDissolve_analysis('temp_shoreline_clip', 'temp_shoreline_diss', multi_part='SINGLE_PART')

intersectCalcTotal('temp_shoreline_diss', shorebuff, 'Shape_Area', outname)
toTable(outname, 'SUM_SUM')

arcpy.Delete_management(['temp_dikes', 'temp_buff', 'temp_timbprocess', 'temp_tenures', 'temp_industrycoastpts', 'temp_pipelines', 'temp_shorezone', 'temp_shoreline_merge', 'temp_shoreline_clip', 'temp_shoreline_diss'])




#########################################
#########################################
# General industry in the watershed

outname = 'thrt_ter_industrywatershed'

### Timber processing
# Most of the large mills will be accounted for in the built up area dataset, but many of the small
# ones do not seem to be there, hence the small buffer distance.
timb = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/BC_Major_Timber_Processing_Facilities')
arcpy.Buffer_analysis(timb, 'temp_buff', 100)
arcpy.Erase_analysis('temp_buff', estuaries, 'temp_timbprocess')
arcpy.Delete_management('temp_buff')

### Mining footprints
# Even though we have a separate mining dataset, we will still include footprints here to catpure
# land conversion. These areas (i.e. bare land, tailings ponds) are not captured in the built up
# land dataset, so we will include them here.
mine_ftp = os.path.join(threats_dir, 'mining/HSP_MJR_MINES_PERMTTD_AREAS_SP.gdb/WHSE_MINERAL_TENURE_HSP_MJR_MINES_PERMTTD_AREAS_SP')

### Tenures (no log handling or mining)
tenures = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/TA_CRT_SVW_polygon')
where = """TEN_PURPOS = 'INDUSTRIAL' And TEN_SUBPRP NOT IN ('LOG HANDLING/STORAGE', 'MINERAL PRODUCTION')"""
arcpy.MakeFeatureLayer_management(tenures, 'temp_out', where_clause=where)
arcpy.CopyFeatures_management('temp_out', 'temp_tenures')
arcpy.Delete_management('temp_out')

### Industry monitoring point dataset
whse_pts = os.path.join(threats_dir, r'Estuary_CSAS_Threat_Data.gdb/WHSE_ENVIRONMENTAL_MONITORING_CHRA_INDUSTRIES_POINT')
where = """DESCRIPTION NOT IN ('Log Booming', 'Log Booms', 'Log Dump/Sort', 'Log Dump')"""
sel = arcpy.SelectLayerByAttribute_management(whse_pts, where_clause=where)
arcpy.Buffer_analysis(sel, 'temp_industrycoastpts', 100, dissolve_option='ALL')

### Pipelines
# https://www.bcogc.ca/data-reports/data-centre/
prow = os.path.join(threats_dir, 'pipelines/Pipeline_Rights_of_Way_(Permitted).shp')
pseg = os.path.join(threats_dir, 'pipelines/Pipeline_Segments_(Permitted).shp')
# together these datasets are the pipeline as well as the footprint of the surrounding infrastructure
arcpy.Buffer_analysis(pseg, 'temp_buff', 2.5)
arcpy.Merge_management(['temp_buff', prow], 'temp_pipelines')
arcpy.Delete_management('temp_buff')

# Merge and Dissolve and Clip
arcpy.Merge_management(
    ['temp_timbprocess', mine_ftp, 'temp_tenures', 'temp_industrycoastpts', 'temp_pipelines'], 
    'temp_watershed_merge')
arcpy.PairwiseClip_analysis('temp_watershed_merge', wat, 'temp_watershed_clip')
arcpy.PairwiseDissolve_analysis('temp_watershed_clip', 'temp_watershed_diss', multi_part='SINGLE_PART')

intersectCalcTotal('temp_watershed_diss', wat, 'Shape_Area', outname)
toTable(outname, 'SUM_SUM')

arcpy.Delete_management(['temp_industrycoastpts', 'temp_mining', 'temp_pipelines', 'temp_tenures', 'temp_timbprocess'])
arcpy.Delete_management(['temp_watershed_merge', 'temp_watershed_clip', 'temp_watershed_diss'])



#########################################
#########################################
# General built up areas in the watershed

outname = 'thrt_ter_generaldevelopment'

### Built up land areas
buland = os.path.join(threats_dir, 'Sentinel_Land_Use_Esri/builtupland.gdb/builtupland_merge')

### Roads
roads_all = 'roads_DONOTDELETE'

# In this specific case, I want to intersect with the watersheds with the shoreline buffer zone
# erased.
# NOTE: there is one watershed that is very small (149), and the shorebuff erases all of the
# watershed polygon. This is ok.
arcpy.Erase_analysis(wat, shorebuff, 'temp_waterase')

arcpy.Merge_management([buland, roads_all], 'temp_watershed_merge')
arcpy.PairwiseClip_analysis('temp_watershed_merge', 'temp_waterase', 'temp_watershed_clip')
# this one takes a while, but it does work

# The intersect in the function takes a long time and consumes all the memory. I haven't been able
# to get it to successfully run all the way through even though it seems to partially process and
# save some of the data.
# So I will need to do a more manual approach and intersect by individual watershed:
arcpy.CreateFileGDB_management(os.path.join(root, '02_working'), 'temp.gdb')
gdb_temp = os.path.join(root, '02_working/temp.gdb')
with arcpy.da.SearchCursor('temp_waterase', ['EST_NO']) as cursor:
    for row in cursor:
        out = os.path.join(gdb_temp, 'temp_diss_{}'.format(str(int(row[0]))))
        if not arcpy.Exists(out):
            print(row[0])
            arcpy.MakeFeatureLayer_management('temp_waterase', 'temp_lyr', f"""EST_NO = {row[0]}""")
            out = os.path.join(gdb_temp, 'temp_diss_{}'.format(str(int(row[0]))))
            arcpy.PairwiseIntersect_analysis(['temp_watershed_clip', 'temp_lyr'], 'temp_int')
            arcpy.PairwiseDissolve_analysis('temp_int', out, 'EST_NO')
            arcpy.Delete_management(['temp_lyr', 'temp_int'])

arcpy.env.workspace = gdb_temp
fcs = arcpy.ListFeatureClasses()
arcpy.Merge_management(fcs, 'temp_merge')
arcpy.AddField_management('temp_merge', 'SUM', 'DOUBLE')
arcpy.CalculateField_management('temp_merge', 'SUM', '!{}!'.format('Shape_Area'))

arcpy.PairwiseDissolve_analysis('temp_merge', os.path.join(gdb_clean, outname) ,'EST_NO', [['SUM', 'SUM']])

arcpy.env.workspace = gdb_clean

toTable(outname, 'SUM_SUM')

arcpy.Delete_management(['temp_waterase', 'temp_watershed_merge', 'temp_watershed_clip'])
arcpy.Delete_management(gdb_temp)

