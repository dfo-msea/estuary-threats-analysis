# John Cristiani john.cristiani@dfo-mpo.gc.ca
# December 2022

# Objective:
# Associate estuaries with their primary watersheds.



import arcpy
import os
import pandas as pd
import numpy as np


#########################################
# Data

root = r'C:\Users\cristianij\Documents\Projects\Estuary_threats\spatial'

# Current estuary data: https://pacificbirds.org/2021/02/an-updated-ranking-of-british-columbias-estuaries/
# There is a point dataset here as well that shows which estuaries were removed from the previous version.
pecp_estuaries_ply = os.path.join(root, r'01_original\PECP_Estuary_Shapefiles_PUBLIC\PECP_estuary_polys_ranked_2019_public.shp')

# Freshwater Atlas fundamental watersheds gdb
watersheds_gdb = os.path.join(r'C:\Users\cristianij\Documents\DATA_BASE\FWA\FWA_WATERSHEDS_POLY.gdb')

# Freshwater Atlas streams gdb
streams_gdb = os.path.join(r'C:\Users\cristianij\Documents\DATA_BASE\FWA\FWA_STREAM_NETWORKS_SP.gdb')

# Land - based on FWA coastline
land = os.path.join(r'C:\Users\cristianij\Documents\DATA_BASE\BC_Boundary_Terrestrial.gdb\BC_Boundary_Terrestrial_Multipart') 

# working gdb
working_gdb = os.path.join(root, r'02_working\estuaries_watersheds.gdb')

arcpy.env.workspace = working_gdb



##########################################
# Estuaries

arcpy.CopyFeatures_management(pecp_estuaries_ply, "estuaries_01_ALL")
arcpy.DeleteField_management("estuaries_01_ALL", 
    ['CODE_LET', 'AREA_HA', 'NDI_CHART', 'PLAN_AREA', 'IMP_CL2019', 
    'NOTES_2019', 'Shape_Leng'])

# In the previous dataset, the unnamed ones were changed to be "Unnamed_{EST_NO}". Update names to match
with arcpy.da.UpdateCursor('estuaries_01_ALL', ['EST_NO', 'EST_NAME']) as cursor:
    for row in cursor:
        if row[1] == 'Unnamed':
            row[1] = f'Unnamed_{str(row[0])}'
            cursor.updateRow(row)



#########################################
# Prepare land dataset

# I need a land dataset for certain buffering, erasing, clipping operations.
# The land dataset is from databc and is based on the Freshwater Atlas.
# However, I've found that in some places like the Skeena estuary, this is covered by land, which
# creates issues for any threats I buffer and then erase by land.
# So first, erase land with the estuaries.

arcpy.Erase_analysis(land, 'estuaries_01_ALL', os.path.join(root, '02_working/estuaries_watersheds.gdb/land_erase'))



#########################################
# Watersheds

# Merge stream lines
arcpy.env.workspace = streams_gdb
fcs = arcpy.ListFeatureClasses()
arcpy.Merge_management(fcs, os.path.join(working_gdb, 'streams_01_mergeALL'))
arcpy.env.workspace = working_gdb

# Merge fundamental watershed polys
arcpy.env.workspace = watersheds_gdb
fcs = arcpy.ListFeatureClasses()
arcpy.Merge_management(fcs, os.path.join(working_gdb, 'watersheds_01_mergeALL'))
arcpy.env.workspace = working_gdb

# Spatial join of streams that intersect estuaries.
# Do this with a 50 m search radius. There are some slivers.
# There are some coastal watersheds without streams that intersect with estuaries, but I am NOT
# including these as part of the watershed. Portions of these will get included as part of the
# shoreline area though.
# Most details on this in notes below.
arcpy.env.qualifiedFieldNames = False
arcpy.SpatialJoin_analysis(
    'streams_01_mergeALL',
    'estuaries_01_ALL',
    'streams_02_sjoin',
    'JOIN_ONE_TO_MANY',
    'KEEP_COMMON',
    match_option = 'INTERSECT',
    search_radius = 50
)

# I did test if all estuaries have streams by doing another spatial join.

# output watersheds and selected stream datasets to pandas for quicker operation
field_names = [i.name for i in arcpy.ListFields('watersheds_01_mergeALL') if i.name in ['WATERSHED_FEATURE_ID', 'LOCAL_WATERSHED_CODE']] 
cursor = arcpy.da.SearchCursor('watersheds_01_mergeALL', field_names)
df_wtr01 = pd.DataFrame(data=[row for row in cursor], columns=field_names)

field_names = [i.name for i in arcpy.ListFields('streams_02_sjoin') if i.name in ['FWA_WATERSHED_CODE', 'LOCAL_WATERSHED_CODE', 'EST_NO']] 
cursor = arcpy.da.SearchCursor('streams_02_sjoin', field_names)
df_str02 = pd.DataFrame(data=[row for row in cursor], columns=field_names)

# Remove stream with a watershed code of all 9s:
# I noticed after processing that I had small slivers of watersheds in northern BC that were not
# connected to any other watersheds/estuaries. These all have watershed codes of all 9s. Streams of
# all 9s are small streams that exist in isolation and are not connected to any other streams. Some
# of these get picked up in the spatial select. Then, when I join the full watershed dataset to the
# streams, it associates these watersheds of all 9s to the other watersheds that contain these
# streams.
# I suppose this is something with the differences between FWA watershed code and Local watershed
# code, however, when filling nulls, as I do below, its not just all 9s that are null in the local
# watershed code, so I don't fully understand why these are null to begin with.
df_str02 = df_str02[df_str02.FWA_WATERSHED_CODE != '999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999-999999']

# not all streams have a local watershed code
df_str02['LOCAL_WATERSHED_CODE'] = df_str02.LOCAL_WATERSHED_CODE.fillna(df_str02.FWA_WATERSHED_CODE)

# remove any duplicates since multiple segments with the same stream code may have been selected
df_str02 = df_str02.groupby(['EST_NO', 'LOCAL_WATERSHED_CODE']).size().reset_index(name='count')

# Create new field for watershed code that strips trailing zero segments (-00000-)
# Use the local watershed code. For coastal watersheds that have no streams, only the local
# watershed code is unique.
def str_code(code):
    code_segs = code.split('-')
    for i, cs in enumerate(code_segs):
        if cs == '000000':
            cutoff = i
            break
    new_code = '-'.join(code.split('-')[:i])
    return new_code

df_str02['code_merge'] = df_str02['LOCAL_WATERSHED_CODE'].apply(str_code)

# Nass river issue:
# When I first ran this code, I found that the Nass river watershed was very small.
# I noticed that the estuary polygon doesn't occupy all of the river for some reason. There is a
# tiny line segment at the very start that is not included in the spatial select:
# 500-00000-...
# For all major rivers, they start with this structure, and in the spatial select these get included.
# The upstream main river segments will have different codes in the second part that don't build
# on each other. Therefore, if the starting 500- isn't selected by the spatial join, then certain
# areas upstream won't match when I do the key and join.
# Whereas other coastal streams will start with 900-xyz with the second part of the code already
# filled in and with segments upstream matching.
# So just add this piece in manually here.
df_nass = pd.DataFrame({'EST_NO':[431], 'code_merge':['500']})
df_str02 = pd.concat([df_str02, df_nass])

# Merge based on if a watershed code starts with the stream code adjacent to an estuary
# What this code does:
# On the right side, it takes all of the shortened codes into a list joined by the OR operator. The
# caret (^) matches the start of strings. Then, the str.extract pulls any matches from the df_wtr01
# field.
# It seems that it will match to the longest string, not just the first one that matches.
key = df_wtr01['LOCAL_WATERSHED_CODE'].str.extract('^(' + '|'.join(df_str02['code_merge']) + ')')
# Then we can join on that key.
df3 = df_str02.merge(df_wtr01.assign(key=key), left_on='code_merge', right_on='key', how='inner')#.drop('key', 1)

# double check that these are unique
df3 = df3.groupby(['EST_NO', 'WATERSHED_FEATURE_ID']).size().reset_index(name='count')
df3 = df3[['EST_NO', 'WATERSHED_FEATURE_ID']]

# output to gdb table
x = np.array(np.rec.fromrecords(df3.values)) 
names = df3.dtypes.index.tolist() 
x.dtype.names = tuple(names) 
arcpy.da.NumPyArrayToTable(x, os.path.join(arcpy.env.workspace, f'watersheds_02_tbl'))

# join with fundamental watersheds, keeping only common
arcpy.MakeFeatureLayer_management('watersheds_01_mergeALL', 'temp_wtr')
arcpy.env.qualifiedFieldNames = False
arcpy.AddJoin_management('temp_wtr', 'WATERSHED_FEATURE_ID', 'watersheds_02_tbl', 'WATERSHED_FEATURE_ID', 'KEEP_COMMON')
arcpy.CopyFeatures_management('temp_wtr', 'watersheds_03_join')
arcpy.Delete_management('temp_wtr')

# Dissolve by estuary number
arcpy.PairwiseDissolve_analysis('watersheds_03_join', 'watersheds_04_dissolve', 'EST_NO')

# Clip with land (e.g. Skeena estuary doesn't align)
arcpy.PairwiseClip_analysis('watersheds_04_dissolve', 'land_erase', 'watersheds_05_clip')


#############################################
# Create coastal shoreline zone

# 2 reasons for this:
# (1) some activities generate different stressors depending on if they are on the shoreline or
# further up a watershed
# (2) the watersheds generated based on streams intersecting don't include other coastal watersheds
# that may boundary estuaries but do not have streams connecting them. For example, look at the 
# Fraser. If we include the watershed that has UBC which technically touches the estuary, then we 
# end up with 2 issues: we include the influence of all of that urban area which is significant, and
# we also end up including some 900 level watersheds that might match with the Fraser if they don't 
# have any other "startswith" watershed matches. This is probably why we get watersheds on the
# sunshine coast being associated with the lower Fraser.

# Buffer distance: 500m
# We should get more lit to support this.

arcpy.PairwiseBuffer_analysis('estuaries_01_ALL', 'shoreline_01_buff', 500)
# Remove ocean bits so we don't include that area in future area calculations
arcpy.Merge_management(['estuaries_01_ALL', 'land_erase'], "temp_clipfeatures")
arcpy.PairwiseClip_analysis('shoreline_01_buff', 'temp_clipfeatures', 'shoreline_02_clip')
# Lots of slivers
arcpy.CopyFeatures_management('shoreline_02_clip', 'shoreline_03_integrate')
arcpy.PairwiseIntegrate_analysis('shoreline_03_integrate', 10)
arcpy.Delete_management('temp_clipfeatures')

# Keep estuaries as part of buffers. Some terrestrial activities may overlap estuaries as well.

# Also I don't think I want to erase the shoreline buffer from the watersheds. Technically, the 
# stressors are different so its not really double counting.

# Update:
# We are now using just the buffered section of the estuary clipped to land, however, we may revert
# back so I will make a new stream of fcs.
# AS OF NOW, I am NOW using this to clip the features in the next script. I am using it just to get
# the area for dividing. There were very few instances of features overlapping the estuaries
# anyways.
arcpy.PairwiseBuffer_analysis('estuaries_01_ALL', 'shorezone_01_buff', 500)
# Remove ocean bits so we don't include that area in future area calculations
arcpy.Merge_management(['estuaries_01_ALL', 'land_erase'], "temp_clipfeatures")
arcpy.PairwiseClip_analysis('shorezone_01_buff', 'temp_clipfeatures', 'shorezone_02_clip')
# There are gaps between the shore and estuary, which we want to include as part of the shoreline.
arcpy.PairwiseErase_analysis('shorezone_02_clip', 'estuaries_01_ALL', 'shorezone_03_erase')
arcpy.Delete_management('temp_clipfeatures')



#############################################
# Combine estuaries and watersheds for future clipping functions

# Merge
arcpy.Merge_management(['estuaries_01_ALL', 'watersheds_05_clip'], 'EST_WTR_01_merge')
# Dissolve on EST_NO to get one polygon for the each estuary/watershed combination
arcpy.Dissolve_management(
    'EST_WTR_01_merge',
    'EST_WTR_02_dissolve',
    'EST_NO',
    [['EST_NAME', 'FIRST']]
)
arcpy.AlterField_management('EST_WTR_02_dissolve', 'FIRST_EST_NAME', 'EST_NAME', 'EST_NAME')


#############################################
# Create a watersheds fc with the shoreline buffer zone erased
# This is used for clipping just the general development dataset.

arcpy.Erase_analysis('watersheds_05_clip', 'shoreline_03_integrate', 'watersheds_shobufferase')
